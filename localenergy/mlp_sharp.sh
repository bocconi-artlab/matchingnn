seeds=(2 7 11)
i=0
datasetf=cifar
dataset=cifar10
gpu=2
for seed in ${seeds[@]}; do
    echo seed=$seed
    ((i++))
    python3 ../../SacredDNN/scripts/sharpness_single.py -F locen/mlp/$datasetf/adam with load_model=../configurations/configs/mlp_512_512/$datasetf/adam/$i/model_final seed=$seed dataset=$dataset model=mlp_512_512 use_center=False no_cuda=False save_model=False save_epoch=1 opt=nesterov y=1 preprocess=1 gpu=$gpu deterministic=True activation=relu noise_type=mult s_max=0.5 batch_size=2048 epochs=10 M2=10 M3=-1 &  
done
