seeds=(2 7 11)
i=0
gpu=2
for seed in ${seeds[@]}; do
    echo seed=$seed
    ((i++))
    python3 ../../SacredDNN/scripts/sharpness_single.py -F locen/resnet18/cifar/from_adv with load_model=../configurations/configs/resnet18/cifar/from_adv/$i/model_final seed=$seed dataset=cifar10 model=resnet18 use_center=False no_cuda=False save_model=False save_epoch=1 opt=nesterov y=1 preprocess=1 gpu=$gpu deterministic=True activation=relu noise_type=mult s_max=0.1 batch_size=2048 epochs=10 M2=10 M3=-1    
done
