import pandas as pd
import numpy as np
import matplotlib
from torch import NoneType
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from statistics import mean, stdev
plt.style.use('seaborn-white')
import os
os.chdir("/home/fabrizio/workspace/matchingnn/localenergy")

#fig, ax = plt.subplots(1, 2, figsize=(10.0, 6.0))

model = "mlp"; dataset = "mnist"
model = "mlp"; dataset = "fashion"     #
model = "mlp"; dataset = "cifar"       #
model = "testlenet"; dataset = "mnist" #
#model = "testlenet"; dataset = "fashion"
#model = "testlenet"; dataset = "cifar"
#model = "vgg16_bn"; dataset = "cifar"

configs = ["adam", "from_adv", "rsgd"]
color = ["tab:blue", "tab:red", "tab:green"]

marks = ["o", "s", "^"]

nstat = 3

localenergies, final_localenergies, final_testerrors = [], [], []

fig, ax = plt.subplots(3, 4, figsize=(10.0, 6.0))
idxs = [[i,j] for i in range(3) for j in range(4)]
print(idxs[::2])
for mh, (idx, model, dataset) in enumerate(zip(idxs[::2], ["mlp", "mlp", "testlenet", "testlenet", "testlenet", "vgg16_bn"], 
                                                          ["fashion", "cifar", "mnist", "fashion", "cifar", "cifar"])):
#for mh, (idx, model, dataset) in enumerate(zip(idxs[::2], ["mlp", "mlp", "mlp", "testlenet", "testlenet", "testlenet", "vgg16_bn", "resnet18"], 
#                                                          ["mnist", "fashion", "cifar", "mnist", "fashion", "cifar", "cifar", "cifar"])):
    #if mh in [4,5,6]:
    #    continue
    legend_flag = True
    for i, conf in enumerate(configs):
        for j in range(nstat):
            filename = f"locen/{model}/{dataset}/{conf}/{j+1}/metrics.json"

            #if model == "testlenet" and dataset == "fashion" and conf=="from_adv" and j==2:
            #    continue
            #elif model == "mlp" and dataset == "cifar" and conf=="adam" and j in [1, 2]:
            #    continue

            if os.path.isfile(filename):

                print(f"FOUND: {filename}")

                locen = pd.read_json(filename)

                final_train_error = locen['train_center_error']['values'][0]
                final_test_error = locen['test_center_error']['values'][0]

                if j % nstat == nstat-1:

                    if model not in ["vgg16_bn"]:
                        localenergies.append(locen['train_center_error_rel']['values'])
                        final_localenergies.append(locen['train_center_error_rel']['values'][-1])
                        sigmas = locen['sigma']['values']
                    else:
                        localenergies.append(locen['train_center_error_rel']['values'][0:-2])
                        final_localenergies.append(locen['train_center_error_rel']['values'][-4])
                        sigmas = locen['sigma']['values'][0:-2]

                    final_testerrors.append(final_test_error)

                    mean_localenergy = [*map(mean, zip(*localenergies))]
                    std_localenergy = [*map(stdev, zip(*localenergies))]
                    mean_final_localenergy = mean(final_localenergies)
                    std_final_localenergy = stdev(final_localenergies)
                    mean_final_trainerror = mean(final_testerrors)
                    std_final_trainerror = stdev(final_testerrors)
                    localenergies, final_localenergies, final_testerrors = [], [], []

                    y1 = [mean_localenergy[i] + std_localenergy[i] for i in range(len(mean_localenergy))]
                    y2 = [mean_localenergy[i] - std_localenergy[i] for i in range(len(mean_localenergy))]
                    y2 = np.asarray(y2)
                    y2[y2<0] = 0
                    ax[idx[0]][idx[1]].fill_between(sigmas, y1, y2, color=color[i], alpha=1.0/4)
                    ax[idx[0]][idx[1]].plot(sigmas, mean_localenergy, 
                            marker=marks[i], ms=7, c=color[i], alpha = 1.0)#, label=f"{configs[i]}  (train: {final_train_error:.1f}%, test: {final_test_error:.1f}%)")
                    # SCATTER PLOT
                    ax[idx[0]][idx[1]+1].errorbar(mean_final_localenergy, mean_final_trainerror, ms=9,
                            xerr=std_final_localenergy, yerr=std_final_trainerror, 
                            marker=marks[i], c=color[i], capsize=3)

                    if legend_flag:
                        modelpl = "LeNet" if model=="testlenet" else "MLP" if model=="mlp" else "VGG16" if model=="vgg16_bn" else model
                        datapl = "MNIST" if dataset=="mnist" else "CIFAR10" if dataset=="cifar" else "FMNIST" if dataset=="fashion" else dataset
                        ax[idx[0]][idx[1]+1].plot([None], [None], label=f"{modelpl}\n{datapl}")
                        legend_flag = False
                else:
                    if model not in ["vgg16_bn"]:
                        localenergies.append(locen['train_center_error_rel']['values'])
                        final_localenergies.append(locen['train_center_error_rel']['values'][-1])
                    else:
                        localenergies.append(locen['train_center_error_rel']['values'][0:-2])
                        final_localenergies.append(locen['train_center_error_rel']['values'][-4])

                    final_testerrors.append(final_test_error)
                    #ax[idx[0]][idx[1]].plot(locen['sigma']['values'], locen['train_center_error_rel']['values'], 
                    #        marker=marks[i], ms=8, c=color[i], alpha = 0.6)

                ## SCATTER PLOT
                #ax[idx[0]][idx[1]+1].scatter(locen['train_center_error_rel']['values'][-1], final_test_error, 
                #        marker=marks[i], c=color[i])

            else:
                print(f"NOT FOUND: {filename}")
                if j % nstat == nstat-1:
                    mean_localenergy = [*map(mean, zip(*localenergies))]
                    std_localenergy = [0 for _ in range(len(mean_localenergy))] #[*map(stdev, zip(*localenergies))]
                    mean_final_localenergy = mean(final_localenergies)
                    std_final_localenergy = 0 #stdev(final_localenergies)
                    mean_final_trainerror = mean(final_testerrors)
                    std_final_trainerror = 0 #stdev(final_testerrors)
                    localenergies, final_localenergies, final_testerrors = [], [], []
                    y1 = [mean_localenergy[i] + std_localenergy[i] for i in range(len(mean_localenergy))]
                    y2 = [mean_localenergy[i] - std_localenergy[i] for i in range(len(mean_localenergy))]
                    y2 = np.asarray(y2)
                    y2[y2<0] = 0
                    ax[idx[0]][idx[1]].fill_between(locen['sigma']['values'], y1, y2, color=color[i], alpha=1.0/4)
                    ax[idx[0]][idx[1]].plot(locen['sigma']['values'], mean_localenergy, 
                            marker=marks[i], ms=7, c=color[i], alpha = 1.0)#, label=f"{configs[i]}  (train: {final_train_error:.1f}%, test: {final_test_error:.1f}%)")
                    # SCATTER PLOT
                    ax[idx[0]][idx[1]+1].errorbar(mean_final_localenergy, mean_final_trainerror, ms=9,
                            xerr=std_final_localenergy, yerr=std_final_trainerror, 
                            marker=marks[i], c=color[i], capsize=3)


# plot con le localenergies
for idx in idxs[::2]:
    ax[idx[0]][idx[1]].set_ylabel("local energy", fontsize=20)
    ax[idx[0]][idx[1]].set_yscale("log")

# scatterplot
for idx in idxs[1::2]:
    ax[idx[0]][idx[1]].set_ylabel("test error", fontsize=20)
    ax[idx[0]][idx[1]].legend(loc=(-0.15,0.5), fontsize=18, handlelength=0)
    ax[idx[0]][idx[1]].set_xscale("log")

for idx in [[2,0], [2,2]]:
    ax[idx[0]][idx[1]].set_xlabel("noise amplitude", fontsize=20)
for idx in [[2,1], [2,3]]:
    ax[idx[0]][idx[1]].set_xlabel("local energy", fontsize=20)

for idx in idxs:
    ax[idx[0]][idx[1]].tick_params(labelsize=18)

ax[0,3].set_xlim(8e0,100)

#plt.suptitle(f"{model},  {dataset}", fontsize=20)
plt.tight_layout()
plt.savefig(f"figures/locen.png")
plt.savefig(f"figures/locen.pdf")
#plt.savefig(f"figures/locen_{model}_{dataset}.pdf")
plt.close()

