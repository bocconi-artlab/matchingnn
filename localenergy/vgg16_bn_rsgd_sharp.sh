seeds=(2 7 11)
i=-1
folder=(1 2 3)
gpu=1
for seed in ${seeds[@]}; do
    echo seed=$seed
    ((i++))
    python3 ../../SacredDNN/scripts/sharpness_single.py -F locen/vgg16_bn/cifar/rsgd with load_model=../configurations/configs/vgg16_bn/cifar/rsgd/${folder[i]}/model_final seed=$seed dataset=cifar10 model=vgg16_bn use_center=False no_cuda=False save_model=False save_epoch=1 opt=nesterov y=1 preprocess=1 gpu=$gpu deterministic=True activation=relu noise_type=mult s_max=0.1 batch_size=2048 epochs=10 M2=10 M3=-1   
done
