def initializeTest(model_name,ncolors,outvolume,curve,num_bends,path_to_check,loss,weight_decay):
    architecture = getattr(TestLeNet,model_name)
    curve = getattr(curves, curve)
    criterion = getattr(F,loss)
    base_model = architecture().base(ncolors, outvolume)
    curve_model = curves.CurveNetTest(
        ncolors,
        outvolume,
        curve,
        architecture.curve,
        num_bends,
        architecture_kwargs=architecture.kwargs
    )

    checkpoint = torch.load(path_to_check,map_location=torch.device('cpu'))
    curve_model.load_state_dict(checkpoint['model_state'])
    regularizer = utils.l2_regularizer(weight_decay)

    return base_model,curve_model,criterion,regularizer,checkpoint

def initializeMLP(model_name,nim,nhs,num_classes,curve,num_bends,path_to_check,loss,weight_decay):
    architecture = getattr(MLP,model_name)
    curve = getattr(curves, curve)
    criterion = getattr(F,loss)
    base_model = architecture().base(nim,nhs,num_classes)
    curve_model = curves.CurveNetMLP(
        num_classes,
        nim,
        nhs,
        curve,
        architecture.curve,
        num_bends,
        architecture_kwargs=architecture.kwargs
    )

    checkpoint = torch.load(path_to_check)
    curve_model.load_state_dict(checkpoint['model_state'])
    regularizer = utils.l2_regularizer(weight_decay)

    return base_model,curve_model,criterion,regularizer,checkpoint

def initializeVGG(model_name, curve, num_bends, path_to_check, loss, weight_decay):
    architecture = getattr(VGG16bn, model_name)
    curve = getattr(curves, curve)
    criterion = getattr(F,loss)
    base_model = architecture.base()

    curve_model = curves.CurveNetVGG(
        curve,
        architecture.curve,
        num_bends,
        architecture_kwargs=architecture.kwargs
    )
    checkpoint = torch.load(path_to_check)
    curve_model.load_state_dict(checkpoint['model_state'])
    #regularizer = utils.l2_regularizer(weight_decay)

    return base_model,curve_model,criterion,0,checkpoint

def to_gpuid_string(x):
    if isinstance(x, list):
        return str(x)[1:-1]
    return str(x)

def initialize(model_name,loader,curve,num_classes,num_bends,path_to_check,loss,weight_decay):
    dim = loader['train'].dataset[0][0].shape[2]
    in_channels = loader['train'].dataset[0][0].shape[0]
    if model_name == 'ResNet20':
        architecture = getattr(RN_cifar, model_name)
    else:
        architecture = getattr(LeNet, model_name)
    curve = getattr(curves, curve)
    criterion = getattr(F,loss)
    if model_name == 'ResNet20':
        base_model = architecture.base()
    else:
        base_model = architecture.base(in_channels = in_channels,dim = dim,nclasses=num_classes, **architecture.kwargs)
    base_model

    curve_model = curves.CurveNet(
        num_classes,
        in_channels,
        dim,
        curve,
        architecture.curve,
        num_bends,
        architecture_kwargs=architecture.kwargs,
    )
    curve_model

    checkpoint = torch.load(path_to_check)
    curve_model.load_state_dict(checkpoint['model_state'])
    regularizer = utils.l2_regularizer(weight_decay)

    return base_model,curve_model,criterion,regularizer,checkpoint

## Funzione: perturbare (prendendo volendo anche pesi random) i pesi dei punti di mezzo e provare cammino a 0 epoche

def get_xy(point, origin, vector_x, vector_y):
    return np.array([np.dot(point - origin, vector_x), np.dot(point - origin, vector_y)])

def space_dimensionality(curve_model,num_bends,perturb = None):
    w = list()
    curve_parameters = list(curve_model.net.parameters())
    for i in range(num_bends):
        w.append(np.concatenate([
            p.data.cpu().numpy().ravel() for p in curve_parameters[i::num_bends]
        ]))
    print('Weight space dimensionality: %d' % w[0].shape[0])
    if perturb is not None:
        z = perturb * np.random.normal(size=w[1].shape[0])
        w[1] = w[1] * (1 + z)
    return w

def curve_coord(w,curve_points,curve_model):
    u = w[2] - w[0]
    dx = np.linalg.norm(u)
    u /= dx

    v = w[1] - w[0]
    v -= np.dot(u, v) * u
    dy = np.linalg.norm(v)
    v /= dy

    bend_coordinates = np.stack(get_xy(p, w[0], u, v) for p in w)

    ts = np.linspace(0.0, 1.0, curve_points)
    curve_coordinates = []
    for t in np.linspace(0.0, 1.0, curve_points):
        weights = curve_model.weights(torch.Tensor([t]))
        curve_coordinates.append(get_xy(weights, w[0], u, v))
    curve_coordinates = np.stack(curve_coordinates)
    return curve_coordinates,dx,dy,u,v,ts,bend_coordinates

def create_grid(grid_points,margin_left = 0.3,margin_right=0.3,margin_bottom=0.9,margin_top=1.5):
    G = grid_points
    alphas = np.linspace(0.0 - margin_left, 1.0 + margin_right, G)
    betas = np.linspace(0.0 - margin_bottom, 1.0 + margin_top, G)

    tr_loss = np.zeros((G, G))
    tr_nll = np.zeros((G, G))
    tr_acc = np.zeros((G, G))
    tr_err = np.zeros((G, G))

    te_loss = np.zeros((G, G))
    te_nll = np.zeros((G, G))
    te_acc = np.zeros((G, G))
    te_err = np.zeros((G, G))

    grid = np.zeros((G, G, 2))

    return alphas,betas,tr_loss,tr_nll,tr_acc,tr_err,te_loss,te_nll,te_acc,te_err,grid

def main():
    parser = argparse.ArgumentParser(description='Plane Maker')
    parser.add_argument('--dir',required = True, type=str, default=None, metavar='DIR',
                        help='training directory (default: None')
    parser.add_argument('--dataset', type=str, default='FashionMNIST', metavar='DATASET',
                        help='dataset name (default: FashionMNIST)')
    parser.add_argument('--ckpt',required = True, type=str, default=None, metavar='CKPT',
                        help='curve checkpoint (default: None)')
    parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                            help='input batch size (default: 128)')
    parser.add_argument('--seed', type=int, default=42, metavar='S', help='random seed (default: 42)')
    parser.add_argument('--model', type=str, default='LeNet', metavar='MODEL',
                        help='model name (default: LeNet)')
    parser.add_argument('--curve', type=str, default='PolyChain', metavar='CURVE',
                            help='curve type to use (default: PolyChain')
    parser.add_argument('--save_dir',required = True, type=str, default=None, metavar='DIR',
                        help='training directory (default: None')
    parser.add_argument('--nin', type=int, default=784, metavar='S', help='random seed (default: 784)')

    parser.add_argument('--gpu', dest='gpu', type=int, default=0, metavar='gpu',
                   help='visible gpu')   
    parser.add_argument('--perturb', type=float, default=None, metavar='S', help='random perturb (default: None)')
    
    args = parser.parse_args()
    
    torch.backends.cudnn.benchmark = True

    data_path = args.dir
    batch_size = args.batch_size
    random_seed = args.seed
    os.environ["CUDA_VISIBLE_DEVICES"] = to_gpuid_string(args.gpu)
    
    torch.backends.cudnn.benchmark = True ## Choose the fastest convolution algorithm
    torch.manual_seed(random_seed) ## Set random_seed to reproduce results
    
    basic_transform = getattr(Transforms,args.dataset)
    
    loaders, num_classes = data_new.loaders(
        args.dataset,
        basic_transform,
        data_path,
        batch_size,
        use_test=True
    )
    
    dim = loaders['train'].dataset[0][0].shape[2]
    in_channels = loaders['train'].dataset[0][0].shape[0]

    if args.model=='MLP':
       base_model,curve_model,criterion,regularizer,checkpoint = initializeMLP(args.model,args.nin,[512,512],10,args.curve, \
                    3,args.ckpt,"cross_entropy",5e-4) 
    elif args.model=='TestLeNet':
        if args.dataset in ["MNIST", "FashionMNIST"]:
            ncolors, outvolume = 1, 800
        elif args.dataset in ["CIFAR10"]:
            ncolors, outvolume = 3, 1250
        base_model,curve_model,criterion,regularizer,checkpoint = initializeTest(args.model, ncolors, outvolume, args.curve, \
                    3,args.ckpt,"cross_entropy",5e-4) 
    elif args.model=='VGG16bn':
        base_model,curve_model,criterion,regularizer,checkpoint = initializeVGG(args.model, args.curve, 3, args.ckpt, "cross_entropy", 5e-4)
    else:
        base_model,curve_model,criterion,regularizer,checkpoint = initialize(args.model,loaders,args.curve,10,3,args.ckpt,"cross_entropy",5e-4)
    
    base_model.cuda()
    curve_model.cuda()

    #base_model.cpu()

    w = space_dimensionality(curve_model,3,args.perturb)
    coord,dx,dy,u,v,ts,bend_coordinates = curve_coord(w,40,curve_model)
    
    alphas,betas,tr_loss,tr_nll,tr_acc,tr_err,te_loss,te_nll,te_acc,te_err,grid = create_grid(30)

    columns = ['X', 'Y', 'Train loss', 'Train nll', 'Train error (%)', 'Test nll', 'Test error (%)']
    
    for i, alpha in enumerate(alphas):
        for j, beta in enumerate(betas):
            p = w[0] + alpha * dx * u + beta * dy * v
    
            offset = 0
            for parameter in base_model.parameters():
                size = np.prod(parameter.size())
                value = p[offset:offset+size].reshape(parameter.size())
                parameter.data.copy_(torch.from_numpy(value))
                offset += size
    
            utils.update_bn(loaders['train'], base_model.cuda())
    
            tr_res = utils.test(loaders['train'], base_model,base_model,criterion = criterion, regularizer = None,geodetic = False)
            te_res = utils.test(loaders['test'], base_model,base_model,criterion = criterion, regularizer = None,geodetic = False)
    
            tr_loss_v, tr_nll_v, tr_acc_v = tr_res['loss'], tr_res['nll'], tr_res['accuracy']
            te_loss_v, te_nll_v, te_acc_v = te_res['loss'], te_res['nll'], te_res['accuracy']
    
            c = get_xy(p, w[0], u, v)
            grid[i, j] = [alpha * dx, beta * dy]
    
            tr_loss[i, j] = tr_loss_v
            tr_nll[i, j] = tr_nll_v
            tr_acc[i, j] = tr_acc_v
            tr_err[i, j] = 100.0 - tr_acc[i, j]
    
            te_loss[i, j] = te_loss_v
            te_nll[i, j] = te_nll_v
            te_acc[i, j] = te_acc_v
            te_err[i, j] = 100.0 - te_acc[i, j]
    
            values = [
                grid[i, j, 0], grid[i, j, 1], tr_loss[i, j], tr_nll[i, j], tr_err[i, j],
                te_nll[i, j], te_err[i, j]
            ]
            table = tabulate.tabulate([values], columns, tablefmt='simple', floatfmt='10.4f')
            if j == 0:
                table = table.split('\n')
                table = '\n'.join([table[1]] + table)
            else:
                table = table.split('\n')[2]
            print(table)
        
    np.savez(
        f"{args.save_dir}/Plane_{args.ckpt}.npz",
        ts=ts,
        bend_coordinates=bend_coordinates,
        curve_coordinates=coord,
        alphas=alphas,
        betas=betas,
        grid=grid,
        tr_loss=tr_loss,
        tr_acc=tr_acc,
        tr_nll=tr_nll,
        tr_err=tr_err,
        te_loss=te_loss,
        te_acc=te_acc,
        te_nll=te_nll,
        te_err=te_err
        )
    
if __name__ == '__main__':

    import numpy as np
    import argparse
    import tabulate
    import torch
    import torch.nn.functional as F
    import LeNet,RN_cifar,MLP,TestLeNet,VGG16bn
    import curves
    import utils
    import Transforms
    import os
    import data_new
    
    main()
    

