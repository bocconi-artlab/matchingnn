import math
import torch.nn as nn
import torch

import curves

def flatten(x): 
    return x.view(x.size(0), -1)


def conv3x3(in_planes, out_planes, stride=1):
    "3x3 convolution with padding"
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False)

def conv3x3Curve(in_planes, out_planes,fix_points, stride=1):
    "3x3 convolution with padding"
    return curves.Conv2d(in_planes, out_planes,fix_points = fix_points, kernel_size=3, stride=stride, padding=1, bias=False)


class BasicBlock(nn.Module):

    expansion = 1       

    def __init__(self, in_planes, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()

        self.conv1 = conv3x3(in_planes, planes, stride)
        self.conv2 = conv3x3(planes, planes)

        self.bn1 = nn.BatchNorm2d(planes)
        self.bn2 = nn.BatchNorm2d(planes)

        self.relu = nn.ReLU(inplace=True)
        #self.relu = QuadLin()
        self.downsample = downsample

        self.stride = stride

    def forward(self, x):

        residue = x
        out = self.relu(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))

        if self.downsample is not None:
            residue = self.downsample(x)

        out += residue
        out = self.relu(out)
        return out



class BasicBlockCurve(nn.Module):

    expansion = 1       

    def __init__(self, in_planes, planes,fix_points, stride=1, downsample=None):
        super(BasicBlockCurve, self).__init__()

        self.conv1 = conv3x3Curve(in_planes, planes,fix_points, stride)
        self.conv2 = conv3x3Curve(planes, planes, fix_points)

        self.bn1 = curves.BatchNorm2d(planes,fix_points)
        self.bn2 = curves.BatchNorm2d(planes,fix_points)

        self.relu = nn.ReLU(inplace=True)
        #self.relu = QuadLin()
        self.downsample = downsample

        self.stride = stride

    def forward(self, x, coeffs_t):

        residue = x
        out = self.relu(self.bn1(self.conv1(x,coeffs_t),coeffs_t))
        out = self.bn2(self.conv2(out,coeffs_t),coeffs_t)

        if self.downsample is not None:
            for element in range(len(self.downsample)):
                residue = self.downsample[element](residue,coeffs_t)

        out += residue
        out = self.relu(out)
        return out
    
class Bottleneck(nn.Module):
    
    expansion = 4

    def __init__(self, in_planes, planes, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        
        self.conv3 = nn.Conv2d(planes, self.expansion*planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(self.expansion*planes)

        self.downsample = downsample
        
        self.stride = stride
        
    def forward(self, x):

        residue = x
        out = self.relu(self.bn1(self.conv1(x)))
        out = self.relu(self.bn2(self.conv2(x)))
        out = self.bn3(self.conv3(out))

        if self.downsample is not None:
            residue = self.downsample(x)

        out += residue
        out = self.relu(out)
        return out
    
class BottleneckCurve(nn.Module):
    
    expansion = 4

    def __init__(self, in_planes, planes,fix_points, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = curves.Conv2d(in_planes, planes,fix_points, kernel_size=1, bias=False)
        self.bn1 = curves.BatchNorm2d(planes,fix_points)
        
        self.conv2 = curves.Conv2d(planes, planes,fix_points, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = curves.BatchNorm2d(planes,fix_points)
        
        self.conv3 = curves.Conv2d(planes, self.expansion*planes,fix_points, kernel_size=1, bias=False)
        self.bn3 = curves.BatchNorm2d(self.expansion*planes,fix_points)

        self.downsample = downsample
        
        self.stride = stride
        
    def forward(self, x, coeffs_t):

        residue = x
        out = self.relu(self.bn1(self.conv1(x,coeffs_t),coeffs_t))
        out = self.relu(self.bn2(self.conv2(x,coeffs_t),coeffs_t))
        out = self.bn3(self.conv3(out,coeffs_t),coeffs_t)

        if self.downsample is not None:
            residue = self.downsample(x,coeffs_t)

        out += residue
        out = self.relu(out)
        return out


class ResNetBase(nn.Module):

    def __init__(self, depth = 20,num_classes=10, block=BasicBlock, activation=nn.ReLU()):
        super(ResNetBase, self).__init__()

        assert (depth - 2) % 6 == 0, 'Depth should be 6n + 2'
        n = (depth - 2) // 6

        block = BasicBlock
        self.inplanes = 16
        fmaps = [16, 32, 64] # CIFAR10

        self.conv = nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn = nn.BatchNorm2d(16)
        self.relu = nn.ReLU(inplace=True)
        #self.relu = QuadLin()

        self.layer1 = self._make_layer(block, fmaps[0], n, stride=1)
        self.layer2 = self._make_layer(block, fmaps[1], n, stride=2)
        self.layer3 = self._make_layer(block, fmaps[2], n, stride=2)

        self.avgpool = nn.AvgPool2d(kernel_size=8, stride=1)
        self.flatten = flatten
        self.fc = nn.Linear(fmaps[2] * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_layer(self, block, planes, blocks, stride=1):
        ''' Between layers convolve input to match dimensions -> stride = 2 '''

        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                    nn.Conv2d(self.inplanes, planes * block.expansion,
                              kernel_size=1, stride=stride, bias=False),
                    nn.BatchNorm2d(planes * block.expansion))

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)
    
    def forward(self, x, print_sizes=False):
        
        if print_sizes:
            print('Sizes of the tensors inside each node: \n')
            print("\t In Model: input size", x.size())
        
        x = self.relu(self.bn(self.conv(x)))    # 32x32
        
        x = self.layer1(x)                      # 32x32
        x = self.layer2(x)                      # 16x16
        x = self.layer3(x)                      # 8x8

        x = self.avgpool(x)                     # 1x1
        x = self.flatten(x)                     # Flatten
        x  = self.fc(x)                         # Dense
        
        if print_sizes:
            print("\t In Model: output size", x.size())
            
        return x
    
class ResNetCurve(nn.Module):

    def __init__(self,in_channels,dim,fix_points,depth=20,num_classes=10, block=BasicBlockCurve, activation=nn.ReLU()):
        super(ResNetCurve, self).__init__()
        kwargs = dict()
        if fix_points is not None:
            kwargs['fix_points'] = fix_points

        assert (depth - 2) % 6 == 0, 'Depth should be 6n + 2'
        n = (depth - 2) // 6

        block = BasicBlockCurve
        self.inplanes = 16
        fmaps = [16, dim, 64] # CIFAR10

        self.conv = curves.Conv2d(in_channels, 16, kernel_size=3,fix_points = fix_points, stride=1, padding=1, bias=False)
        self.bn = curves.BatchNorm2d(16,fix_points)
        self.relu = nn.ReLU(inplace=True)
        #self.relu = QuadLin()

        self.layer1 = self._make_layer(block, fmaps[0], n, stride=1,fix_points = fix_points)
        self.layer2 = self._make_layer(block, fmaps[1], n, stride=2,fix_points = fix_points)
        self.layer3 = self._make_layer(block, fmaps[2], n, stride=2,fix_points = fix_points)

        self.avgpool = nn.AvgPool2d(kernel_size=8, stride=1)
        self.flatten = flatten
        self.fc = curves.Linear(fmaps[2] * block.expansion, num_classes,fix_points=fix_points)

        for m in self.modules():
            if isinstance(m, curves.Conv2d):
                for i in range(m.num_bends):
                    nn.init.kaiming_normal_(getattr(m, 'weight_%d' % i), mode='fan_out', nonlinearity='relu')
            elif isinstance(m, curves.BatchNorm2d):
                for i in range(m.num_bends):
                    nn.init.constant_(getattr(m, 'weight_%d' % i), 1)
                    nn.init.constant_(getattr(m, 'bias_%d' % i), 0)

    def _make_layer(self, block, planes, blocks,fix_points, stride=1):
        ''' Between layers convolve input to match dimensions -> stride = 2 '''

        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                    curves.Conv2d(self.inplanes, planes * block.expansion,
                              kernel_size=1,fix_points=fix_points, stride=stride, bias=False),
                    curves.BatchNorm2d(planes * block.expansion,fix_points))

        layers = []
        layers.append(block(self.inplanes, planes,fix_points, stride, downsample))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes,fix_points))

        return nn.Sequential(*layers)


    def forward(self, x, coeffs_t):
                
        x = self.relu(self.bn(self.conv(x,coeffs_t),coeffs_t))    # 32x32
        
        for block in self.layer1:  # 32x32
            x = block(x,coeffs_t)
        for block in self.layer2:  # 16x16
            x = block(x, coeffs_t)
        for block in self.layer3:  # 8x8
            x = block(x, coeffs_t)
        
        x = self.avgpool(x)        # 1x1
        x = self.flatten(x)        # Flatten
        x  = self.fc(x,coeffs_t)   # Dense
              
        return x


class ResNet20:
    base = ResNetBase
    curve = ResNetCurve
    kwargs = {}
