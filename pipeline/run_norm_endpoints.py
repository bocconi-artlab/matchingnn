import os
from itertools import product

def give_model_names(config, modeltype1):
    conf1 = config[0].replace("/", "_")
    if modeltype1 == "permuted":
        modeltype11 = ""
        modeltype2 = f"_permuted_{conf1}"
    elif modeltype1 == "permuted_normalized":
        modeltype11 = "_normalized"
        modeltype2 = f"_permuted_{conf1}_normalized"
    elif modeltype1 == "permuted_rescaled":
        modeltype11 = "_rescaled"
        modeltype2 = f"_permuted_{conf1}_rescaled"
    elif modeltype1 == "permuted_fromnormed":
        modeltype11 = ""
        modeltype2 = f"_permuted_{conf1}_fromnormed"
    else:
        modeltype11 = modeltype1
        modeltype2 = modeltype1
    return modeltype11, modeltype2

configurations=[["adam/1", "adam/2"],
                ["adam/1", "adam/3"],
                ["adam/2", "adam/3"],
                ["from_adv/1", "from_adv/2"],
                ["from_adv/1", "from_adv/3"],
                ["from_adv/2", "from_adv/3"],
                ["adam/1", "from_adv/2"],
                ["adam/2", "from_adv/1"],
                ["adam/3", "from_adv/3"],

                #["rsgd/1", "rsgd/2"],
                ["rsgd/1", "rsgd/3"],
                ["rsgd/2", "rsgd/3"],
                ["rsgd/1", "adam/2"],
                ["rsgd/1", "adam/1"],
                ["rsgd/3", "adam/3"],
                ["rsgd/1", "from_adv/2"],
                ["rsgd/2", "from_adv/1"],
                ["rsgd/3", "from_adv/3"],

                # SUPER-ADV (confusion_R=10)
                #["from_adv/4", "from_adv/6"], # from same init sgd vs adam
                #["from_adv/6", "from_adv/9"], # from different init adam vs sgd
                #["from_adv/4", "from_adv/9"],  # from different init sgd vs sgd
                #["from_adv/10", "from_adv/11"], # sgd vs sgd earlystop
                #["from_adv/6", "from_adv/8"], # different init adam vs adam
                #["from_adv/4", "from_adv/12"], # different DATASET sgd vs sgd
                #["adam/1", "from_adv/6"],
                #["rsgd/1", "from_adv/6"],

                ]

nin = 784
#model = "MLP"; modeldir = "mlp_512_512/mnist"; dataset = "MNIST"
#model = "MLP"; modeldir = "mlp_512_512/fashion"; dataset = "FashionMNIST"
#model = "MLP"; modeldir = "mlp_512_512/cifar"; dataset = "CIFAR10"; nin=3072
#model = "TestLeNet"; modeldir = "testlenet/mnist"; dataset = "MNIST"
#model = "TestLeNet"; modeldir = "testlenet/fashion"; dataset = "FashionMNIST"
#model = "TestLeNet"; modeldir = "testlenet/cifar"; dataset = "CIFAR10"
model = "VGG16bn"; modeldir = "vgg16_bn/cifar"; dataset = "CIFAR10"

epochs = 0
opt = "cosine"; lr = 0.02
modeltypes = ["", "_normalized", "permuted", "permuted_normalized", 
              "permuted_fromnormed", "_rescaled", "permuted_rescaled"]
#modeltypes = ["", "_normalized", "permuted", "permuted_normalized"]
#modeltypes = ["permuted_fromnormed", "_rescaled", "permuted_rescaled"]
modeltypes = [modeltypes[1], modeltypes[0]]
modeltypes = ["", "permuted", "permuted_normalized"]
modeltypes = ["permuted"]
#modeltypes = ["permuted"]
#modeltypes = ["", "permuted", "permuted_fromnormed"]
#modeltypes = [modeltypes[2]]

normalize_endpointsS = [False]
geodeticS = [True]
normalize_last_layerS = [True]
gpu = 2

for normalize_endpoints, geodetic, normalize_last_layer in product(normalize_endpointsS, geodeticS, normalize_last_layerS):
    
    normend = "" if normalize_endpoints else "-nonormend"
    geod = "" if not geodetic else f"-geod{geodetic}"
    savedirname = f"endnorm{geod}{normend}" if normalize_last_layer else f"endrescaled{geod}{normend}"

    for config in configurations:
        for modeltype1 in modeltypes:
            conf1 = config[0].replace("/", "_")
            conf2 = config[1].replace("/", "_")
            modeldirpl = modeldir.replace("/", "_")
            modeltype11, modeltype2 = give_model_names(config, modeltype1)

            checkpoint_name = f"curves/checkpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"
            #print(checkpoint_name)
            #continue

            dir = f"DIRcheckpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"
            if normalize_endpoints or geodetic:
                dir = f"DIRcheckpoint-{savedirname}-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"
            
            cmd = f"python Evaluation.py --dir=curves/ --dataset={dataset} --nin={nin} \
            --ckpt={checkpoint_name} --normalize_endpoints={normalize_endpoints} --geodetic={geodetic}\
            --model={model} --gpu={gpu} --normalize_last_layer={normalize_last_layer} \
            --save_dir=curves/{dir}"
            
            if os.path.exists(checkpoint_name):
                os.system(cmd)
            else:
                print(f"NOT FOUND: {checkpoint_name}")