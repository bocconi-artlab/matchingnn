import math
import torch
import torch.nn as nn
from torchvision.models import vgg16
import os
import curves

class LeNetBase(nn.Module):
    def __init__(self, in_channels,dim=28,
             out_channels_1=20, out_channels_2=50,
             kernel_size=5, stride=1, padding=0, dilation=1,
             mp_kernel_size=2, mp_stride=2, mp_padding=0, mp_dilation=1,
             fcsize1=500, nclasses=10):

        super(LeNetBase,self).__init__()

        # helper for calculating dimension after conv/max_pool op
        def convdim(dim):
            return (dim + 2*padding - dilation * (kernel_size - 1) - 1)//stride + 1
        def mpdim(dim):
            return (dim + 2*mp_padding - mp_dilation * (mp_kernel_size - 1) - 1)//mp_stride + 1

        self.conv1 = nn.Conv2d(in_channels, out_channels_1, kernel_size, stride)
        self.maxpool1 = nn.MaxPool2d(mp_kernel_size,
                                     stride=mp_stride,
                                     padding=mp_padding,
                                     dilation=mp_dilation)
        self.conv2 = nn.Conv2d(out_channels_1, out_channels_2, kernel_size, stride)

        # final dimension after applying conv->max_pool->conv->max_pool
        dim = mpdim(convdim(mpdim(convdim(dim))))
        self.fc1 = nn.Linear(out_channels_2 * dim * dim, fcsize1)
        self.fc2 = nn.Linear(fcsize1, nclasses)
        self.relu = nn.ReLU()
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                m.bias.data.zero_()

    def forward(self, x):
        x = self.relu(self.conv1(x))
        x = self.maxpool1(x)
        x = self.relu(self.conv2(x))
        x = self.maxpool1(x)
        x = x.view(x.size(0), -1)
        x = self.relu(self.fc1(x))
        x = self.fc2(x)
        return x

class LeNetCurve(nn.Module):
    def __init__(self, num_classes, fix_points,in_channels,dim=28,
                 out_channels_1=20, out_channels_2=50,
                 kernel_size=5, stride=1, padding=0, dilation=1,
                 mp_kernel_size=2, mp_stride=2, mp_padding=0, mp_dilation=1,fcsize1=500):
        
        super(LeNetCurve, self).__init__()
        kwargs = dict()
        if fix_points is not None:
            kwargs['fix_points'] = fix_points
        self.conv1 = curves.Conv2d(in_channels,out_channels_1,kernel_size,fix_points,stride)
        self.maxpool1 = nn.MaxPool2d(mp_kernel_size,mp_stride,
                                     padding=mp_padding,dilation=mp_dilation,ceil_mode = False)
        self.conv2 = curves.Conv2d(out_channels_1,out_channels_2,kernel_size,fix_points,stride)
        def convdim(dim):
            return (dim + 2*padding - dilation * (kernel_size - 1) - 1)//stride + 1
        def mpdim(dim):
            return (dim + 2*mp_padding - mp_dilation * (mp_kernel_size - 1) - 1)//mp_stride + 1
        
        dim = mpdim(convdim(mpdim(convdim(dim))))
        
        self.fc1 = curves.Linear(out_channels_2 * dim * dim,fcsize1,bias = True,**kwargs)
        self.fc2 = curves.Linear(fcsize1,num_classes,bias = True,**kwargs)
        self.relu = nn.ReLU()

        # Initialize weights
        for m in self.modules():
            if isinstance(m, curves.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                for i in range(m.num_bends):
                    getattr(m, 'weight_%d' % i).data.normal_(0, math.sqrt(2. / n))
                    getattr(m, 'bias_%d' % i).data.zero_()

    def forward(self, x, coeffs_t):
        x = self.relu(self.conv1(x,coeffs_t))
        x = self.maxpool1(x)
        x = self.relu(self.conv2(x,coeffs_t))
        x = self.maxpool1(x)
        x = x.view(x.size(0), -1)
        x = self.relu(self.fc1(x,coeffs_t))
        x = self.fc2(x,coeffs_t)
        return x

class LeNet:
    base = LeNetBase
    curve = LeNetCurve
    kwargs = {}
