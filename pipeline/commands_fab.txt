python training_LeNet.py --dir=curves --dataset=FashionMNIST --init_s=../configurations/configs/testlenet/adam/1/model_final.pt --init_e=../configurations/configs/testlenet/adam/2/model_final.pt --model=TestLeNet --epochs=0

python Evaluation.py --dir=curves/ --dataset=FashionMNIST --ckpt=curves/checkpoint-testlenet_adam_1_model_final.pt-testlenet_adam_2_model_final.pt-0.pt --model=TestLeNet --save_dir=curves/checkpoint-testlenet_adam_1_model_final.pt-testlenet_adam_2_model_final.pt-0.ptDIR
