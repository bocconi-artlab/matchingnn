def initializeTest(model_name, ncolors, outvolume, curve, num_bends):
    architecture = getattr(TestLeNet, model_name)
    if curve is None:
        model = architecture().base(ncolors, outvolume)
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetTest(
            ncolors,
            outvolume,
            curve,
            architecture.curve,
            num_bends,
            architecture_kwargs=architecture.kwargs
        )
    return model

def initializeVGG(model_name, curve, num_bends):
    architecture = getattr(VGG16bn, model_name)
    if curve is None:
        model = architecture().base()
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetVGG(
            curve,
            architecture.curve,
            num_bends,
            architecture_kwargs=architecture.kwargs
        )
    return model
    
def initializeMLP(model_name, nin, nhs, num_classes, curve, num_bends):
    architecture = getattr(MLP, model_name)
    if curve is None:
        model = architecture().base(nin, nhs, num_classes)
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetMLP(
            num_classes,
            nin,
            nhs,
            curve,
            architecture.curve,
            num_bends,
            architecture_kwargs = architecture.kwargs
        )
    return model

def initialize(model_name, curve, num_classes, in_channels, dim, num_bends):
    if model_name == 'ResNet20':
        curve = getattr(curves, curve)
        architecture = getattr(RN_cifar, model_name)
        model = curves.CurveNet(
        num_classes,
        in_channels,
        dim,
        curve,
        architecture.curve,
        num_bends,
        architecture_kwargs=architecture.kwargs
    )
    else:
        architecture = getattr(LeNet, model_name)
        curve = getattr(curves, curve)
        model = curves.CurveNet(
            num_classes,
            in_channels,
            dim,
            curve,
            architecture.curve,
            num_bends,
            architecture_kwargs=architecture.kwargs
        )
    return model.cuda()

def model_curve(model, t, check):

    values = []
    flag = 0
    res = OrderedDict()
    weights = model.weights(t)
    model.load_state_dict(check['model_state'])

    for i in check['model_state'].keys():
        if '_0' in i:
            s = check['model_state'][i].size()
            for element in range(len(check['model_state'][i].flatten())):
                values.append(weights[flag])
                flag+=1
            print(s)
            print(len(values))
            tens = torch.Tensor(values).reshape(s)
            print(i[4:-2])
            res[i[4:-2]] = tens
            values = []
        elif 'running_mean' in i or 'running_var' in i or 'num_batches_tracked' in i:
            res[i[4:]] = check['model_state'][i]
        else:
            continue
    return res

def hyper(loss, w_d):
    criterion = getattr(F, loss)
    regularizer = None
    return criterion, regularizer

def evaluate(num_points, model, modelname, m_to_norm, loaders, criterion, regularizer,
             s_dir, save_model, model_original, freq_save, check, geodetic=False, normalize_last_layer=True):

    T = num_points
    ts = np.linspace(0.0, 1.0, T)
    tr_loss = np.zeros(T)
    tr_nll = np.zeros(T)
    tr_acc = np.zeros(T)
    te_loss = np.zeros(T)
    te_nll = np.zeros(T)
    te_acc = np.zeros(T)
    tr_err = np.zeros(T)
    te_err = np.zeros(T)
    dl = np.zeros(T)
    
    t_list = []
    tr_loss_list = []
    tr_err_list = []
    te_loss_list = []
    te_err_list = []
    
    previous_weights = None

    columns = ['t', 'dist', 'Train loss', 'Train nll', 'Train error (%)', 'Test nll', 'Test error (%)']

    # GEODETIC
    if geodetic and False:

        # primo giro di normalizzazione di tutto il cammino
        with torch.no_grad():
            gamma = 1e-1 # * mean(norm(weights))
            num_iters = 100
            t = torch.FloatTensor([0.0])
            curve_dists = np.zeros(T)
            neighb_dists = np.zeros(T)

            point_indexes = [i for i in range(T)]
            weights_along_curve = []
            buffers_along_curve = []
            for i in point_indexes:
                t_value = ts[i]
                t.data.fill_(t_value)

                with torch.no_grad():
                    weights = model.weights(t, structured_net=True)
                    utils.update_bn(loaders['train'], model, t=t)

                #normalize point
                parameters_norm = list(m_to_norm.parameters())
                for j in range(len(parameters_norm)):
                    parameters_norm[j].data.copy_(weights[j].data)
                for bnorm, b in zip(m_to_norm.buffers(), model.net.buffers()):
                    bnorm.data.copy_(b.data)                    
                m_to_norm.cuda()

                if modelname in ["MLP"]:
                    net1 = m_to_norm.layers
                elif modelname in ["TestLeNet", "VGG16bn"]:
                    net1 = m_to_norm.model
                net1 = normalize_model(net1, normalize_last_layer=normalize_last_layer)
                if modelname in ["MLP"]:
                    m_to_norm.layers = net1
                elif modelname in ["TestLeNet", "VGG16bn"]:
                    m_to_norm.model = net1
                
                parameters_norm = list(m_to_norm.parameters())
                for j in range(len(parameters_norm)):
                    weights[j].data.copy_(parameters_norm[j].data)
                for bnorm, b in zip(m_to_norm.buffers(), model.net.buffers()):
                    b.data.copy_(bnorm.data)

                weights_along_curve.append(weights)
                buffers_along_curve.append(list(m_to_norm.buffers()))

            if False:
                if ".pt-0.pt" not in s_dir:
                    print(f"epoch: {s_dir[-10:]}, the midpoint is fixed in geodetic")
                    point_indexes.pop(10)
                else:
                    print("Epoch 0, the midpoint is not fixed in geodetic")
                #print(len(point_indexes))
                #raise NameError

                point_indexes = point_indexes[1:-1] # non devo prendere i punti estremi
                prev_curve_weights = None
                for it in range(num_iters):

                    #shuffle(point_indexes)
                    for i in point_indexes:

                        weights = weights_along_curve[i]
                        #weights_prev = deepcopy(weights)
                        #print(weights_prev[1])

                        if i == 0:
                            weights_next = weights_along_curve[i+1]
                            for j in range(len(weights)):
                                weights[j] -= gamma * (weights[j] - weights_next[j])
                        elif i == len(ts)-1:
                            weights_prev = weights_along_curve[i-1]
                            for j in range(len(weights)):
                                weights[j] -= gamma * (weights[j] - weights_prev[j])
                        else:
                            weights_next = weights_along_curve[i+1]
                            weights_prev = weights_along_curve[i-1]
                            for j in range(len(weights)):
                                #if j > 0 and len(weights[j].size()) == 1 and len(weights[j-1].size()) == 1: # con questo ho escluso i parametri batchnorm
                                #    continue
                                #else:
                                # interazione elastica (geodetica come era fatta all'inizio)
                                weights[j] -= (gamma * (weights[j] - weights_next[j]) + gamma * (weights[j] - weights_prev[j]))
                                # interazione informata (bisecante)
                                #weights[j] = 0.5 * (weights_next[j] + weights_prev[j])


                        #print(weights_prev[1])
                        #print(weights[1])
                        #print(weights[1] - weights_prev[1])
                        #raise NameError

                        #net1 = weights
                        parameters_norm = list(m_to_norm.parameters())
                        for j in range(len(parameters_norm)):
                            parameters_norm[j].data.copy_(weights[j].data)
                        for bnorm, b in zip(m_to_norm.buffers(), model.net.buffers()):
                            bnorm.data.copy_(b.data)
                        if modelname in ["MLP"]:
                            net1 = m_to_norm.layers
                        elif modelname in ["TestLeNet", "VGG16bn"]:
                            net1 = m_to_norm.model
                        # normalize network
                        net1 = normalize_model(net1, normalize_last_layer=normalize_last_layer)
                        if modelname in ["MLP"]:
                            m_to_norm.layers = net1
                        elif modelname in ["TestLeNet", "VGG16bn"]:
                            m_to_norm.model = net1
                        #weights = net1
                        parameters_norm = list(m_to_norm.parameters())
                        for j in range(len(parameters_norm)):
                            weights[j].data.copy_(parameters_norm[j].data)
                        for bnorm, b in zip(m_to_norm.buffers(), model.net.buffers()):
                            b.data.copy_(bnorm.data)

                    #calculate mean distance with previous curve to check convergence
                    if prev_curve_weights is not None:
                        for idx in [ii for ii in range(T)]:

                            # dist from previous curve
                            w1 = np.concatenate([w.cpu().numpy().ravel() for w in weights_along_curve[idx]])
                            w2 = np.concatenate([w.cpu().numpy().ravel() for w in prev_curve_weights[idx]])
                            curve_dists[idx] = np.sqrt(np.sum(np.square(w1 - w2)))

                            # dist from previous neigbor
                            if idx != 0:
                                w3 = np.concatenate([w.cpu().numpy().ravel() for w in weights_along_curve[idx-1]])
                                neighb_dists[idx] = np.sqrt(np.sum(np.square(w1 - w3)))

                            print("dist_curve:", curve_dists[idx], 
                                "dist_neighb:", neighb_dists[idx])
                    #prev_curve_weights = [deepcopy(w) for w in weights_along_curve]
                    prev_curve_weights = [copy(w) for w in weights_along_curve]
                    #prev_curve_weights = deepcopy(weights_along_curve)
                    # TODO: with VGG we cannot use deepcopy

                    print(f"MEAN NEIGHB DISTANCE (iter {it}):", sum(neighb_dists)/len(neighb_dists))
                    print(f"MEAN CURVE DISTANCE (iter {it}):", sum(curve_dists)/len(curve_dists))

            #raise NameError

    t = torch.FloatTensor([0.0])
    for i, t_value in enumerate(ts):

        t.data.fill_(t_value)
        #if not geodetic:
        weights_at_position_i = model.weights(t, structured_net=True)
        #else:
        #    weights_at_position_i = weights_along_curve[i]
        #    #model_weights = model.weights(t, structured_net=True)
        #    #for j in range(len(model_weights)):
        #    #    model_weights[j].data.copy_(weights_at_position_i[j].data)

        # TODO: ATTENZIONE a questo, non è implementato per la GEODETICA
        # cioé in pratica devo fare la stessa cosa che fa questa funzione ma su m_to_norm
        
        #if i not in [0, len(ts)-1]:
        utils.update_bn(loaders['train'], model, t=t)

        if geodetic:
            #normalize point
            parameters_norm = list(m_to_norm.parameters())
            for j in range(len(parameters_norm)):
                parameters_norm[j].data.copy_(weights_at_position_i[j].data)
            for bnorm, b in zip(m_to_norm.buffers(), model.net.buffers()):
                bnorm.data.copy_(b.data)                    
            m_to_norm.cuda()

            if modelname in ["MLP"]:
                net1 = m_to_norm.layers
            elif modelname in ["TestLeNet", "VGG16bn"]:
                net1 = m_to_norm.model
            net1 = normalize_model(net1, normalize_last_layer=True)
            if modelname in ["MLP"]:
                m_to_norm.layers = net1
            elif modelname in ["TestLeNet", "VGG16bn"]:
                m_to_norm.model = net1
            
            parameters_norm = list(m_to_norm.parameters())
            for j in range(len(parameters_norm)):
                weights_at_position_i[j].data.copy_(parameters_norm[j].data)
            for bnorm, b in zip(m_to_norm.buffers(), model.net.buffers()):
                b.data.copy_(bnorm.data)

            #parameters_norm = list(m_to_norm.parameters())
            #for j in range(len(parameters_norm)):
            #    parameters_norm[j].data.copy_(weights_at_position_i[j].data)
            #for bnorm, b in zip(m_to_norm.buffers(), buffers_along_curve[i]):
            #    bnorm.data.copy_(b.data)

        m_to_norm.eval()
        weights = np.concatenate([w.cpu().detach().numpy().ravel() for w in weights_at_position_i])

        if save_model:
            if i%freq_save == 0:
                s_d = model_curve(model, t, check)
                model_original.load_state_dict(s_d)
                utils.save_checkpoint(
                s_dir,
                i,
                model_state = model_original.state_dict()
            )

        if previous_weights is not None:
            dl[i] = np.sqrt(np.sum(np.square(weights - previous_weights)))
        previous_weights = deepcopy(weights)

        tr_res = utils.test(loaders['train'], model, m_to_norm, criterion, geodetic, regularizer, t=t)
        te_res = utils.test(loaders['test'], model, m_to_norm, criterion, geodetic, regularizer, t=t)

        #import pdb; pdb.set_trace()

        tr_loss[i] = tr_res['loss']
        tr_nll[i] = tr_res['nll']
        tr_acc[i] = tr_res['accuracy']
        tr_err[i] = 100.0 - tr_acc[i]
        te_loss[i] = te_res['loss']
        te_nll[i] = te_res['nll']
        te_acc[i] = te_res['accuracy']
        te_err[i] = 100.0 - te_acc[i]
        
        t_list.append(t_value)
        tr_err_list.append(tr_err[i])
        tr_loss_list.append(tr_loss[i])
        te_err_list.append(te_err[i])
        te_loss_list.append(te_loss[i])

        values = [t, dl[i], tr_loss[i], tr_nll[i], tr_err[i], te_nll[i], te_err[i]]
        table = tabulate.tabulate([values], columns, tablefmt='simple', floatfmt='10.4f')
        if i % 40 == 0:
            table = table.split('\n')
            table = '\n'.join([table[1]] + table)
        else:
            table = table.split('\n')[2]

        print(table)
        
    df = pd.DataFrame({'t':t_list, 'dist':dl, 'Train loss':tr_loss_list, 'Train error (%)':tr_err_list, 
                                   'Test loss':te_loss_list, 'Test error (%)':te_err_list})
    
    df.to_excel('{}/Curve_Evaluation.xlsx'.format(s_dir), index=False)

    return tr_loss, tr_nll, tr_err, te_loss, te_nll, te_err, dl

def stats(values, dl):
    min = np.min(values)
    max = np.max(values)
    avg = np.mean(values)
    int = np.sum(0.5 * (values[:-1] + values[1:]) * dl[1:]) / np.sum(dl[1:])
    return min, max, avg, int

def to_gpuid_string(x):
    if isinstance(x, list):
        return str(x)[1:-1]
    return str(x)

def main():
    parser = argparse.ArgumentParser(description='Curve evaluation')
    parser.add_argument('--dir',required = True, type=str, default=None, metavar='DIR',
                        help='training directory (default: None')
    parser.add_argument('--dataset', type=str, default='FashionMNIST', metavar='DATASET',
                        help='dataset name (default: FashionMNIST)')
    parser.add_argument('--ckpt',required = True, type=str, default=None, metavar='CKPT',
                        help='curve checkpoint (default: None)')
    parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                            help='input batch size (default: 128)')
    parser.add_argument('--seed', type=int, default=42, metavar='S', help='random seed (default: 42)')
    parser.add_argument('--model', type=str, default='LeNet', metavar='MODEL',
                        help='model name (default: LeNet)')
    parser.add_argument('--curve', type=str, default='PolyChain', metavar='CURVE',
                            help='curve type to use (default: PolyChain')
    parser.add_argument('--save_dir', required = True, type=str, default=None, metavar='DIR',
                        help='training directory (default: None')
    parser.add_argument('--nin', type=int, default=784, metavar='S', help='random seed (default: 784)')

    parser.add_argument('--save_models', dest='is_esgd', action='store_true',
                   help='turns on save_models (default: off)')

    parser.add_argument('--freq_save', type=int, default=10, metavar='N',
                        help='save model for each n (default: 10)')
    parser.add_argument('--gpu', dest='gpu', type=int, default=0, metavar='gpu',
                   help='visible gpu')              
    parser.add_argument('--normalize_endpoints', help='normalize_endpoints', type=eval, 
                        choices=[True, False],  default='False')     
    parser.add_argument('--normalize_last_layer', help='normalize last layer', type=eval, 
                        choices=[True, False],  default='True')
    parser.add_argument('--geodetic', help='normalize last layer', type=eval, 
                        choices=[True, False],  default='False')

    parser.set_defaults(save_models=False)    
    #parser.set_defaults(normalize_endpoints=False)

    args = parser.parse_args()
    data_path = args.dir
    batch_size = args.batch_size
    random_seed = args.seed
    os.environ["CUDA_VISIBLE_DEVICES"] = to_gpuid_string(args.gpu)    

    if not os.path.isdir(args.save_dir):
        os.mkdir(args.save_dir)
    #else:
    #    raise NameError("Directory %s already exists"%args.save_dir)
    
    torch.backends.cudnn.benchmark = True ## Choose the fastest convolution algorithm
    torch.manual_seed(random_seed) ## Set random_seed to reproduce results
    
    print("torch.cuda.is_available():", torch.cuda.is_available())

    basic_transform = getattr(Transforms,args.dataset)
    
    loaders, num_classes = data_new.loaders(
        args.dataset,
        basic_transform,
        data_path,
        batch_size,
        use_test=True
    )
    
    nin = args.nin
    dim = loaders['train'].dataset[0][0].shape[2]
    in_channels = loaders['train'].dataset[0][0].shape[0]
    
    # TODO: ricontrollare la funzione che salva i modelli lungo la curva
    checkpoint = torch.load(args.ckpt)

    if args.model=='MLP':
        model = initializeMLP(args.model, nin, [512,512], 10, args.curve, 3)
        model_original = initializeMLP(args.model, nin, [512,512], 10, None, 3)
        m_to_norm = MLP.MLPBase(nin, [512, 512], 10)
    elif args.model=='TestLeNet':
        if args.dataset in ["MNIST", "FashionMNIST"]:
            ncolors, outvolume = 1, 800
        elif args.dataset in ["CIFAR10"]:
            ncolors, outvolume = 3, 1250
        model = initializeTest(args.model, ncolors, outvolume, args.curve, 3)
        model_original = initializeTest(args.model, ncolors, outvolume, None, 3)
        m_to_norm = TestLeNet.TestLeNetBase(ncolors, outvolume)
    elif args.model=='VGG16bn':
        model = initializeVGG(args.model, args.curve, 3)
        model_original = initializeVGG(args.model, None, 3)
        m_to_norm = VGG16bn.vgg16_bn()
    else:
        model = initialize(args.model, args.curve, 10, in_channels, dim, 3)
        model_original = initialize(args.model, None, 10, in_channels, dim, 3)
    
    m_to_norm.eval()
    model.load_state_dict(checkpoint['model_state'])
    model.cuda()
    
    # normalize checkpoint and permute (all 3 points)
    permute_endpoints = True
    # TODO: la permutazione va fatta solo per epoca diversa da zero (sicuro?)
    if ".pt-0.pt" not in args.ckpt:
        normalize_midpoint = True
        print(f"epoch: {args.ckpt[-10:]}, normalize_midpoint={normalize_midpoint}")
    else:
        normalize_midpoint = False
        #normalize_midpoint = True
        print(f"Epoch 0, normalize_midpoint={normalize_midpoint}")
    #model = normalize_model_in_curve(model, m_to_norm, args.model, None, 
    #                                    normalize_endpoints=args.normalize_endpoints, normalize_last_layer=args.normalize_last_layer,
    #                                    permute_endpoints=permute_endpoints, normalize_midpoint=normalize_midpoint)
    
    criterion, regularizer = hyper("cross_entropy", 5e-4)
    tr_loss, tr_nll, tr_err, te_loss, te_nll, te_err, dl = evaluate(21, model, args.model, 
            m_to_norm, loaders, criterion, regularizer, args.save_dir, args.save_models, 
            model_original, args.freq_save, checkpoint, geodetic=args.geodetic, normalize_last_layer=args.normalize_last_layer)
    
    tr_loss_min, tr_loss_max, tr_loss_avg, tr_loss_int = stats(tr_loss, dl)
    tr_nll_min, tr_nll_max, tr_nll_avg, tr_nll_int = stats(tr_nll, dl)
    tr_err_min, tr_err_max, tr_err_avg, tr_err_int = stats(tr_err, dl)
    
    te_loss_min, te_loss_max, te_loss_avg, te_loss_int = stats(te_loss, dl)
    te_nll_min, te_nll_max, te_nll_avg, te_nll_int = stats(te_nll, dl)
    te_err_min, te_err_max, te_err_avg, te_err_int = stats(te_err, dl)
    
    print('Length: %.2f' % np.sum(dl))
    print(tabulate.tabulate([
            ['train loss', tr_loss[0], tr_loss[-1], tr_loss_min, tr_loss_max, tr_loss_avg, tr_loss_int],
            ['train error (%)', tr_err[0], tr_err[-1], tr_err_min, tr_err_max, tr_err_avg, tr_err_int],
            ['test nll', te_nll[0], te_nll[-1], te_nll_min, te_nll_max, te_nll_avg, te_nll_int],
            ['test error (%)', te_err[0], te_err[-1], te_err_min, te_err_max, te_err_avg, te_err_int],
        ], [
            '', 'start', 'end', 'min', 'max', 'avg', 'int'
        ], tablefmt='simple', floatfmt='10.4f'))
            
    labels = ['train loss','train error (%)','test error (%)']
    first = [tr_loss[0],tr_err[0],te_err[0]]
    second = [tr_loss[-1],tr_err[-1],te_err[-1]]
    minimum = [tr_loss_min,tr_err_min,te_err_min]
    maximum = [tr_loss_max,tr_err_max,te_err_max]
    average = [tr_loss_avg,tr_err_avg,te_err_avg]
    
    df = pd.DataFrame({'Labels':labels,'First Node':first,'Second Node':second,
                       'Minimum':minimum,'Maximum':maximum,'Average':average})
    
    df.to_excel('{}/logCurve.xlsx'.format(args.save_dir))
    
if __name__=='__main__':
    import os, sys
    import numpy as np
    from copy import copy, deepcopy
    import tabulate
    import torch
    import torch.nn as nn
    import torch.nn.functional as F
    import curves
    import utils
    import LeNet
    import data_new
    import Transforms
    import argparse
    import pandas as pd
    import RN_cifar, MLP, TestLeNet, VGG16bn
    from collections import OrderedDict
    from training_LeNet import normalize_model_in_curve
    from random import shuffle
    from src.normalize import normalize_model
    
    main()
