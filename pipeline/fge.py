def clean_dict(check,to_retain='center'):
    out = {}
    for i in check.keys():
        if i.split('.')[1] == to_retain: 
            out[i] = check[i]  
    return out

def clean_optimizer(d):
    for i in range(0,195):
        d['state'].pop(i)
        d['param_groups'][0]['params'].remove(i)

def initialize(model_name,ckpt,loaders,num_classes,s_e,is_rsgd):
    if model_name == "ResNet20":
        architecture = getattr(RN_cifar, model_name)
    else:
        architecture = getattr(LeNet, model_name)
    dim = loaders['train'].dataset[0][0].shape[2]
    in_channels = loaders['train'].dataset[0][0].shape[0]
    
    if model_name == 'ResNet20':
        model = architecture.base()
    else:
        model = architecture.base(nclasses=num_classes,in_channels=in_channels,dim=dim,**architecture.kwargs)
    criterion = F.cross_entropy
    
    checkpoint = torch.load(ckpt)
    
    if is_rsgd:
        state_dict = {}
        checkpoint_l = clean_dict(checkpoint[0],'0')
        print(checkpoint_l.keys())
        for i in checkpoint_l.keys():
            print(i)
            temp2 = re.sub("replicas.0.","",i)
            state_dict[temp2] = checkpoint_l[i]
        
    else: 
        state_dict={}
        for i in checkpoint[0].keys():
            print(i)
            temp2 = re.sub("replicas.0.","",i)
            state_dict[temp2] = checkpoint[0][i]

    start_epoch = s_e
    model.load_state_dict(state_dict)
    model.cuda()
    
    return model,checkpoint,criterion,start_epoch

def main():
    parser = argparse.ArgumentParser(description='FGE training')

    parser.add_argument('--dir', type=str, default='/tmp/fge/', metavar='DIR',
                        help='training directory (default: /tmp/fge)')
    parser.add_argument('--dataset', type=str, default='CIFAR10', metavar='DATASET',
                        help='dataset name (default: CIFAR10)')
    parser.add_argument('--use_test', action='store_true',
                        help='switches between validation and test set (default: validation)')
    parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                        help='input batch size (default: 128)')
    parser.add_argument('--start_epoch', type=int, default=200, metavar='N',
                        help='epoch from when to explore (default: 200)')
    parser.add_argument('--model', type=str, default='LeNet', metavar='MODEL',
                        help='model name (default: LeNet)')
    parser.add_argument('--ckpt',required=True, type=str, default=None, metavar='CKPT',
                        help='checkpoint to eval (default: None)')
    parser.add_argument('--epochs', type=int, default=20, metavar='N',
                        help='number of epochs to train (default: 20)')
    parser.add_argument('--cycle', type=int, default=4, metavar='N',
                        help='number of epochs to train (default: 4)')
    parser.add_argument('--lr_1', type=float, default=0.05, metavar='LR1',
                        help='initial learning rate (default: 0.05)')
    parser.add_argument('--lr_2', type=float, default=0.0001, metavar='LR2',
                        help='initial learning rate (default: 0.0001)')
    parser.add_argument('--momentum', type=float, default=0.9, metavar='M',
                        help='SGD momentum (default: 0.9)')
    parser.add_argument('--wd', type=float, default=1e-4, metavar='WD',
                        help='weight decay (default: 1e-4)')
    parser.add_argument('--seed', type=int, default=1, metavar='S', help='random seed (default: 1)')
    parser.add_argument('--is_rsgd', dest='is_rsgd', action='store_false',
                       help='turns on if_rsgd (default: off)')
    parser.add_argument('--save_dir', type=str, default='/tmp/fge/', metavar='DIR',
                        help='training directory (default: /tmp/fge)')
        
    parser.set_defaults(is_rsgd=True)
    
    args = parser.parse_args()
    
    assert args.cycle % 2 == 0, 'Cycle length should be even'
    
    os.makedirs(args.save_dir, exist_ok=True)
    with open(os.path.join(args.dir, 'fge.sh'), 'w') as f:
        f.write(' '.join(sys.argv))
        f.write('\n')
    
    data_path = args.dir
    batch_size = args.batch_size
    seed = args.seed
    
    torch.backends.cudnn.benchmark = True
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    
    basic_transform = getattr(Transforms,args.dataset)
    
    loaders, num_classes = data_new.loaders(
        args.dataset,
        basic_transform,
        data_path,
        batch_size,
        4,
        args.use_test
    )
    
    model,checkpoint,criterion,start_epoch = initialize(args.model,args.ckpt,loaders,10,args.start_epoch,args.is_rsgd)

    optimizer = torch.optim.SGD(
        model.parameters(),
        lr=args.lr_1,
        momentum=args.momentum,
        weight_decay=args.wd
    )
    if args.is_rsgd:
        clean_optimizer(checkpoint[1])
        print(checkpoint[1]['state'].keys())
        optimizer.load_state_dict(checkpoint[1])
    else:
        optimizer.load_state_dict(checkpoint[1])
    
    ensemble_size = 0
    predictions_sum = np.zeros((len(loaders['test'].dataset), num_classes))
    
    columns = ['ep', 'lr', 'tr_loss', 'tr_acc', 'te_nll', 'te_acc', 'ens_acc', 'time']
    
    ep = []
    lr = []
    te_acc = []
    e_acc = []
    print(utils.test(loaders['test'], model, criterion))
    for epoch in range(args.epochs):
        time_ep = time.time()
        lr_schedule = utils.cyclic_learning_rate(epoch, args.cycle, args.lr_1, args.lr_2)
        train_res = utils.train(loaders['train'], model, optimizer, criterion, lr_schedule=lr_schedule)
        test_res = utils.test(loaders['test'], model, criterion)
        time_ep = time.time() - time_ep
        predictions, targets = utils.predictions(loaders['test'], model)
        ens_acc = None
        if (epoch % args.cycle + 1) == args.cycle // 2:
            ensemble_size += 1
            predictions_sum += predictions
            ens_acc = 100.0 * np.mean(np.argmax(predictions_sum, axis=1) == targets)
    
        if (epoch + 1) % (args.cycle // 2) == 0:
            utils.save_checkpoint(
                args.save_dir,
                start_epoch + epoch,
                name='fge',
                model_state=model.state_dict(),
                optimizer_state=optimizer.state_dict()
            )
    
        values = [epoch, lr_schedule(1.0), train_res['loss'], train_res['accuracy'], test_res['nll'],
                  test_res['accuracy'], ens_acc, time_ep]
        
        ep.append(epoch)
        lr.append(lr_schedule(1.0))
        te_acc.append(test_res['accuracy'])
        e_acc.append(ens_acc)
        
        table = tabulate.tabulate([values], columns, tablefmt='simple', floatfmt='9.4f')
        if epoch % 40 == 0:
            table = table.split('\n')
            table = '\n'.join([table[1]] + table)
        else:
            table = table.split('\n')[2]
        print(table)
        
    df = pd.DataFrame({'Epoch':ep,'Learning Rate':lr,'Test error':te_acc,'Ensembling error':e_acc})
    
    df.to_excel('{}/logFGE.xlsx'.format(args.save_dir))
        
        

if __name__ == '__main__':
    
    import argparse
    import numpy as np
    import os
    import sys
    import tabulate
    import pandas as pd
    import time
    import torch
    import torch.nn.functional as F
    import data_new
    import LeNet,RN_cifar
    import utils
    import Transforms
    import re
    
    main()
    