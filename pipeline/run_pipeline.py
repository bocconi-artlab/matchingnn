import os

def give_model_names(config, modeltype1):
    conf1 = config[0].replace("/", "_")
    if modeltype1 == "permuted":
        modeltype11 = ""
        modeltype2 = f"_permuted_{conf1}"
    elif modeltype1 == "permuted_normalized":
        modeltype11 = "_normalized"
        modeltype2 = f"_permuted_{conf1}_normalized"
    elif modeltype1 == "permuted_rescaled":
        modeltype11 = "_rescaled"
        modeltype2 = f"_permuted_{conf1}_rescaled"
    elif modeltype1 == "permuted_fromnormed":
        modeltype11 = ""
        modeltype2 = f"_permuted_{conf1}_fromnormed"
    else:
        modeltype11 = modeltype1
        modeltype2 = modeltype1
    return modeltype11, modeltype2

configurations=[["adam/1", "adam/2"],
                ["adam/1", "adam/3"],
                ["adam/2", "adam/3"],
                ["from_adv/1", "from_adv/2"],
                ["from_adv/1", "from_adv/3"],
                ["from_adv/2", "from_adv/3"],
                ["adam/1", "from_adv/2"],
                ["adam/2", "from_adv/1"],
                ["adam/3", "from_adv/3"],
                ["rsgd/1", "rsgd/2"],
                ["rsgd/1", "rsgd/3"],
                ["rsgd/2", "rsgd/3"],
                ["rsgd/1", "adam/2"],
                ["rsgd/1", "adam/1"],
                ["rsgd/3", "adam/3"],
                ["rsgd/1", "from_adv/2"],
                ["rsgd/2", "from_adv/1"],
                ["rsgd/3", "from_adv/3"],

                # SUPER-ADV (confusion_R=10)
                #["from_adv/4", "from_adv/6"], # from same init sgd vs adam
                #["from_adv/6", "from_adv/9"], # from different init adam vs sgd
                #["from_adv/4", "from_adv/9"], # from different init sgd vs sgd
                #["from_adv/10", "from_adv/11"], # sgd vs sgd earlystop
                #["from_adv/6", "from_adv/8"], # different init adam vs adam
                #["from_adv/4", "from_adv/12"], # different DATASET sgd vs sgd
                #["adam/1", "from_adv/6"],
                #["rsgd/1", "from_adv/6"],
                
                ]

save_normalized_checkpoint = False

nin = 784
#model = "MLP"; modeldir = "mlp_512_512/mnist"; dataset = "MNIST"
#model = "MLP"; modeldir = "mlp_512_512/fashion"; dataset = "FashionMNIST"
#model = "MLP"; modeldir = "mlp_512_512/cifar"; dataset = "CIFAR10"; nin=3072
#model = "TestLeNet"; modeldir = "testlenet/mnist"; dataset = "MNIST"
#model = "TestLeNet"; modeldir = "testlenet/fashion"; dataset = "FashionMNIST"
#model = "TestLeNet"; modeldir = "testlenet/cifar"; dataset = "CIFAR10"
model = "VGG16bn"; modeldir = "vgg16_bn/cifar"; dataset = "CIFAR10"

epochs = 0
opt = "cosine"; lr = 0.02

#modeltypes = ["", "_normalized", "permuted", "permuted_normalized"]
#modeltypes = ["permuted", "permuted_fromnormed", "_rescaled", "permuted_rescaled"]
#modeltypes = ["permuted", "permuted_fromnormed", "_rescaled", "permuted_rescaled"]
modeltypes = ["", "permuted", "permuted_fromnormed"]
modeltypes = [modeltypes[1], modeltypes[0]]
modeltypes = ["", "permuted"]
modeltypes = ["permuted"]
modeltypes = ["", "permuted", "permuted_normalized"]
modeltypes = ["", "permuted", "permuted_fromnormed"]
modeltypes = ["permuted"]

gpu = 2

for modeltype1 in modeltypes:
    for config in configurations:
        modeltype11, modeltype2 = give_model_names(config, modeltype1)
        cmd = f"python training_LeNet.py --dir=curves/ --dataset={dataset} --nin={nin} \
        --init_s=../configurations/configs/{modeldir}/{config[0]}/model_final{modeltype11}.pt \
        --init_e=../configurations/configs/{modeldir}/{config[1]}/model_final{modeltype2}.pt \
        --model={model} --is_esgd --epochs={epochs} --gpu={gpu} --mtype={modeltype1} --opt={opt} --lr={lr} \
        --save_normalized_checkpoint={save_normalized_checkpoint}"
        os.system(cmd)

#os.system("wait")

#for config in configurations:
#    for modeltype1 in modeltypes:
#        conf1 = config[0].replace("/", "_")
#        conf2 = config[1].replace("/", "_")
#        modeldirpl = modeldir.replace("/", "_")
#        modeltype11, modeltype2 = give_model_names(config, modeltype1)
#        cmd = f"python Evaluation.py --dir=curves/ --dataset={dataset} --nin={nin} \
#        --ckpt=curves/checkpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt \
#        --model={model} --gpu={gpu}\
#        --save_dir=curves/DIRcheckpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"
#        os.system(cmd)
