import torchvision.transforms as transforms

width_crop = 32
padding_crop = 4 



FashionMNIST = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.2860], std=[0.3530])
            ])

MNIST = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.1307], std=[0.3081])
            ])  

CIFAR10 = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.4914, 0.4822, 0.4465], std=[0.2023, 0.1994, 0.2010])
            ]) 





