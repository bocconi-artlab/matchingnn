from torch._C import FileCheck
import torch.nn as nn
from torch.nn import Linear, Sequential, ReLU, Tanh, Conv2d, Flatten, MaxPool2d, Module, BatchNorm2d
import numpy as np
import curves


# test lenet used in test3 
# modified from https://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html
class TestLeNetBase(nn.Module):

    def __init__(self):
        super(TestLeNetBase, self).__init__()

        conv1 = nn.Conv2d(1, 6, 5, 1)
        conv2 = nn.Conv2d(6, 16, 5, 1)
        maxp = nn.MaxPool2d(2, stride=2, padding=0, dilation=1)
        # an affine operation: y = Wx + b
        fc1 = nn.Linear(16 * 4 * 4, 120)  # 5*5 from image dimension
        fc2 = nn.Linear(120, 84)
        fc3 = nn.Linear(84, 10)


        module_list = [conv1, nn.ReLU(), maxp, conv2, nn.ReLU(), maxp, nn.Flatten(), fc1, nn.ReLU(), fc2, nn.ReLU(), fc3]

        self.layers = nn.Sequential(*module_list)
    def forward(self, x):

        return self.layers(x)

class TestLeNetCurve(nn.Module):

    def __init__(self,fix_points):
        super(TestLeNetCurve, self).__init__()

        conv1 = curves.Conv2d(1, 6, 5, stride=1,fix_points=fix_points)
        conv2 = curves.Conv2d(6, 16, 5, stride=1,fix_points=fix_points)
        maxp = nn.MaxPool2d(2, stride=2, padding=0, dilation=1)
        # an affine operation: y = Wx + b
        fc1 = curves.Linear(16 * 4 * 4, 120,fix_points=fix_points)  # 5*5 from image dimension
        fc2 = curves.Linear(120, 84, fix_points=fix_points)
        fc3 = curves.Linear(84, 10, fix_points=fix_points)


        module_list = [conv1, nn.ReLU(), maxp, conv2, nn.ReLU(), maxp, nn.Flatten(), fc1, nn.ReLU(), fc2, nn.ReLU(), fc3]

        self.layers = nn.Sequential(*module_list)

    def forward(self, x,coeffs_t):

        for i in range(len(self.layers)):
            if type(self.model[i]) not in [Linear, Conv2d, BatchNorm2d, curves.Linear, curves.Conv2d, curves.BatchNorm2d]:
                x = self.layers[i](x)
            else:
                x = self.layers[i](x,coeffs_t)
        return x


class TestLeNet:
    base = TestLeNetBase
    curve = TestLeNetCurve
    kwargs = {}
