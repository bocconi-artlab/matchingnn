from torch._C import FileCheck
import torch.nn as nn
from torch.nn import Linear, Sequential, ReLU, Tanh, Conv2d, Flatten, MaxPool2d, Module, BatchNorm2d
import numpy as np
import curves


# test lenet used in test3 
# modified from https://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html
class TestLeNetBase(nn.Module):

    def __init__(self, ncolors, outvolume):
        super(TestLeNetBase, self).__init__()

        #ncolors, outvolume = 1, 800
        #ncolors, outvolume = 3, 1250

        conv1 = nn.Conv2d(ncolors, 20, 5, 1)
        conv2 = nn.Conv2d(20, 50, 5, 1)
        maxp = nn.MaxPool2d(2, stride=2, padding=0, dilation=1)
        # an affine operation: y = Wx + b
        fc1 = nn.Linear(outvolume, 500)  # 5*5 from image dimension
        fc2 = nn.Linear(500, 10)

        module_list = [conv1, nn.ReLU(), maxp, conv2, nn.ReLU(), maxp, nn.Flatten(), fc1, nn.ReLU(), fc2]

        self.model = nn.Sequential(*module_list)

    def forward(self, x):

        return self.model(x)

class TestLeNetCurve(nn.Module):

    def __init__(self, ncolors, outvolume, fix_points):
        super(TestLeNetCurve, self).__init__()

        #ncolors, outvolume = 1, 800
        #ncolors, outvolume = 3, 1250

        conv1 = curves.Conv2d(ncolors, 20, 5, stride=1, fix_points=fix_points)
        conv2 = curves.Conv2d(20, 50, 5, stride=1, fix_points=fix_points)
        maxp = nn.MaxPool2d(2, stride=2, padding=0, dilation=1)
        # an affine operation: y = Wx + b
        fc1 = curves.Linear(outvolume, 500, fix_points=fix_points)  # 5*5 from image dimension
        fc2 = curves.Linear(500, 10, fix_points=fix_points)

        module_list = [conv1, nn.ReLU(), maxp, conv2, nn.ReLU(), maxp, nn.Flatten(), fc1, nn.ReLU(), fc2]

        self.model = nn.Sequential(*module_list)

    def forward(self, x,coeffs_t):

        for i in range(len(self.model)):
            if type(self.model[i]) not in [Linear, Conv2d, BatchNorm2d, curves.Linear, curves.Conv2d, curves.BatchNorm2d]:
                x = self.model[i](x)
            else:
                x = self.model[i](x,coeffs_t)
        return x


class TestLeNet:
    base = TestLeNetBase
    curve = TestLeNetCurve
    kwargs = {}
