import torch
from copy import deepcopy
import os, sys

matchingnn_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if not matchingnn_dir in sys.path:
    sys.path.insert(0, matchingnn_dir)
from src.normalize import normalize_model
from src.matching import match_sequential

def normalize_model_in_curve(model, m_to_norm, modelname, mtype, 
                             normalize_endpoints=False, normalize_last_layer=True,
                             permute_endpoints=True, normalize_midpoint=True):

    with torch.no_grad():

        model_normalized = deepcopy(model)
        m_to_norm_end1 = deepcopy(m_to_norm)
        m_to_norm_end2 = deepcopy(m_to_norm)
        parameters_norm_end1 = list(m_to_norm_end1.parameters())
        parameters_norm_end2 = list(m_to_norm_end2.parameters())

        if mtype in ["_normalized", "permuted_normalized", "_rescaled", "permuted_rescaled"] or normalize_endpoints:

            # get nets
            parameters = list(model_normalized.net.parameters())
            parameters_norm = list(m_to_norm.parameters())
            j = 0
            for i in range(0, len(parameters), model_normalized.num_bends):
                weights = parameters[i:i+model_normalized.num_bends]
                parameters_norm[j].data.copy_(weights[1].data)
                if normalize_endpoints:
                    parameters_norm_end1[j].data.copy_(weights[0].data)
                    parameters_norm_end2[j].data.copy_(weights[-1].data)
                j += 1
            if modelname in ["MLP"]:
                net1 = m_to_norm.layers
            elif modelname in ["TestLeNet", "VGG16bn"]:
                net1 = m_to_norm.model
            # normalize
            if normalize_midpoint:
                net1 = normalize_model(net1, normalize_last_layer=normalize_last_layer)
            if modelname in ["MLP"]:
                m_to_norm.layers = net1
            elif modelname in ["TestLeNet", "VGG16bn"]:
                m_to_norm.model = net1

            if normalize_endpoints:
                if modelname in ["MLP"]:
                    net1 = m_to_norm_end1.layers
                    net2 = m_to_norm_end2.layers
                elif modelname in ["TestLeNet", "VGG16bn"]:
                    net1 = m_to_norm_end1.model
                    net2 = m_to_norm_end2.model
                net1 = normalize_model(net1, normalize_last_layer=normalize_last_layer)
                net2 = normalize_model(net2, normalize_last_layer=normalize_last_layer)
                if modelname in ["MLP"]:
                    m_to_norm_end1.layers = net1
                    m_to_norm_end2.layers = net2
                elif modelname in ["TestLeNet", "VGG16bn"]:
                    m_to_norm_end1.model = net1
                    m_to_norm_end2.model = net2

                parameters_norm_end1 = list(m_to_norm_end1.parameters())
                parameters_norm_end2 = list(m_to_norm_end2.parameters())

            # put nets back into the checkpoint
            parameters = list(model_normalized.net.parameters())
            parameters_norm = list(m_to_norm.parameters())
            j = 0
            for i in range(0, len(parameters), model_normalized.num_bends):
                weights = parameters[i:i+model_normalized.num_bends]
                weights[1].data.copy_(parameters_norm[j].data)
                if normalize_endpoints:
                    weights[0].data.copy_(parameters_norm_end1[j].data)
                    weights[-1].data.copy_(parameters_norm_end2[j].data)
                j += 1

        if permute_endpoints:

            parameters_norm_end1 = list(m_to_norm_end1.parameters())
            parameters_norm_end2 = list(m_to_norm_end2.parameters())
            parameters = list(model_normalized.net.parameters())
            parameters_norm = list(m_to_norm.parameters())
            j = 0
            for i in range(0, len(parameters), model_normalized.num_bends):
                weights = parameters[i:i+model_normalized.num_bends]
                parameters_norm[j].data.copy_(weights[1].data)
                parameters_norm_end1[j].data.copy_(weights[0].data)
                parameters_norm_end2[j].data.copy_(weights[-1].data)
                j += 1

            if modelname in ["MLP"]:
                net1 = m_to_norm_end1.layers
                net2 = m_to_norm_end2.layers
                net_mid = m_to_norm.layers
            elif modelname in ["TestLeNet", "VGG16bn"]:
                net1 = m_to_norm_end1.model
                net2 = m_to_norm_end2.model
                net_mid = m_to_norm.model

            _, net1 = match_sequential(net_mid, net1)
            _, net2 = match_sequential(net_mid, net2)

            if modelname in ["MLP"]:
                m_to_norm_end1.layers = net1
                m_to_norm_end2.layers = net2
            elif modelname in ["TestLeNet", "VGG16bn"]:
                m_to_norm_end1.model = net1
                m_to_norm_end2.model = net2

            parameters_norm_end1 = list(m_to_norm_end1.parameters())
            parameters_norm_end2 = list(m_to_norm_end2.parameters())
            j = 0
            for i in range(0, len(parameters), model_normalized.num_bends):
                weights = parameters[i:i+model_normalized.num_bends]
                weights[0].data.copy_(parameters_norm_end1[j].data)
                weights[-1].data.copy_(parameters_norm_end2[j].data)
                j += 1

        return model_normalized