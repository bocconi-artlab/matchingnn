def clean_dic(c):

    checkpoint={}

    for i in c.keys():
        print(i)
        temp2 = re.sub("replicas.0.","",i)
        #temp2 = re.sub("model","layers",temp2)
        checkpoint[temp2] = c[i]

    return checkpoint

def main():
    
    parser = argparse.ArgumentParser(description='Convert ESGD')
    parser.add_argument('--dir',required = True, type=str, default=None, metavar='DIR',
                        help='training directory (default: None')

    args = parser.parse_args()
    data_path = args.dir
    check = torch.load(data_path + "/model_final.pt")
    check2 = clean_dic(check)
    torch.save(check2,data_path + "/model_final_clean.pt")

if __name__ == '__main__':
    
    import torch
    import re
    import argparse
    
    main()
    
