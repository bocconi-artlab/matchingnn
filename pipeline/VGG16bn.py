from torch._C import FileCheck
import torch
import torch.nn as nn
from torch.nn import Linear, Sequential, ReLU, Tanh, Conv2d, Flatten, MaxPool2d, Module, BatchNorm2d
import numpy as np
import curves

from typing import Union, List, Dict, Any, cast

import torch
import torch.nn as nn

class VGGBase(nn.Module):
    model_name='vgg16_bn'
    def __init__(self, features: nn.Module, num_classes: int = 10, init_weights: bool = True) -> None:
        super(VGGBase, self).__init__()

        self.features = features
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))
        self.classifier = [
            nn.Linear(512 * 7 * 7, 4096),
            nn.ReLU(True),
            nn.Linear(4096, 4096),
            nn.ReLU(True),
            nn.Linear(4096, num_classes),
        ]

        module_list = [*self.features, self.avgpool, nn.Flatten(), *self.classifier]

        self.model = nn.Sequential(*module_list)

    def forward(self, x: torch.Tensor) -> torch.Tensor:

        return self.model(x)


def make_layers(cfg: List[Union[str, int]], batch_norm: bool = False) -> nn.Sequential:
    layers: List[nn.Module] = []
    in_channels = 3
    for v in cfg:
        if v == "M":
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            v = cast(int, v)
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return layers
    #return nn.Sequential(*layers)


cfgs: Dict[str, List[Union[str, int]]] = {
    "A": [64, "M", 128, "M", 256, 256, "M", 512, 512, "M", 512, 512, "M"],
    "B": [64, 64, "M", 128, 128, "M", 256, 256, "M", 512, 512, "M", 512, 512, "M"],
    "D": [64, 64, "M", 128, 128, "M", 256, 256, 256, "M", 512, 512, 512, "M", 512, 512, 512, "M"],
    "E": [64, 64, "M", 128, 128, "M", 256, 256, 256, 256, "M", 512, 512, 512, 512, "M", 512, 512, 512, 512, "M"],
}


def _vgg(arch: str, cfg: str, batch_norm: bool, pretrained: bool, progress: bool, **kwargs: Any) -> VGGBase:
    #if pretrained:
    #    kwargs["init_weights"] = False
    model = VGGBase(make_layers(cfgs[cfg], batch_norm=batch_norm), **kwargs)
    #if pretrained:
    #    state_dict = load_state_dict_from_url(model_urls[arch], progress=progress)
    #    model.load_state_dict(state_dict)
    return model

def vgg16(pretrained: bool = False, progress: bool = True, **kwargs: Any) -> VGGBase:
    r"""VGG 16-layer model (configuration "D")
    """
    return _vgg("vgg16", "D", False, pretrained, progress, **kwargs)


def vgg16_bn(pretrained: bool = False, progress: bool = True, **kwargs: Any) -> VGGBase:
    r"""VGG 16-layer model (configuration "D") with batch normalization
    """
    return _vgg("vgg16_bn", "D", True, pretrained, progress, **kwargs)

#*#

class VGGCurve(nn.Module):
    def __init__(self, features: nn.Module, num_classes: int = 10, init_weights: bool = True, fix_points=None) -> None:
        super(VGGCurve, self).__init__()

        self.features = features
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))
        self.classifier = [
            curves.Linear(512 * 7 * 7, 4096, fix_points=fix_points),
            nn.ReLU(True),
            curves.Linear(4096, 4096, fix_points=fix_points),
            nn.ReLU(True),
            curves.Linear(4096, num_classes, fix_points=fix_points),
        ]

        module_list = [*self.features, self.avgpool, nn.Flatten(), *self.classifier]

        self.model = nn.Sequential(*module_list)

    def forward(self, x: torch.Tensor, coeffs_t) -> torch.Tensor:

        #return self.model(x)

        for i in range(len(self.model)):
            if type(self.model[i]) not in [Linear, Conv2d, BatchNorm2d, curves.Linear, curves.Conv2d, curves.BatchNorm2d, curves._BatchNorm]:
                x = self.model[i](x)
                #print(type(self.model[i]))
            else:
                x = self.model[i](x, coeffs_t)
        return x



def make_layers_curves(cfg: List[Union[str, int]], batch_norm: bool = False, fix_points=None) -> nn.Sequential:
    layers: List[nn.Module] = []
    in_channels = 3
    for v in cfg:
        if v == "M":
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            v = cast(int, v)
            conv2d = curves.Conv2d(in_channels, v, kernel_size=3, padding=1, fix_points=fix_points)
            if batch_norm:
                layers += [conv2d, curves.BatchNorm2d(v, fix_points), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return layers
    #return nn.Sequential(*layers)

def _vgg_curves(arch: str, cfg: str, batch_norm: bool, pretrained: bool, progress: bool, fix_points=None, **kwargs: Any) -> VGGCurve:
    #if pretrained:
    #    kwargs["init_weights"] = False
    model = VGGCurve(make_layers_curves(cfgs[cfg], batch_norm=batch_norm, fix_points=fix_points), fix_points=fix_points, **kwargs)
    #if pretrained:
    #    state_dict = load_state_dict_from_url(model_urls[arch], progress=progress)
    #    model.load_state_dict(state_dict)
    return model

def vgg16_curves(pretrained: bool = False, progress: bool = True, **kwargs: Any) -> VGGCurve:
    r"""VGG 16-layer model (configuration "D")
    """
    return _vgg_curves("vgg16", "D", False, pretrained, progress, **kwargs)


def vgg16_bn_curves(pretrained: bool = False, progress: bool = True, fix_points=None,  **kwargs: Any) -> VGGCurve:
    r"""VGG 16-layer model (configuration "D") with batch normalization
    """
    return _vgg_curves("vgg16_bn", "D", True, pretrained, progress, fix_points=fix_points, **kwargs)


class VGG16bn:
    base = vgg16_bn
    curve = vgg16_bn_curves
    kwargs = {}
