"""
    PreResNet model definition
    ported from https://github.com/bearpaw/pytorch-classification/blob/master/models/cifar/preresnet.py
"""

import math
import torch.nn as nn
import torch

import curves18

def conv3x3(in_planes, out_planes, stride=1):
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)

def conv1x1(in_planes, out_planes, stride = 1):
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


def conv3x3curve(in_planes, out_planes, fix_points, stride=1):
    return curves18.Conv2d(in_planes, out_planes, kernel_size=3, fix_points=fix_points, stride=stride,
                         padding=1, bias=False)

def conv1x1curve(in_planes, out_planes, fix_points, stride = 1):
    """1x1 convolution"""
    return curves18.Conv2d(in_planes, out_planes, kernel_size=1,fix_points = fix_points, stride=stride,bias=False)


class BasicBlock(nn.Module):
    expansion = 1
    def __init__(
        self,
        inplanes,
        planes,
        stride = 1,
        downsample = None,
        groups = 1,
        base_width = 64,
        dilation = 1,
        norm_layer = None
    ):
        super(BasicBlock, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        if groups != 1 or base_width != 64:
            raise ValueError('BasicBlock only supports groups=1 and base_width=64')
        if dilation > 1:
            raise NotImplementedError("Dilation > 1 not supported in BasicBlock")
        # Both self.conv1 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = norm_layer(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = norm_layer(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out



class BasicBlockCurve(nn.Module):
    expansion = 1

    def __init__(
        self,
        inplanes,
        planes,
        fix_points,
        stride = 1,
        downsample = None,
        groups = 1,
        base_width = 64,
        dilation = 1,
        norm_layer = None
    ):
        super(BasicBlockCurve, self).__init__()
        if norm_layer is None:
            norm_layer = curves18.BatchNorm2d
        if groups != 1 or base_width != 64:
            raise ValueError('BasicBlock only supports groups=1 and base_width=64')
        if dilation > 1:
            raise NotImplementedError("Dilation > 1 not supported in BasicBlock")
        # Both self.conv1 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv3x3curve(inplanes, planes, fix_points = fix_points)
        self.bn1 = norm_layer(planes,fix_points=fix_points)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3curve(planes, planes, fix_points = fix_points)
        self.bn2 = norm_layer(planes,fix_points = fix_points)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x, coeffs_t):
        identity = x

        out = self.conv1(x,coeffs_t)
        out = self.bn1(out,coeffs_t)
        out = self.relu(out)

        out = self.conv2(out,coeffs_t)
        out = self.bn2(out,coeffs_t)

        if self.downsample is not None:
            identity = self.downsample(x,coeffs_t)
        out += identity
        out = self.relu(out)

        return out


class ResNetBase(nn.Module):

    def __init__(
        self,
        block,
        layers,
        num_classes: int = 1000,
        zero_init_residual: bool = False,
        groups: int = 1,
        width_per_group: int = 64,
        replace_stride_with_dilation = None,
        norm_layer = None
    ):
        super(ResNetBase, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        self._norm_layer = norm_layer

        self.inplanes = 64
        self.dilation = 1
        if replace_stride_with_dilation is None:
            # each element in the tuple indicates if we should replace
            # the 2x2 stride with a dilated convolution instead
            replace_stride_with_dilation = [False, False, False]
        if len(replace_stride_with_dilation) != 3:
            raise ValueError("replace_stride_with_dilation should be None "
                             "or a 3-element tuple, got {}".format(replace_stride_with_dilation))
        self.groups = groups
        self.base_width = width_per_group
        self.conv1 = nn.Conv2d(3, self.inplanes, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = norm_layer(self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=1,
                                       dilate=replace_stride_with_dilation[0])
        self.layer3 = self._make_layer(block, 256, layers[2], stride=1,
                                       dilate=replace_stride_with_dilation[1])
        self.layer4 = self._make_layer(block, 512, layers[3], stride=1,
                                       dilate=replace_stride_with_dilation[2])
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def _make_layer(self, block, planes, blocks,
                    stride = 1, dilate = False):
        norm_layer = self._norm_layer
        downsample = None
        previous_dilation = self.dilation
        if dilate:
            self.dilation *= stride
            stride = 1
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = conv1x1(self.inplanes, planes * block.expansion, stride)
        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample, self.groups,
                            self.base_width, previous_dilation, norm_layer))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes, groups=self.groups,
                                base_width=self.base_width, dilation=self.dilation,
                                norm_layer=norm_layer))

        return nn.Sequential(*layers)

    def _forward_impl(self, x):
        # See note [TorchScript super()]
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)

        return x

    def forward(self, x):
        return self._forward_impl(x)
    

class ResNetCurve(nn.Module):

    def __init__(
        self,
        block,
        layers,
        fix_points,
        num_classes = 10,
        zero_init_residual = False,
        groups = 1,
        width_per_group = 64,
        replace_stride_with_dilation = None,
        norm_layer = None
    ):
        super(ResNetCurve, self).__init__()
        kwargs = dict()
        if fix_points is not None:
            kwargs['fix_points'] = fix_points
        if norm_layer is None:
            norm_layer = curves18.BatchNorm2d
        self._norm_layer = norm_layer

        self.inplanes = 64
        self.dilation = 1
        if replace_stride_with_dilation is None:
            # each element in the tuple indicates if we should replace
            # the 2x2 stride with a dilated convolution instead
            replace_stride_with_dilation = [False, False, False]
        if len(replace_stride_with_dilation) != 3:
            raise ValueError("replace_stride_with_dilation should be None "
                             "or a 3-element tuple, got {}".format(replace_stride_with_dilation))

        self.groups = groups
        self.base_width = width_per_group
        self.conv1 = curves18.Conv2d(3, self.inplanes, kernel_size=7, stride=2, padding=3,
                               bias=False,fix_points = fix_points)
        self.bn1 = norm_layer(self.inplanes,fix_points = fix_points)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0],fix_points = fix_points)
        self.layer2 = self._make_layer(block, 128, layers[1], stride=1,
                                       dilate=replace_stride_with_dilation[0],fix_points = fix_points)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=1,
                                       dilate=replace_stride_with_dilation[1],fix_points = fix_points)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=1,
                                       dilate=replace_stride_with_dilation[2],fix_points = fix_points)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = curves18.Linear(512 * block.expansion, num_classes,fix_points = fix_points)

        for m in self.modules():
            if isinstance(m, curves18.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                for i in range(m.num_bends):
                    getattr(m, 'weight_%d' % i).data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, curves18.BatchNorm2d):
                for i in range(m.num_bends):
                    getattr(m, 'weight_%d' % i).data.fill_(1)
                    getattr(m, 'bias_%d' % i).data.zero_()

        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves like an identity.
        # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, BasicBlockCurve):
                    nn.init.constant_(m.bn2.weight_t, 0)  # type: ignore[arg-type]

    def _make_layer(self, block, planes, blocks,fix_points,
                    stride = 1, dilate = False):
        norm_layer = self._norm_layer
        downsample = None
        previous_dilation = self.dilation
        if dilate:
            self.dilation *= stride
            stride = 1
        if self.inplanes != planes * block.expansion:
            downsample = curves18.Conv2d(self.inplanes, planes * block.expansion, kernel_size=1,
                                       stride=stride, bias=False, fix_points=fix_points)

        layers = []
        layers.append(block(self.inplanes, planes,fix_points = fix_points, stride = stride, downsample = downsample, groups = self.groups,
                            base_width  = self.base_width, dilation = previous_dilation, norm_layer = norm_layer))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes,fix_points = fix_points, groups=self.groups,
                                base_width=self.base_width, dilation=self.dilation,
                                norm_layer=norm_layer))

        return nn.Sequential(*layers)

    def _forward_impl(self, x, coeffs_t):
        x = self.conv1(x, coeffs_t)
        x = self.bn1(x, coeffs_t)
        x = self.relu(x)
        x = self.maxpool(x)
    
        for block in self.layer1:  # 32x32
            x = block(x,coeffs_t)
        for block in self.layer2:  # 16x16
            x = block(x, coeffs_t)
        for block in self.layer3:  # 8x8
            x = block(x, coeffs_t)
        #x = self.bn(x, coeffs_t)
        for block in self.layer4:
            x = block(x, coeffs_t)
        #x = self.relu(x)
    
        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x, coeffs_t)

        return x

    def forward(self, x,coeffs_t):
        return self._forward_impl(x,coeffs_t)


"""class ResNetCurve(nn.Module):

    def __init__(self, num_classes, fix_points, depth=110):
        super(ResNetCurve, self).__init__()
        n = 2
        block = BasicBlockCurve
        
        self.inplanes = 64
        self.conv1 = curves.Conv2d(3, 64, kernel_size=7,stride=2, padding=3,
                                   bias=False, fix_points=fix_points)
        self.bn1 = curves.BatchNorm2d(self.inplanes, fix_points=fix_points)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3,stride=2,padding=1)
        self.layer1 = self._make_layer(block,64, n)
        self.layer2 = self._make_layer(block, 128, n, stride=2,fix_points = fix_points)
        self.layer3 = self._make_layer(block, 256, n, stride=2,fix_points = fix_points)
        self.layer4 = self._make_layer(block, 512, n, stride=2,fix_points = fix_points)
        #self.bn = curves.BatchNorm2d(64 * block.expansion, fix_points=fix_points)
        #self.relu = nn.ReLU(inplace=True)
        self.avgpool = nn.AdaptiveAvgPool2d((1,1))
        self.fc = curves.Linear(512 * block.expansion, num_classes, fix_points=fix_points)

        for m in self.modules():
            if isinstance(m, curves.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                for i in range(m.num_bends):
                    getattr(m, 'weight_%d' % i).data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, curves.BatchNorm2d):
                for i in range(m.num_bends):
                    getattr(m, 'weight_%d' % i).data.fill_(1)
                    getattr(m, 'bias_%d' % i).data.zero_()

    def _make_layer(self, block, planes, blocks, fix_points, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = curves.Conv2d(self.inplanes, planes * block.expansion, kernel_size=1,
                                       stride=stride, bias=False, fix_points=fix_points)

        layers = list()
        layers.append(block(self.inplanes, planes, fix_points=fix_points, stride=stride,
                            downsample=downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes, fix_points=fix_points))

        return nn.ModuleList(layers)

    def forward(self, x, coeffs_t):
        x = self.conv1(x, coeffs_t)
        x = self.bn(x, coeffs_t)
        x = self.relu(x)

        for block in self.layer1:  # 32x32
            x = block(x, coeffs_t)
        for block in self.layer2:  # 16x16
            x = block(x, coeffs_t)
        for block in self.layer3:  # 8x8
            x = block(x, coeffs_t)
        #x = self.bn(x, coeffs_t)
        for block in self.layer4:
            x = block(x, coeffs_t)
        #x = self.relu(x)

        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x, coeffs_t)

        return x"""


class ResNet18:
    base = ResNetBase
    curve = ResNetCurve
    kwargs = {'block':BasicBlockCurve,'layers':[2,2,2,2]}