def compose_state_dict(checkpoint,center):
    state_dict={}
    if center:
        for i in checkpoint.keys():
            if re.match('center.',i):                           
                temp2 = re.sub("center.","",i)
                state_dict[temp2] = checkpoint[i] 
            else:
                continue
    else:
        for i in checkpoint.keys():
            if re.match('replicas.0.',i):                           
                temp2 = re.sub("replicas.0.","",i)
                state_dict[temp2] = checkpoint[i] 
            else:
                continue
    return state_dict

def import_model(model_name,checkpoint,n_c,i_c,d):
    if model_name == 'ResNet20':
        base_model = RN_cifar.ResNetBase()
    else:
        base_model = LeNet.LeNetBase(nclasses=n_c,in_channels = i_c,dim=d)

    base_model.load_state_dict(checkpoint)
    base_model = base_model.cuda()
    
    return base_model


def train_hyper(model,loss):
    criterion = getattr(F,loss)
    return criterion

def perturb_efficient(model_name,check_0,sigma,noise,i_c,d):
    check = check_0.copy()
    model_c = import_model(model_name,check,10,i_c,d)
    z = []
    for i in model_c.parameters():
        r = i.clone().detach().normal_()
        if noise == 'add':
            z.append(r)
        else:
            z.append(i.data*r)
    with torch.no_grad():
        for i,p in enumerate(model_c.parameters()):
            p.data += sigma * z[i]           
    return model_c


def compute_sharpness(model_name,check,m_c,center,loaders):
    dim = loaders['train'].dataset[0][0].shape[2]
    in_channels = loaders['train'].dataset[0][0].shape[0]
    sigma_grid = list(np.arange(0,0.12,0.02)) 
    state_dict = compose_state_dict(check,center)
    model_0 = import_model(model_name,state_dict,10,in_channels,dim)
    results = {}
    crit_0 = train_hyper(model_0,'cross_entropy')
    print(crit_0)
    train_res_0 = utils.test(loaders['train'], model_0,crit_0)
    temp = []
    m=0
    
    for s in sigma_grid:
        print(s)
        for it in range(m_c):
            model = perturb_efficient(model_name,state_dict,s,'None',in_channels,dim)
            crit = train_hyper(model,'cross_entropy')
            train_res = utils.test(loaders['train'], model,crit)
            temp.append(train_res['accuracy'])
        m += abs(np.mean(temp) - train_res_0['accuracy'])
        results.update({s:m})
        temp = []
        
    print(results)
        
    return results

def make_dir(s):
    sigmas = []
    values = []
    
    for i in range(len(list(s.keys()))):
        sigmas.append(list(s.keys())[i])
        values.append(s[list(s.keys())[i]])
        
    df = pd.DataFrame({'Sigma':sigmas,'Value':values})
    
    return df

def main():
    parser = argparse.ArgumentParser(description='Sharpness Evaluation')
    parser.add_argument('--dir',required = True, type=str, default=None, metavar='DIR',
                        help='training directory (default: None')
    parser.add_argument('--dataset', type=str, default='FashionMNIST', metavar='DATASET',
                        help='dataset name (default: FashionMNIST)')
    parser.add_argument('--init_s',required = True, type=str, default=None, metavar='CKPT',
                        help='checkpoint to init start point (default: None)')
    parser.add_argument('--init_e',required = True, type=str, default=None, metavar='CKPT',
                        help='checkpoint to init end point (default: None)')
    parser.add_argument('--init_d',required = True, type=str, default=None, metavar='CKPT',
                        help='checkpoint to init end point (default: None)')
    parser.add_argument('--mc', type=int, default=10, metavar='N',
                        help='Monte Carlo (default: 10)')
    parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                        help='input batch size (default: 128)')
    parser.add_argument('--seed', type=int, default=42, metavar='S', help='random seed (default: 42)')
    parser.add_argument('--model', type=str, default='LeNet', metavar='MODEL',
                        help='model name (default: LeNet)')
    parser.add_argument('--save_dir',required = True, type=str, default=None, metavar='DIR',
                            help='training directory (default: None')
    parser.add_argument('--center', dest='is_rsgd', action='store_false',
                   help='turns off if_rsgd (default: on)')
    
    parser.set_defaults(center=False)
    
    args = parser.parse_args()
    
    data_path = args.dir
    batch_size = args.batch_size
    random_seed = args.seed
    
    torch.backends.cudnn.benchmark = True ## Choose the fastest convolution algorithm
    torch.manual_seed(random_seed) ## Set random_seed to reproduce results
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
    
    basic_transform = getattr(Transforms,args.dataset)
    
    loaders, num_classes = data_new.loaders(
        args.dataset,
        basic_transform,
        data_path,
        batch_size,
        dl = True,
        use_test=True,
        num_workers = 3
    )
    
    path = args.init_s
    path2 = args.init_e
    path3 = args.init_d
    checkpoint = torch.load(path)[0]
    checkpoint2 = torch.load(path2)[0]
    checkpoint3 = torch.load(path3)[0]
    

    start = timeit.default_timer()
    
    r3 = compute_sharpness(args.model,checkpoint3,args.mc,args.center,loaders)
    r = compute_sharpness(args.model,checkpoint,args.mc,args.center,loaders)
    r2 = compute_sharpness(args.model,checkpoint2,args.mc,args.center,loaders)
    #r3 = compute_sharpness(args.model,checkpoint3,args.mc,args.center,loaders)
    
    make_dir(r).to_excel('{}/Sharpness_RSGD.xlsx'.format(args.save_dir),index=False)
    make_dir(r2).to_excel('{}/Sharpness_SGD.xlsx'.format(args.save_dir),index=False)
    make_dir(r3).to_excel('{}/Sharpness_SGD.xlsx'.format(args.save_dir),index=False)

    stop = timeit.default_timer()
    print('Time: ', (stop - start)/60)  
    
    plt.figure(figsize=(12.4, 7))

    plt.plot(r.keys(),r.values(),lw=3,marker='o')
    plt.plot(r2.keys(),r2.values(),lw=3,marker='o')
    plt.plot(r3.keys(),r3.values(),lw=3,marker='o')
    plt.xlabel('$\sigma$',fontsize=20)
    plt.ylabel('Train Error',fontsize=20)
    plt.legend(['r-SGD','SGD','e-SGD'],prop={'size':20})
    plt.grid()
    plt.savefig("{}/Sharpness_Measure.pdf".format(args.save_dir), format='pdf', bbox_inches='tight')
    plt.show()
    
if __name__ == '__main__':
    
    import timeit
    import torch
    import torch.nn.functional as F
    import matplotlib.pyplot as plt
    import re
    import numpy as np
    import LeNet,RN_cifar
    import utils
    import data_new
    import Transforms
    import argparse
    import pandas as pd
    from multiprocessing import set_start_method    
    
    set_start_method('spawn')
    
    main()



