import torch
import torch.nn.functional as F
import json
import os

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')

import RN_cifar, MLP, TestLeNet, VGG16bn
import curves

def initializeTest(model_name, curve, num_bends):
    architecture = getattr(TestLeNet, model_name)
    if curve is None:
        model = architecture().base()
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetTest(
            curve,
            architecture.curve,
            num_bends,
            architecture_kwargs=architecture.kwargs
        )
    return model

def initializeMLP(model_name, nin, nhs, num_classes, curve, num_bends):
    architecture = getattr(MLP, model_name)
    if curve is None:
        model = architecture().base(nin, nhs, num_classes)
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetMLP(
            num_classes,
            nin,
            nhs,
            curve,
            architecture.curve,
            num_bends,
            architecture_kwargs = architecture.kwargs
        )
    return model
    
def calculate_euclidean_distance(net1, net2, npars):
    dist = 0.0
    for w1, w2 in zip(net1.parameters(), net2.parameters()):
        dist += F.mse_loss(w1, w2, reduction='sum')
    dist /= npars
    return torch.sqrt(dist).item()

def distance_optimized_points(modeltype="MLP", dataset="MNIST", ckpt_before_opt="", ckpt_after_opt=""):

    curve = 'PolyChain'
    nin = 784 if dataset in ["MNIST", "FashionMNIST"] else 3072
    in_channels = 1 if dataset in ["mnist", "fashion"] else 3

    if modeltype=='MLP':
        model_before_opt = initializeMLP(modeltype, nin, [512,512], 10, curve, 3)
        model_after_opt = initializeMLP(modeltype, nin, [512,512], 10, curve, 3)
        model_original = initializeMLP(modeltype, nin, [512,512], 10, None, 3)
    elif modeltype=='TestLeNet':
        model_before_opt = initializeTest(modeltype, curve, 3)
        model_after_opt = initializeTest(modeltype, curve, 3)
        model_original = initializeTest(modeltype, None, 3)
    elif modeltype=='VGG16bn':
        model_before_opt = initializeVGG(modeltype, curve, 3)
        model_after_opt = initializeVGG(modeltype, curve, 3)
        model_original = initializeVGG(modeltype, None, 3)
    else:
        model = initialize(modeltype, curve, 10, in_channels, dim, 3)
        model_original = initialize(modeltype, None, 10, in_channels, dim, 3)
    
    checkpoint_before_opt = torch.load(ckpt_before_opt)
    checkpoint_after_opt = torch.load(ckpt_after_opt)

    model_before_opt.load_state_dict(checkpoint_before_opt['model_state'])
    model_after_opt.load_state_dict(checkpoint_after_opt['model_state'])

    model_before_opt.cuda()
    model_after_opt.cuda()

    # TODO: estrai i parametri dalla curva

    nbends = model_before_opt.num_bends
    parameters_before_opt = list(model_before_opt.net.parameters())
    parameters_after_opt = list(model_after_opt.net.parameters())

    dist, numel = 0, 0
    for i in range(0, len(parameters_before_opt), nbends):
        weights_before_opt = parameters_before_opt[i:i+nbends]
        weights_before_opt = weights_before_opt[1].data
        weights_after_opt = parameters_after_opt[i:i+nbends]
        weights_after_opt = weights_after_opt[1].data
        #print(weights_before_opt.size())
        numel_lay = weights_before_opt.numel()
        dist_lay = torch.sum((weights_before_opt - weights_after_opt)**2)
        dist += dist_lay
        numel += numel_lay
        #print(dist_lay, numel_lay)

    dist = torch.sqrt(dist/numel)

    return dist



def give_model_names(config, modeltype1):
    conf1 = config[0].replace("/", "_")
    if modeltype1 == "permuted":
        modeltype11 = ""
        modeltype2 = "_permuted_%s"%conf1
    elif modeltype1 == "permuted_normalized":
        modeltype11 = "_normalized"
        modeltype2 = "_permuted_%s_normalized"%conf1
    else:
        modeltype11 = modeltype1
        modeltype2 = modeltype1
    return modeltype11, modeltype2

configurations=[["adam/1", "adam/2"],
                ["adam/1", "adam/3"],
                ["adam/2", "adam/3"],
                ["from_adv/1", "from_adv/2"],
                ["from_adv/1", "from_adv/3"],
                ["from_adv/2", "from_adv/3"],

                ["adam/1", "from_adv/2"],
                ["adam/2", "from_adv/1"],
                ["adam/3", "from_adv/3"],

                ["rsgd/1", "rsgd/2"],
                ["rsgd/1", "rsgd/3"],
                ["rsgd/2", "rsgd/3"],
    
                ["rsgd/1", "adam/2"],
                ["rsgd/1", "adam/1"],
                ["rsgd/3", "adam/3"],
                ["rsgd/1", "from_adv/2"],
                ["rsgd/2", "from_adv/1"],
                ["rsgd/3", "from_adv/3"],
                ]

model = "MLP"; modeldir = "mlp_512_512/mnist"; dataset = "MNIST"
#model = "TestLeNet"; modeldir = "testlenet/fashion"; dataset = "FashionMNIST"
#model = "TestLeNet"; modeldir = "testlenet/cifar"; dataset = "CIFAR10"
#model = "VGG16bn"; modeldir = "vgg16_bn/cifar"; dataset = "CIFAR10"

epochs = 600
opt = "cosine"; lr = 0.02
modeltypes = ["", "_normalized", "permuted", "permuted_normalized"]
#modeltypes = [modeltypes[3]]
gpu = 2

color = ["tab:orange", "tab:blue", "tab:green", "tab:red"]
marker = ["o", "s", "^", "x"]
labels = ["nothing", "normalized", "permuted", "permuted_normalized"]

plot = False

fig, ax = plt.subplots(1, figsize=(10.0, 6.0))

nstat = 3
if not plot:
    distances_all = {}
else:
    f = open(f"distances/distances_{model}_{dataset}_{opt}.json", "r")
    distances_all = json.load(f)
    f.close()
config_labels = []
for (i,config) in enumerate(configurations):
    for (j,modeltype1) in enumerate(modeltypes):
        conf1 = config[0].replace("/", "_")
        conf2 = config[1].replace("/", "_")
        modeldirpl = modeldir.replace("/", "_")
        modeltype11, modeltype2 = give_model_names(config, modeltype1)

        ckpt_before_opt = f"curves/checkpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-0.pt"
        ckpt_after_opt = f"curves/checkpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"

        if os.path.isfile(ckpt_before_opt) and os.path.isfile(ckpt_after_opt):
            key = f"{conf1}-{conf2}-{modeltype1}"
            if not plot:
                dist = distance_optimized_points(modeltype=model, dataset=dataset, ckpt_before_opt=ckpt_before_opt, ckpt_after_opt=ckpt_after_opt)
                dist = dist.item()
                distances_all[key] = dist
            else:
                dist = distances_all[key]

            print(f"dist:{dist}")

            if i % 3 == 0 and j == 0:
                config_labels.append(f"{conf1[0]}-{conf2[0]}")
                #print("CONF", f"{conf1[0]}-{conf2[0]}")

            if i == 0:
                #print("MODEL", modeltype1)
                plt.scatter(int(i/nstat), dist, marker=marker[j], c=color[j], alpha=0.6, label=labels[j])
            else:
                plt.scatter(int(i/nstat), dist, marker=marker[j], c=color[j], alpha=0.6)
            j += 1
        else:
            print(f"NOT FOUND: {ckpt_before_opt} or {ckpt_after_opt}")

#print(distances_all)
f = open(f"distances/distances_{model}_{dataset}_{opt}.json", "w")
json.dump(distances_all, f)
f.close()

config_labels = ["a-a", *config_labels]
config_labels = ["a-a", "sgd-sgd", "adv-adv", "sgd-adv", "rsgd-rsgd", "rsgd-sgd", "rsgd-adv"]
print(config_labels)
ax.set_xticklabels(config_labels)
ax.tick_params(labelsize=16)
ax.set_xlabel("couple type", fontsize=16)
ax.set_ylabel("distance (per-parameter)", fontsize=16)

#ax.legend(loc=(1,0.1))
plt.legend(loc="best", fontsize=14)

plt.suptitle(f"{model},  {dataset}, {opt}", fontsize=18)
#plt.tight_layout()
plt.savefig(f"plots/figures/distances_opt_points.png")
