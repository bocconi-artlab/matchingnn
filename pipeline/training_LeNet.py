## I checkpoint sono liste adesso

import torch 

import os, sys
from torch.functional import norm
from copy import deepcopy
matchingnn_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if not matchingnn_dir in sys.path:
    sys.path.insert(0, matchingnn_dir)
from src.normalize import normalize_model
from src.matching import match_sequential

def clean_dict(check,to_retain='center'):
    out = {}
    for i in check.keys():
        if i.split('.')[0] == to_retain: 
            out[i] = check[i]  
    return out

def initializeTest(model_name, ncolors, outvolume, curve, num_bends, fix_start=True, fix_end = True, \
                   init_start = None, init_end = None, init_linear = True, is_esgd = False):
    
    architecture = getattr(TestLeNet, model_name)
    if curve is None:
        model = architecture().base(ncolors, outvolume)
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetTest(
            ncolors,
            outvolume,
            curve,
            architecture.curve,
            num_bends,
            fix_start,
            fix_end,
            architecture_kwargs = architecture.kwargs,
        )
        
    base_model = None
    for path, k in [(init_start, 0), (init_end, num_bends - 1)]:
        print(path)
        if path is not None:
            if base_model is None:
                base_model = architecture.base(ncolors, outvolume)

            if is_esgd:
                checkpoint_temp = torch.load(path, map_location=torch.device('cpu'))
                checkpoint={}
                for i in checkpoint_temp.keys():
                    print(i)
                    temp2 = re.sub("replicas.0.", "", i)
                    #temp2 = re.sub("model.","model.",i)
                    checkpoint[temp2] = checkpoint_temp[i]
                #is_esgd = False
            else:
                checkpoint_temp = torch.load(path,map_location=torch.device('cpu'))
                checkpoint={}
                for i in checkpoint_temp.keys():
                    print(i)
                    temp2 = re.sub("model.","model.",i)
                    checkpoint[temp2] = checkpoint_temp[i]
              
            print('Loading %s as point #%d' % (path, k))
            base_model.load_state_dict(checkpoint)
            model.import_base_parameters(base_model, k)
            print('OK')
    if init_linear:
        print('Linear initialization.')
        model.init_linear()
    return model

def initializeVGG(model_name, curve, num_bends, fix_start = True, fix_end = True, \
                init_start = None, init_end = None, init_linear = True, is_esgd = False):
    
    architecture = getattr(VGG16bn, model_name)
    if curve is None:
        model = architecture().base()
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetVGG(
            curve,
            architecture.curve,
            num_bends,
            fix_start,
            fix_end,
            architecture_kwargs = architecture.kwargs,
        )
    base_model = None
    for path, k in [(init_start, 0), (init_end, num_bends - 1)]:
        print(path)
        if path is not None:
            if base_model is None:
                base_model = architecture.base()

            if is_esgd:
                checkpoint_temp = torch.load(path, map_location=torch.device('cpu'))
                checkpoint={}
                for i in checkpoint_temp.keys():
                    print(i)
                    temp2 = re.sub("replicas.0.", "", i)
                    #temp2 = re.sub("model.","model.",i)
                    checkpoint[temp2] = checkpoint_temp[i]
                #is_esgd = False
            else:
                checkpoint_temp = torch.load(path,map_location=torch.device('cpu'))
                checkpoint={}
                for i in checkpoint_temp.keys():
                    print(i)
                    temp2 = re.sub("model.","model.",i)
                    checkpoint[temp2] = checkpoint_temp[i]
              
            print('Loading %s as point #%d' % (path, k))
            base_model.load_state_dict(checkpoint)
            model.import_base_parameters(base_model, k)
            if k == 0:
                model.import_base_buffers(base_model)
                # devono diventare dei buffer k-esimi. o almeno averne due. poi li interpolo
            print('OK')
    if init_linear:
        print('Linear initialization.')
        model.init_linear()
    return model

def initializeMLP(model_name, nim, nhs, num_classes, curve, num_bends, fix_start=True, fix_end = True, \
                init_start = None, init_end = None, init_linear = True):

    architecture = getattr(MLP, model_name)

    if curve is None:
        model = architecture().base(nim, nhs, num_classes)
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetMLP(
            num_classes,
            nim,
            nhs,
            curve,
            architecture.curve,
            num_bends,
            fix_start,
            fix_end,
            architecture_kwargs = architecture.kwargs,
        )
        
    base_model = None
    for path, k in [(init_start, 0), (init_end, num_bends - 1)]:
        print(path)
        if path is not None:
            if base_model is None:
                base_model = architecture.base(nim, nhs, num_classes)
            
            checkpoint = torch.load(path, map_location=torch.device('cpu'))
              
            print('Loading %s as point #%d' % (path, k))
            base_model.load_state_dict(checkpoint)
            model.import_base_parameters(base_model, k)
            print('OK')

    if init_linear:
        print('Linear initialization.')
        model.init_linear()

    return model

def initialize(model_name,in_channels,dim,num_classes,curve,num_bends,is_rsgd = True,fix_start=True,fix_end = True, \
                init_start = None,init_end = None,init_linear = True,mixed = True):
    
    if model_name == 'ResNet20':
        architecture = getattr(RN_cifar,model_name)
    else:
        architecture = getattr(LeNet,model_name)
    if curve is None:
        model = architecture().base(num_classes,in_channels)

    else:
        curve = getattr(curves, curve)
        model = curves.CurveNet(
            num_classes,
            in_channels,
            dim,
            curve,
            architecture.curve,
            num_bends,
            fix_start,
            fix_end,
            architecture_kwargs=architecture.kwargs,
        )
        
    base_model = None
    for path, k in [(init_start, 0), (init_end, num_bends - 1)]:
        if path is not None:
            if base_model is None:
                if model_name == 'ResNet20':
                    base_model = architecture.base()
                else:
                    base_model = architecture.base(nclasses=num_classes,in_channels=in_channels,dim=dim, **architecture.kwargs)
            
            checkpoint = torch.load(path)[0]
                
            if is_rsgd:
                state_dict = {}
                checkpoint = clean_dict(checkpoint,'center')
                for i in checkpoint.keys():
                    print(i)
                    temp2 = re.sub("center.","",i)
                    state_dict[temp2] = checkpoint[i]
                    
                if mixed:
                    is_rsgd = False
                
            else: 
                state_dict={}
                for i in checkpoint.keys():
                    print(i)
                    temp2 = re.sub("replicas.0.","",i)
                    state_dict[temp2] = checkpoint[i]
                if mixed:
                    is_rsgd = True
              
            print('Loading %s as point #%d' % (path, k))
            base_model.load_state_dict(state_dict)
            model.import_base_parameters(base_model, k)

    if init_linear:
        print('Linear initialization.')
        model.init_linear()

    return model.cuda()

def learning_rate_schedule(base_lr, epoch, total_epochs):
    alpha = epoch / total_epochs
    if alpha <= 0.5:
        factor = 1.0
    elif alpha <= 0.9:
        factor = 1.0 - (alpha - 0.5) / 0.4 * 0.99
    else:
        factor = 0.01
    return factor * base_lr

def train_hyper(model, loss, optimizer, lr, mom, w_d, curve = None, opt = "sgd"):

    criterion = getattr(F,loss)
    regularizer = None if curve is None else None

    if opt == "sgd":
        #optimizer = getattr(torch.optim, optimizer)
        #optimizer = optimizer(filter(lambda param: param.requires_grad, model.parameters()),
        #                    lr = lr,
        #                    momentum = mom,
        #                    weight_decay = w_d)
        optimizer = torch.optim.SGD(
               filter(lambda param: param.requires_grad, model.parameters()),
               lr=0.02,
               momentum = 0.0,
               weight_decay=0.0)
    elif opt == "adam":
        optimizer = torch.optim.Adam(
               filter(lambda param: param.requires_grad, model.parameters()),
               lr=0.001,
               weight_decay=w_d)
    elif opt == "cosine":
        optimizer = torch.optim.SGD(filter(lambda param: param.requires_grad, model.parameters()), 
                                    lr=lr, momentum=0.9, nesterov=True, weight_decay=0.0)
        
    print('Criterion: {}'.format(criterion))
    print('optimizer: {}'.format(optimizer))

    return criterion, optimizer, regularizer

def normalize_model_in_curve(model, m_to_norm, modelname, mtype, 
                             normalize_endpoints=False, normalize_last_layer=True,
                             permute_endpoints=True, normalize_midpoint=True):

    with torch.no_grad():

        model_normalized = deepcopy(model)
        m_to_norm_end1 = deepcopy(m_to_norm)
        m_to_norm_end2 = deepcopy(m_to_norm)
        parameters_norm_end1 = list(m_to_norm_end1.parameters())
        parameters_norm_end2 = list(m_to_norm_end2.parameters())

        if mtype in ["_normalized", "permuted_normalized", "_rescaled", "permuted_rescaled"] or normalize_endpoints:

            # get nets
            parameters = list(model_normalized.net.parameters())
            parameters_norm = list(m_to_norm.parameters())
            j = 0
            for i in range(0, len(parameters), model_normalized.num_bends):
                weights = parameters[i:i+model_normalized.num_bends]
                parameters_norm[j].data.copy_(weights[1].data)
                if normalize_endpoints:
                    parameters_norm_end1[j].data.copy_(weights[0].data)
                    parameters_norm_end2[j].data.copy_(weights[-1].data)
                j += 1
            if modelname in ["MLP"]:
                net1 = m_to_norm.layers
            elif modelname in ["TestLeNet", "VGG16bn"]:
                net1 = m_to_norm.model
            # normalize
            if normalize_midpoint:
                net1 = normalize_model(net1, normalize_last_layer=normalize_last_layer)
            if modelname in ["MLP"]:
                m_to_norm.layers = net1
            elif modelname in ["TestLeNet", "VGG16bn"]:
                m_to_norm.model = net1

            if normalize_endpoints:
                if modelname in ["MLP"]:
                    net1 = m_to_norm_end1.layers
                    net2 = m_to_norm_end2.layers
                elif modelname in ["TestLeNet", "VGG16bn"]:
                    net1 = m_to_norm_end1.model
                    net2 = m_to_norm_end2.model
                net1 = normalize_model(net1, normalize_last_layer=normalize_last_layer)
                net2 = normalize_model(net2, normalize_last_layer=normalize_last_layer)
                if modelname in ["MLP"]:
                    m_to_norm_end1.layers = net1
                    m_to_norm_end2.layers = net2
                elif modelname in ["TestLeNet", "VGG16bn"]:
                    m_to_norm_end1.model = net1
                    m_to_norm_end2.model = net2

                parameters_norm_end1 = list(m_to_norm_end1.parameters())
                parameters_norm_end2 = list(m_to_norm_end2.parameters())

            # put nets back into the checkpoint
            parameters = list(model_normalized.net.parameters())
            parameters_norm = list(m_to_norm.parameters())
            j = 0
            for i in range(0, len(parameters), model_normalized.num_bends):
                weights = parameters[i:i+model_normalized.num_bends]
                weights[1].data.copy_(parameters_norm[j].data)
                if normalize_endpoints:
                    weights[0].data.copy_(parameters_norm_end1[j].data)
                    weights[-1].data.copy_(parameters_norm_end2[j].data)
                j += 1

        if permute_endpoints:

            parameters_norm_end1 = list(m_to_norm_end1.parameters())
            parameters_norm_end2 = list(m_to_norm_end2.parameters())
            parameters = list(model_normalized.net.parameters())
            parameters_norm = list(m_to_norm.parameters())
            j = 0
            for i in range(0, len(parameters), model_normalized.num_bends):
                weights = parameters[i:i+model_normalized.num_bends]
                parameters_norm[j].data.copy_(weights[1].data)
                parameters_norm_end1[j].data.copy_(weights[0].data)
                parameters_norm_end2[j].data.copy_(weights[-1].data)
                j += 1

            if modelname in ["MLP"]:
                net1 = m_to_norm_end1.layers
                net2 = m_to_norm_end2.layers
                net_mid = m_to_norm.layers
            elif modelname in ["TestLeNet", "VGG16bn"]:
                net1 = m_to_norm_end1.model
                net2 = m_to_norm_end2.model
                net_mid = m_to_norm.model

            _, net1 = match_sequential(net_mid, net1)
            _, net2 = match_sequential(net_mid, net2)

            if modelname in ["MLP"]:
                m_to_norm_end1.layers = net1
                m_to_norm_end2.layers = net2
            elif modelname in ["TestLeNet", "VGG16bn"]:
                m_to_norm_end1.model = net1
                m_to_norm_end2.model = net2

            parameters_norm_end1 = list(m_to_norm_end1.parameters())
            parameters_norm_end2 = list(m_to_norm_end2.parameters())
            j = 0
            for i in range(0, len(parameters), model_normalized.num_bends):
                weights = parameters[i:i+model_normalized.num_bends]
                weights[0].data.copy_(parameters_norm_end1[j].data)
                weights[-1].data.copy_(parameters_norm_end2[j].data)
                j += 1

        return model_normalized

def Dead_Lift(model, criterion, loader, optimizer, start_epoch, epochs, save_freq, dire, 
              modelname, mtype, m_to_norm, regularizer=None, curve=None, resume=None, 
              name='checkpoint', scheduler=None, normalize_last_layer=True, save_normalized_checkpoint=False):

    if resume is not None:
        print('Resume training from %s' % resume)
        checkpoint = torch.load(resume)
        start_epoch = checkpoint['epoch'] + 1
        model.load_state_dict(checkpoint['model_state'])
        optimizer.load_state_dict(checkpoint['optimizer_state'])
    
    columns = ['ep', 'lr', 'tr_loss', 'tr_acc', 'te_nll', 'te_acc', 'time']

    if save_normalized_checkpoint:
        model_normalized = normalize_model_in_curve(model, m_to_norm, modelname, mtype, 
                                                    normalize_endpoints=True, normalize_last_layer=True,
                                                    permute_endpoints=True, normalize_midpoint=True)
        utils.save_checkpoint(
            dire,
            epochs,
            model_state = model_normalized.state_dict(),
            optimizer_state = optimizer.state_dict(),
            name = name+"NORMALIZED"
        )

        return None


    utils.save_checkpoint(
        dire,
        start_epoch - 1,
        model_state = model.state_dict(),
        optimizer_state = optimizer.state_dict(),
        name = name
    )

    test_res = {'loss': None, 'accuracy': None, 'nll': None}

    for epoch in range(start_epoch, epochs + 1):

        time_ep = time.time()

        if scheduler is None:
            lr = learning_rate_schedule(0.001, epoch, epochs)
        else:
            scheduler.step()
            lr = scheduler.get_last_lr()[0]
        utils.adjust_learning_rate(optimizer, lr)

        train_res = utils.train(loader['train'], model, optimizer, criterion, regularizer)
        if curve is None:
            test_res = utils.test(loader['test'], model, criterion, regularizer)

        if epoch % save_freq == 0:
            #model_normalized = normalize_model_in_curve(model, m_to_norm, modelname, mtype, 
            #                                            normalize_last_layer=normalize_last_layer)
            utils.save_checkpoint(
                dire,
                epoch,
                model_state = model.state_dict(),
                optimizer_state = optimizer.state_dict(),
                name = name
            )

        time_ep = time.time() - time_ep
        values = [epoch, lr, train_res['loss'], train_res['accuracy'], test_res['nll'],
                  test_res['accuracy'], time_ep]

        table = tabulate.tabulate([values], columns, tablefmt='simple', floatfmt='9.4f')
        if epoch % 40 == 1 or epoch == start_epoch:
            table = table.split('\n')
            table = '\n'.join([table[1]] + table)
        else:
            table = table.split('\n')[2]
        print(table)

def to_gpuid_string(x):
    if isinstance(x, list):
        return str(x)[1:-1]
    return str(x)

def main():
    
    parser = argparse.ArgumentParser(description='DNN curve training')
    parser.add_argument('--dir',required = True, type=str, default=None, metavar='DIR',
                        help='training directory (default: None')
    parser.add_argument('--dataset', type=str, default='FashionMNIST', metavar='DATASET',
                        help='dataset name (default: FashionMNIST)')
    parser.add_argument('--init_s',required = True, type=str, default=None, metavar='CKPT',
                        help='checkpoint to init start point (default: None)')
    parser.add_argument('--init_e',required = True, type=str, default=None, metavar='CKPT',
                        help='checkpoint to init end point (default: None)')
    parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                        help='input batch size (default: 128)')
    parser.add_argument('--seed', type=int, default=42, metavar='S', help='random seed (default: 42)')
    parser.add_argument('--nin', type=int, default=784, metavar='S', help='random seed (default: 784)')
    parser.add_argument('--model', type=str, default='LeNet', metavar='MODEL',
                        help='model name (default: LeNet)')
    parser.add_argument('--curve', type=str, default='PolyChain', metavar='CURVE',
                        help='curve type to use (default: PolyChain')
    parser.add_argument('--mixed', dest='mixed', action='store_false',
                    help='turns off mixed (default: off)')
    parser.add_argument('--is_rsgd', dest='is_rsgd', action='store_false',
                   help='turns off if_rsgd (default: off)')
    parser.add_argument('--is_esgd', dest='is_esgd', action='store_true',
                   help='turns off if_esgd (default: off)')              
    parser.add_argument('--epochs', dest='epochs', type=int, default=200, metavar='EPOCHS',
                   help='number of epochs (default: 200). choose 0 for linear path.')              
    parser.add_argument('--gpu', dest='gpu', type=int, default=0, metavar='gpu',
                   help='visible gpu')              
    parser.add_argument('--mtype', type=str, default='', metavar='mtype',
                        help='model type (default: )')
    parser.add_argument('--opt', type=str, default='', metavar='mtype',
                        help='optimizer type (default: sgd; other are adam and cosine)')
    parser.add_argument('--lr', type=float, default=1e-3, metavar='S', help='learning rate (default: 1e-3)')
    parser.add_argument('--save_normalized_checkpoint', help='normalize the checkpoint', type=eval, 
                        choices=[True, False],  default='False')
    

    #parser.set_defaults(mixed=True)
    #parser.set_defaults(is_rsgd=True)
    #parser.set_defaults(is_esgd=False)

    args = parser.parse_args()
    print(args.model)
    print(args.is_esgd)
    data_path = args.dir
    batch_size = args.batch_size
    random_seed = args.seed
    os.environ["CUDA_VISIBLE_DEVICES"] = to_gpuid_string(args.gpu)    
    
    torch.backends.cudnn.benchmark = True ## Choose the fastest convolution algorithm
    torch.manual_seed(random_seed) ## Set random_seed to reproduce results
    
    basic_transform = getattr(Transforms, args.dataset)
    
    loaders, num_classes = data_new.loaders(
        args.dataset,
        basic_transform,
        data_path,
        batch_size,
        dl = True,
        use_test=True
    )
    
    init_s = args.init_s
    init_e = args.init_e

    dim = loaders['train'].dataset[0][0].shape[2]
    in_channels = loaders['train'].dataset[0][0].shape[0]
    nin = args.nin
    
    if args.model == 'MLP':
        m = initializeMLP(args.model, nim=nin, nhs=[512, 512], num_classes=10, 
                        curve=args.curve, num_bends=3, init_start=init_s, init_end=init_e)
        m_to_norm = MLP.MLPBase(nin, [512, 512], 10)
    elif args.model == 'TestLeNet':
        if args.dataset in ["MNIST", "FashionMNIST"]:
            ncolors, outvolume = 1, 800
        elif args.dataset in ["CIFAR10"]:
            ncolors, outvolume = 3, 1250
        m = initializeTest(args.model, ncolors, outvolume, args.curve, 3, init_start=init_s, init_end=init_e, 
                           is_esgd=args.is_esgd)
        m_to_norm = TestLeNet.TestLeNetBase(ncolors, outvolume)
    elif args.model == 'VGG16bn':
        m = initializeVGG(args.model, args.curve, 3, init_start=init_s, init_end=init_e, 
                          is_esgd=args.is_esgd)
        m_to_norm = VGG16bn.vgg16_bn()
    else:
        m = initialize(args.model, in_channels, dim, 10, args.curve, 3, init_start = init_s, \
                   init_end = init_e,mixed = args.mixed, is_rsgd=args.is_rsgd)

    m.cuda()
    crit, optim, regularizer = train_hyper(m, "cross_entropy", "SGD", args.lr, 0.9, 5e-4, curve=args.curve, opt=args.opt)
    scheduler = None
    if args.opt == "cosine":
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optim, T_max=args.epochs, eta_min=0., last_epoch=-1)
    inits = args.init_s.replace("/", "_")[26:]
    inite = args.init_e.replace("/", "_")[26:]
    name = f"checkpoint-{args.opt}-{inits}-{inite}"

    if args.mtype in ["_rescaled", "permuted_rescaled"]:
        normalize_last_layer = False
        print(f"normalize_last_layer: FALSE\n")
    else:
        normalize_last_layer = True

    Dead_Lift(m, crit, loaders, optim, 1, args.epochs, 200, args.dir, args.model, 
              args.mtype, m_to_norm, regularizer, curve=args.curve, name=name, 
              scheduler=scheduler, normalize_last_layer=normalize_last_layer, 
              save_normalized_checkpoint=args.save_normalized_checkpoint)
    
if __name__ == '__main__':
    
    import os, sys
    from copy import deepcopy
    import tabulate
    import time
    import torch
    import torch.nn.functional as F
    import re
    import curves
    import LeNet, RN_cifar, MLP, TestLeNet, VGG16bn
    import utils
    import data_new
    import Transforms
    import argparse
    
    matchingnn_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    if not matchingnn_dir in sys.path:
        sys.path.insert(0, matchingnn_dir)
    from src.normalize import normalize_model

    main()
