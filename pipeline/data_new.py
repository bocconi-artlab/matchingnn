import os
import torch
import torchvision
import torchvision.transforms as transforms


def loaders(dataset, tr, path, batch_size, num_workers = 3, use_test=False,
            shuffle_train=True, dl = True, train_size = 0.8):

    data = getattr(torchvision.datasets, dataset)

    if dataset.lower() in os.listdir(path):
        dl = False

    path = os.path.join(path, dataset.lower())

    train_set = data(path, train=True, download=dl, transform=tr)

    if use_test:

        test_set = data(path, train=False, download=dl, transform=tr)
        
    else:

        dim_test = int(train_set.data.shape[0] - train_set.data.shape[0]*0.8)

        train_set.data = train_set.data[:-dim_test]
        train_set.targets = train_set.targets[:-dim_test]

        test_set = data(path, train=True, download=False, transform=tr)
        test_set.train = False
        test_set.data = test_set.data[-dim_test:]
        test_set.targets = test_set.targets[-dim_test:]

    return {
               'train': torch.utils.data.DataLoader(
                   train_set,
                   batch_size=batch_size,
                   shuffle=shuffle_train,
                   num_workers=num_workers,
               ),
               'test': torch.utils.data.DataLoader(
                   test_set,
                   batch_size=batch_size,
                   shuffle=False,
                   num_workers=num_workers,
               ),
           }, max(train_set.targets) + 1



