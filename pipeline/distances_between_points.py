import torch
import torch.nn.functional as F
import json
import os, sys
from copy import deepcopy
from statistics import mean, stdev
from math import isnan, nan

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')

import RN_cifar, MLP, TestLeNet, VGG16bn
import curves
matchingnn_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if not matchingnn_dir in sys.path:
    sys.path.insert(0, matchingnn_dir)
from src.normalize import normalize_model
from src.matching import match_sequential

def initializeTest(model_name, ncolors, outvolume, curve, num_bends):
    architecture = getattr(TestLeNet, model_name)
    if curve is None:
        model = architecture().base(ncolors, outvolume)
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetTest(
            ncolors,
            outvolume,
            curve,
            architecture.curve,
            num_bends,
            architecture_kwargs=architecture.kwargs
        )
    return model

def initializeMLP(model_name, nin, nhs, num_classes, curve, num_bends):
    architecture = getattr(MLP, model_name)
    if curve is None:
        model = architecture().base(nin, nhs, num_classes)
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetMLP(
            num_classes,
            nin,
            nhs,
            curve,
            architecture.curve,
            num_bends,
            architecture_kwargs = architecture.kwargs
        )
    return model
    
def initializeVGG(model_name, curve, num_bends):
    architecture = getattr(VGG16bn, model_name)
    if curve is None:
        model = architecture().base()
    else:
        curve = getattr(curves, curve)
        model = curves.CurveNetVGG(
            curve,
            architecture.curve,
            num_bends,
            architecture_kwargs=architecture.kwargs
        )
    return model

def calculate_euclidean_distance(net1, net2, npars):
    dist = 0.0
    for w1, w2 in zip(net1.parameters(), net2.parameters()):
        dist += F.mse_loss(w1, w2, reduction='sum')
    dist /= npars
    return torch.sqrt(dist).item()

def distance_optimized_points(modeltype="MLP", dataset="MNIST", ckpt_before_opt="", 
                              ckpt_after_opt="", dist_type="", for_adv_midpoint=None):

    curve = 'PolyChain'
    nin = 784 if dataset in ["MNIST", "FashionMNIST"] else 3072
    in_channels = 1 if dataset in ["mnist", "fashion"] else 3

    if modeltype=='MLP':
        model_before_opt = initializeMLP(modeltype, nin, [512,512], 10, curve, 3)
        model_after_opt = initializeMLP(modeltype, nin, [512,512], 10, curve, 3)
        model_original = initializeMLP(modeltype, nin, [512,512], 10, None, 3)
    elif modeltype=='TestLeNet':
        if dataset in ["MNIST", "FashionMNIST"]:
            ncolors, outvolume = 1, 800
        elif dataset in ["CIFAR10"]:
            ncolors, outvolume = 3, 1250
        model_before_opt = initializeTest(modeltype, ncolors, outvolume, curve, 3)
        model_after_opt = initializeTest(modeltype, ncolors, outvolume, curve, 3)
        model_original = initializeTest(modeltype, ncolors, outvolume, None, 3)
    elif modeltype=='VGG16bn':
        model_before_opt = initializeVGG(modeltype, curve, 3)
        model_after_opt = initializeVGG(modeltype, curve, 3)
        model_original = initializeVGG(modeltype, None, 3)
    else:
        model = initialize(modeltype, curve, 10, in_channels, dim, 3)
        model_original = initialize(modeltype, None, 10, in_channels, dim, 3)
    
    checkpoint_before_opt = torch.load(ckpt_before_opt)
    checkpoint_after_opt = torch.load(ckpt_after_opt)

    model_before_opt.load_state_dict(checkpoint_before_opt['model_state'])
    model_after_opt.load_state_dict(checkpoint_after_opt['model_state'])

    model_before_opt.cuda()
    model_after_opt.cuda()

    # TODO: estrai i parametri dalla curva
    nbends = model_before_opt.num_bends
    parameters_before_opt = list(model_before_opt.net.parameters())
    parameters_after_opt = list(model_after_opt.net.parameters())
 
    if dist_type == "return_adv":
        return parameters_before_opt
    #######################################################

    dist_before_after_opt, dist_endpoints = 0, 0
    dist_end1_mid_opt, dist_end2_mid_opt = 0, 0
    numel = 0
    for i in range(0, len(parameters_before_opt), nbends):
        # QUI SCELGO I MODELLI TRA CUI CALCOLARE LE DISTANZE
        weights_before_opt = parameters_before_opt[i:i+nbends]
        weights_after_opt = parameters_after_opt[i:i+nbends]
        midpoint_before_opt = weights_before_opt[1].data
        midpoint_after_opt = weights_after_opt[1].data
        endpoint1 = weights_before_opt[0].data
        endpoint2 = weights_before_opt[2].data
        # QUI CALCOLO LE VARIE DISTANZE
        # distanza tra prima/dopo ottimizzazione
        dist_before_after_opt += torch.sum((midpoint_before_opt - midpoint_after_opt)**2)
        # distanza tra endpoints
        dist_endpoints += torch.sum((endpoint1 - endpoint2)**2)
        # distanza tra endpoints 1 e 2 e midpoint ottimizzato
        dist_end1_mid_opt += torch.sum((endpoint1 - midpoint_after_opt)**2)
        dist_end2_mid_opt += torch.sum((endpoint2 - midpoint_after_opt)**2)

        numel_lay = midpoint_before_opt.numel()
        numel += numel_lay

    dist_before_after_opt = torch.sqrt(dist_before_after_opt/numel).item()
    dist_endpoints = torch.sqrt(dist_endpoints/numel).item()
    dist_end1_mid_opt = torch.sqrt(dist_end1_mid_opt/numel).item()
    dist_end2_mid_opt = torch.sqrt(dist_end2_mid_opt/numel).item()

    # this is the ADV midpoint vs others (RSGD, SGD, ADV)
    dist_with_others = 0
    if dist_type == "with_others":

        if modeltype == 'MLP':
            m_mid = MLP.MLPBase(nin, [512, 512], 10)
        elif modeltype == 'TestLeNet':
            if dataset in ["MNIST", "FashionMNIST"]:
                ncolors, outvolume = 1, 800
            elif dataset in ["CIFAR10"]:
                ncolors, outvolume = 3, 1250
            m_mid = TestLeNet.TestLeNetBase(ncolors, outvolume)
        elif modeltype == 'VGG16bn':
            m_mid = VGG16bn.vgg16_bn()

        m_end1 = deepcopy(m_mid)

        parameters_mid = list(m_mid.parameters())
        parameters_end1 = list(m_end1.parameters())
        j = 0
        for i in range(0, len(for_adv_midpoint), nbends):
            weights = parameters_before_opt[i:i+nbends]
            weights_adv = for_adv_midpoint[i:i+nbends]
            parameters_end1[j].data.copy_(weights[0].data)
            parameters_mid[j].data.copy_(weights_adv[1].data)
            j += 1

        if modeltype in ["MLP"]:
            net1 = m_end1.layers
            net_mid = m_mid.layers
        elif modeltype in ["TestLeNet", "VGG16bn"]:
            net1 = m_end1.model
            net_mid = m_mid.model

        if "normalized" in ckpt_before_opt:
            net1 = normalize_model(net1, normalize_last_layer=True)
            net_mid = normalize_model(net_mid, normalize_last_layer=True)

        _, net_mid = match_sequential(net1, net_mid)

        if modeltype in ["MLP"]:
            m_mid.layers = net_mid
        elif modeltype in ["TestLeNet", "VGG16bn"]:
            m_mid.model = net_mid

        parameters_mid = list(m_mid.parameters())
        j = 0
        for i in range(0, len(for_adv_midpoint), nbends):
            weights = for_adv_midpoint[i:i+nbends]
            weights[1].data.copy_(parameters_mid[j].data)
            j += 1

        for i in range(0, len(for_adv_midpoint), nbends):
            # QUI SCELGO I MODELLI TRA CUI CALCOLARE LE DISTANZE
            weights_before_opt = parameters_before_opt[i:i+nbends]
            weights_adv = for_adv_midpoint[i:i+nbends]
            endpoint1 = weights_before_opt[0].data
            midpoint_adv = weights_adv[1].data
            # QUI CALCOLO LE VARIE DISTANZE
            dist_with_others += torch.sum((endpoint1 - midpoint_adv)**2)
        dist_with_others = torch.sqrt(dist_with_others/numel).item()
        return dist_with_others
    else:
        dist_with_others = None

    return dist_before_after_opt, dist_endpoints, dist_end1_mid_opt, dist_end2_mid_opt, dist_with_others


def give_model_names(config, modeltype1):
    conf1 = config[0].replace("/", "_")
    if modeltype1 == "permuted":
        modeltype11 = ""
        modeltype2 = "_permuted_%s"%conf1
    elif modeltype1 == "permuted_normalized":
        modeltype11 = "_normalized"
        modeltype2 = "_permuted_%s_normalized"%conf1
    else:
        modeltype11 = modeltype1
        modeltype2 = modeltype1
    return modeltype11, modeltype2

configurations=[
                ["rsgd/1", "rsgd/2"],
                ["rsgd/1", "rsgd/3"],
                ["rsgd/2", "rsgd/3"],

                ["rsgd/1", "adam/2"],
                ["rsgd/1", "adam/1"],
                ["rsgd/3", "adam/3"],

                ["adam/1", "adam/2"],
                ["adam/1", "adam/3"],
                ["adam/2", "adam/3"],

                ["rsgd/1", "from_adv/2"],
                ["rsgd/2", "from_adv/1"],
                ["rsgd/3", "from_adv/3"],

                ["adam/1", "from_adv/2"],
                ["adam/2", "from_adv/1"],
                ["adam/3", "from_adv/3"],
    
                ["from_adv/1", "from_adv/2"],
                ["from_adv/1", "from_adv/3"],
                ["from_adv/2", "from_adv/3"],

                ]

model = "MLP"; modeldir = "mlp_512_512/mnist"; dataset = "MNIST"
model = "MLP"; modeldir = "mlp_512_512/fashion"; dataset = "FashionMNIST"
model = "MLP"; modeldir = "mlp_512_512/cifar"; dataset = "CIFAR10"
model = "TestLeNet"; modeldir = "testlenet/mnist"; dataset = "MNIST"
model = "TestLeNet"; modeldir = "testlenet/fashion"; dataset = "FashionMNIST"
model = "TestLeNet"; modeldir = "testlenet/cifar"; dataset = "CIFAR10"
model = "VGG16bn"; modeldir = "vgg16_bn/cifar"; dataset = "CIFAR10"

epochs = 200
opt = "cosine"; lr = 0.02
modeltypes = ["", "permuted", "_normalized", "permuted_normalized"]
#modeltypes = ["", "permuted"]
#modeltypes = [""]
gpu = 0

color = ["tab:orange", "tab:blue", "tab:green", "tab:red"]
marker = ["o", "s", "^", "x"]
#labels = ["nothing", "permuted", "normalized", "permuted_normalized"]
labels = ["Raw", "Aligned", "Normalized", "Normalized-Aligned"]

#modeltypes = ["_normalized", "permuted_normalized"]
#color = ["tab:green", "tab:red"]
#marker = ["^", "x"]
#labels = ["normalized", "permuted_normalized"]


plot = False

fig, ax = plt.subplots(1, 3, figsize=(6.4*2.0, 4.8*1.2))

nstat = 3
if not plot:
    distances_all = {}
    distances_all_ends = {}
else:
    f = open(f"distances/distances_{model}_{dataset}_{opt}.json", "r")
    distances_all = json.load(f)
    f.close()
config_labels = []

dist_leftright, distleft, distright = [], [], []
for (j,modeltype1) in enumerate(modeltypes):
    flaglabel = True
    for (i,config) in enumerate(configurations):
        conf1 = config[0].replace("/", "_")
        conf2 = config[1].replace("/", "_")
        modeldirpl = modeldir.replace("/", "_")
        modeltype11, modeltype2 = give_model_names(config, modeltype1)

        ckpt_before_opt = f"curves/checkpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-0.pt"
        ckpt_after_opt = f"curves/checkpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"

        #["rsgd/1", "from_adv/2"],
        #["rsgd/2", "from_adv/1"],
        #["rsgd/3", "from_adv/3"],

        if modeldir=="testlenet/mnist" and modeltype1=="permuted" and config==["rsgd/1", "from_adv/2"]:
            continue

        if os.path.isfile(ckpt_before_opt) and os.path.isfile(ckpt_after_opt):
            key = f"{conf1}-{conf2}-{modeltype1}"
            if not plot:
                dist, dist_ends, dist_end1_mid_opt, dist_end2_mid_opt, _ = \
                distance_optimized_points(modeltype=model, dataset=dataset, 
                                          ckpt_before_opt=ckpt_before_opt, 
                                          ckpt_after_opt=ckpt_after_opt)
                
                triangular_deviation =  dist_end1_mid_opt + dist_end2_mid_opt - dist_ends

                distances_all[key] = dist
                distances_all_ends[key] = dist_ends

            else:
                dist = distances_all[key]

            print(f"dist:{dist}")

            if i % 3 == 0 and j == 0:
                config_labels.append(f"{conf1[0]}-{conf2[0]}")
                #print("CONF", f"{conf1[0]}-{conf2[0]}")

            if i % nstat == nstat-1:
                #print("MODEL", modeltype1)
                if not isnan(dist_ends):
                    dist_leftright.append(dist_ends)
                if not isnan(dist_end1_mid_opt):
                    distleft.append(dist_end1_mid_opt)
                if not isnan(dist_end2_mid_opt):
                    distright.append(dist_end2_mid_opt)                    

                if len(dist_leftright) > 1:
                    mean_dist_leftright = mean(dist_leftright)
                    std_dist_leftright = stdev(dist_leftright)
                else:
                    mean_dist_leftright = dist_leftright[0] if len(dist_leftright)==1 else nan
                    std_dist_leftright = 0
                if len(distleft) > 1:
                    mean_distleft = mean(distleft)
                    std_distleft = stdev(distleft)
                else:
                    mean_distleft = distleft[0] if len(distleft)==1 else nan
                    std_distleft = 0
                if len(distright) > 1:
                    mean_distright = mean(distright)
                    std_distright = stdev(distright)
                else:
                    mean_distright = distright[0] if len(distright)==1 else nan
                    std_distright = 0

                dist_leftright, distleft, distright = [], [], []

                if flaglabel:
                    #ax[j].errorbar(int(i/nstat), dist, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    ax[0].errorbar(int(6+i/nstat), mean_dist_leftright, yerr=std_dist_leftright, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    #ax[j].errorbar(int(12+i/nstat), triangular_deviation, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    ax[1].errorbar(int(12+i/nstat), mean_distleft, yerr=std_distleft, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    ax[2].errorbar(int(18+i/nstat), mean_distright, yerr=std_distright, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    flaglabel = False
                else:
                    #ax[j].errorbar(int(i/nstat), dist, marker=marker[j], c=color[j], alpha=1.0, capsize=2)
                    ax[0].errorbar(int(6+i/nstat), mean_dist_leftright, yerr=std_dist_leftright, marker=marker[j], c=color[j], alpha=1.0, capsize=2)
                    #ax[j].errorbar(int(12+i/nstat), triangular_deviation, marker=marker[j], c=color[j], alpha=1.0, capsize=2)
                    ax[1].errorbar(int(12+i/nstat), mean_distleft, yerr=std_distleft, marker=marker[j], c=color[j], alpha=1.0, capsize=2)
                    ax[2].errorbar(int(18+i/nstat), mean_distright, yerr=std_distright, marker=marker[j], c=color[j], alpha=1.0, capsize=2)

                ##ax[j].scatter(int(i/nstat), dist, marker=marker[j], c=color[j], alpha=0.6, label=labels[j])
                #ax[0].scatter(int(6+i/nstat), dist_ends, marker=marker[j], c=color[j], alpha=0.6, label=labels[j])
                ##ax[j].scatter(int(12+i/nstat), triangular_deviation, marker=marker[j], c=color[j], alpha=0.6, label=labels[j])
                #ax[1].scatter(int(12+i/nstat), dist_end1_mid_opt, marker=marker[j], c=color[j], alpha=0.6, label=labels[j])
                #ax[2].scatter(int(18+i/nstat), dist_end2_mid_opt, marker=marker[j], c=color[j], alpha=0.6, label=labels[j])
            else:
                if not isnan(dist_ends):
                    dist_leftright.append(dist_ends)
                if not isnan(dist_end1_mid_opt):
                    distleft.append(dist_end1_mid_opt)
                if not isnan(dist_end2_mid_opt):
                    distright.append(dist_end2_mid_opt)
                ##ax[j].scatter(int(i/nstat), dist, marker=marker[j], c=color[j], alpha=0.6)
                #ax[0].scatter(int(6+i/nstat), dist_ends, marker=marker[j], c=color[j], alpha=0.6)
                ##ax[j].scatter(int(12+i/nstat), triangular_deviation, marker=marker[j], c=color[j], alpha=0.6)
                #ax[1].scatter(int(12+i/nstat), dist_end1_mid_opt, marker=marker[j], c=color[j], alpha=0.6)
                #ax[2].scatter(int(18+i/nstat), dist_end2_mid_opt, marker=marker[j], c=color[j], alpha=0.6)
            #j += 1
        else:
            print(f"NOT FOUND: {ckpt_before_opt} or {ckpt_after_opt}")
            if i % nstat == nstat-1:
                #print("MODEL", modeltype1)

                if len(dist_leftright) > 1:
                    mean_dist_leftright = mean(dist_leftright)
                    std_dist_leftright = stdev(dist_leftright)
                else:
                    mean_dist_leftright = dist_leftright[0] if len(dist_leftright)==1 else nan
                    std_dist_leftright = 0
                if len(distleft) > 1:
                    mean_distleft = mean(distleft)
                    std_distleft = stdev(distleft)
                else:
                    mean_distleft = distleft[0] if len(distleft)==1 else nan
                    std_distleft = 0
                if len(distright) > 1:
                    mean_distright = mean(distright)
                    std_distright = stdev(distright)
                else:
                    mean_distright = distright[0] if len(distright)==1 else nan
                    std_distright = 0

                dist_leftright, distleft, distright = [], [], []

                if flaglabel:
                    #ax[j].errorbar(int(i/nstat), dist, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    ax[0].errorbar(int(6+i/nstat), mean_dist_leftright, yerr=std_dist_leftright, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    #ax[j].errorbar(int(12+i/nstat), triangular_deviation, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    ax[1].errorbar(int(12+i/nstat), mean_distleft, yerr=std_distleft, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    ax[2].errorbar(int(18+i/nstat), mean_distright, yerr=std_distright, marker=marker[j], c=color[j], alpha=1.0, label=labels[j], capsize=2)
                    flaglabel = False
                else:
                    #ax[j].errorbar(int(i/nstat), dist, marker=marker[j], c=color[j], alpha=1.0, capsize=2)
                    ax[0].errorbar(int(6+i/nstat), mean_dist_leftright, yerr=std_dist_leftright, marker=marker[j], c=color[j], alpha=1.0, capsize=2)
                    #ax[j].errorbar(int(12+i/nstat), triangular_deviation, marker=marker[j], c=color[j], alpha=1.0, capsize=2)
                    ax[1].errorbar(int(12+i/nstat), mean_distleft, yerr=std_distleft, marker=marker[j], c=color[j], alpha=1.0, capsize=2)
                    ax[2].errorbar(int(18+i/nstat), mean_distright, yerr=std_distright, marker=marker[j], c=color[j], alpha=1.0, capsize=2)

# CALCOLA DISTANZA TRA IL PUNTO DI MEZZO ADVERSARIAL E GLI ALTRI (RSGD, SGD, ADV)
"""
configurations=[

                ["from_adv/1", "from_adv/2"],
                ["from_adv/1", "from_adv/3"],
                ["from_adv/2", "from_adv/3"],

                ["rsgd/1", "from_adv/2"],
                ["rsgd/2", "from_adv/1"],
                ["rsgd/3", "from_adv/3"],

                ["adam/1", "from_adv/2"],
                ["adam/2", "from_adv/1"],
                ["adam/3", "from_adv/3"],
    
                ]
for (i,config) in enumerate(configurations):
    for (j, modeltype1) in enumerate(modeltypes):
        conf1 = config[0].replace("/", "_")
        conf2 = config[1].replace("/", "_")
        modeldirpl = modeldir.replace("/", "_")
        modeltype11, modeltype2 = give_model_names(config, modeltype1)

        ckpt_before_opt = f"curves/checkpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-0.pt"
        ckpt_after_opt = f"curves/checkpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"

        if os.path.isfile(ckpt_before_opt) and os.path.isfile(ckpt_after_opt):
            key = f"{conf1}-{conf2}-{modeltype1}"

            if conf1 == "from_adv_1" and conf2 == "from_adv_2":
                conf1_adv, conf2_adv = "from_adv_1", "from_adv_2"
                ckpt_before_opt_adv = f"curves/checkpoint-{opt}-{modeldirpl}_{conf1_adv}_model_final{modeltype11}.pt-{modeldirpl}_{conf2_adv}_model_final{modeltype2}.pt-0.pt"
                ckpt_after_opt_adv = f"curves/checkpoint-{opt}-{modeldirpl}_{conf1_adv}_model_final{modeltype11}.pt-{modeldirpl}_{conf2_adv}_model_final{modeltype2}.pt-{epochs}.pt"
                for_adv_midpoint = \
                distance_optimized_points(modeltype=model, dataset=dataset, 
                                            ckpt_before_opt=ckpt_before_opt_adv, 
                                            ckpt_after_opt=ckpt_after_opt_adv, 
                                            dist_type="return_adv")
            # TODO: bisogna ancora permutare for_adv_midpoint rispetto all'endpoint corrispondente
            # i ticks sono anche nell'ordine sbagliato per questa parte
            dist_w_others = \
            distance_optimized_points(modeltype=model, dataset=dataset, 
                                        ckpt_before_opt=ckpt_before_opt, 
                                        ckpt_after_opt=ckpt_after_opt, 
                                        dist_type="with_others", 
                                        for_adv_midpoint=for_adv_midpoint)
            
            print(f"dist:{dist_w_others}")

            if not ("from_adv" in conf1 and "from_adv" in conf2):
                if i % 3 == 0 and j == 0:
                    config_labels.append(f"{conf1[0]}-{conf2[0]}")
                    #print("CONF", f"{conf1[0]}-{conf2[0]}")

                if i == 0:
                    plt.scatter(int(24+i/nstat)-1, dist_w_others, marker=marker[j], c=color[j], alpha=0.6, label=labels[j])
                else:
                    plt.scatter(int(24+i/nstat)-1, dist_w_others, marker=marker[j], c=color[j], alpha=0.6)
                #j += 1
        else:
            print(f"NOT FOUND: {ckpt_before_opt} or {ckpt_after_opt}")
"""

#print(distances_all)
f = open(f"distances/distances_{model}_{dataset}_{opt}.json", "w")
json.dump(distances_all, f)
f.close()

#config_labels = ["a-a", *config_labels]
config_labels = ["", "R-R", "R-S", "S-S", "R-A", "S-A", "A-A"]
for _ in range(3):
    config_labels.extend(["R-R", "R-S", "S-S", "R-A", "S-A", "A-A"])
#config_labels.extend(["aa", "ra", "sa"])
config_labels.extend(["R-A", "S-A"])
print(config_labels)

for i in range(3):
    ax[i].xaxis.set_major_locator(matplotlib.ticker.FixedLocator(range(-1,26)))
    ax[i].set_xticklabels(config_labels)
    ax[i].tick_params(axis='x', labelsize=15)
    ax[i].tick_params(axis='y', labelsize=18)
    ax[i].set_xlabel("pair type", fontsize=22)
ax[0].set_ylabel("distance (per-parameter)", fontsize=22)
ax[0].set_title("left - right endpoints", fontsize=18)
ax[1].set_title("left endpoint - midpoint", fontsize=18)
ax[2].set_title("right endpoint - midpoint", fontsize=18)


#ax.legend(loc=(1,0.1))
ax[0].legend(loc="upper left", fontsize=15)

modelpl = "LeNet" if model=="TestLeNet" else model
plt.suptitle(f"{modelpl},  {dataset}", fontsize=18)
#plt.tight_layout()
plt.savefig(f"plots/figures/distances_zopt_points.png")
plt.savefig(f"plots/figures/distances_{model}_{dataset}.pdf")
