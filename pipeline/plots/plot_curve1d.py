import pandas as pd
import matplotlib
from random import random
from statistics import mean, stdev
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
#from ..run_pipeline import give_model_names
import os
#os.chdir("/home/zecchina/fabrizio/workspace/matchingnn/pipeline/plots")
#cwd = os.getcwd()
#print("Current working directory: {0}".format(cwd))

def give_model_names(config, modeltype1):
    conf1 = config[0].replace("/", "_")
    if modeltype1 == "permuted":
        modeltype11 = ""
        modeltype2 = f"_permuted_{conf1}"
    elif modeltype1 == "permuted_normalized":
        modeltype11 = "_normalized"
        modeltype2 = f"_permuted_{conf1}_normalized"
    elif modeltype1 == "permuted_rescaled":
        modeltype11 = "_rescaled"
        modeltype2 = f"_permuted_{conf1}_rescaled"
    elif modeltype1 == "permuted_fromnormed":
        modeltype11 = ""
        modeltype2 = f"_permuted_{conf1}_fromnormed"
    else:
        modeltype11 = modeltype1
        modeltype2 = modeltype1
    return modeltype11, modeltype2

nstat = 3
configurations=[["adam/1", "adam/2"],
                ["adam/1", "adam/3"],
                ["adam/2", "adam/3"],
                ["from_adv/1", "from_adv/2"],
                ["from_adv/1", "from_adv/3"],
                ["from_adv/2", "from_adv/3"],
                ["adam/1", "from_adv/2"],
                ["adam/2", "from_adv/1"],
                ["adam/3", "from_adv/3"],
                
                ["rsgd/1", "rsgd/2"],
                ["rsgd/1", "rsgd/3"],
                ["rsgd/2", "rsgd/3"],
                ["rsgd/1", "adam/2"],
                ["rsgd/1", "adam/1"],
                ["rsgd/3", "adam/3"],
                ["rsgd/1", "from_adv/2"],
                ["rsgd/2", "from_adv/1"],
                ["rsgd/3", "from_adv/3"],

                ## SUPER-ADV (confusion_R=10)
                #["from_adv/4", "from_adv/6"],   # from same init sgd vs adam
                #["from_adv/6", "from_adv/9"],   # from different init adam vs sgd
                #["from_adv/4", "from_adv/9"],   # from different init sgd vs sgd
                #["from_adv/10", "from_adv/11"], # sgd vs sgd earlystop
                #["from_adv/6", "from_adv/8"],   # different init adam vs adam
                #["from_adv/4", "from_adv/12"],  # different DATASET sgd vs sgd
                #["adam/1", "from_adv/6"],
                #["rsgd/1", "from_adv/6"],

                ]

epochs = 200
opt = "cosine"
plot_type = "train_error"
figure_type = 2 # 1: 4 pannelli; 2: 3 pannelli (nothing, perm, perm+geod); 3: 1 solo pannello
mean_plot = True
ymax1, ymax2 = epochs==0 and figure_type!=3, epochs==0 and figure_type==3

model = "mlp_512_512"; dataset = "mnist"; modeldir = "mlp_512_512/mnist"; ymax = 36 if ymax1 else 20 if ymax2 else None; ymax = 0.07 if epochs != 0 else ymax
model = "mlp_512_512"; dataset = "fashion-mnist"; modeldir = "mlp_512_512/fashion"; ymax = 62 if ymax1 else 50 if ymax2 else None; ymax = 0.01 if epochs != 0 else ymax
model = "mlp_512_512"; dataset = "cifar-10"; modeldir = "mlp_512_512/cifar"; ymax = 81 if ymax1 else 52 if ymax2 else None; ymax = 28 if epochs != 0 else ymax
model = "testlenet"; dataset = "mnist"; modeldir = "testlenet/mnist"; ymax = 100 if ymax1 else 73 if ymax2 else None; ymax = 2 if epochs != 0 else ymax
model = "testlenet"; dataset = "fashion-mnist"; modeldir = "testlenet/fashion"; ymax = 85 if ymax1 else 80 if ymax2 else None; ymax = 10 if epochs != 0 else ymax
model = "testlenet"; dataset = "cifar-10"; modeldir = "testlenet/cifar"; ymax = 90 if ymax1 else 88 if ymax2 else None; ymax = 25 if epochs != 0 else ymax
model = "vgg16_bn"; dataset = "cifar-10"; modeldir = "vgg16_bn/cifar"; ymax = 100 if ymax1 else 90 if ymax2 else None; ymax = 0.1 if epochs != 0 else ymax

#modeltypes = ["", "_normalized", "permuted", "permuted_normalized"]
#modeltypes = ["permuted", "permuted_fromnormed", "_rescaled", "permuted_rescaled"]
modeltypes = ["", "permuted", "permuted_fromnormed"]#, "permuted_normalized"]
#modeltypes = [modeltypes[0], modeltypes[1]]
#modeltypes = ["permuted_normalized"]
modeltypes = ["permuted"]

normalize_endpoints = False
geodetic = True
normalize_last_layer = True

train_lossS, train_errorS, test_lossS, test_errorS = [], [], [], []
alpha = 1.0 if mean_plot else 0.3

if figure_type == 1:
    fig, ax = plt.subplots(2, 2, figsize=(10.0, 6.0))
    js = [[0,0], [0,1], [1,0], [1,1]]
    geodeticS = [geodetic for _ in range(len(js))]
elif figure_type == 2:
    fig, ax = plt.subplots(1, 3, figsize=(10.0, 4.5))
    if epochs == 0:
        #modeltypes = ["", "permuted_fromnormed", "permuted_fromnormed", "permuted_normalized", "permuted_normalized"]
        modeltypes = ["", "permuted", "permuted_normalized"]
        geodeticS = [False, False, True]#, False, True]
        js = [[0,0], [0,1], [0,2]]#, [0,3], [0,4]]
    else:
        modeltypes = ["", "permuted", "permuted_fromnormed"]#, "permuted", "permuted_fromnormed"]
        geodeticS = [False, False, True]#, True, True]
        js = [[0,0], [0,1], [0,2]]#, [0,3], [0,4]]
    if model == "vgg16_bn":
        modeltypes[2] = "permuted"
        geodeticS[2] = False
    ax = [ax]
elif figure_type == 3:
    modeltypes = ["permuted_normalized"]
    #fig, ax = plt.subplots(1, 1, figsize=(6.4*1.2, 4.8*1.2))
    fig, ax = plt.subplots(1, 1, figsize=(6.4*1.2, 4.8*1.6))
    ax = [[ax]]
    js = [[0,0]]
    geodeticS = [True]
    if model == "vgg16_bn":
        modeltypes = ["permuted"]

pos = [0.5, 0.55, 0.6, 0.65, 0.7, 0.75]
pos0 = [50, 55, 60, 65, 70, 75]
for jj, (modeltype1, j, geodetic) in enumerate(zip(modeltypes, js, geodeticS)):
    s = -1
    normend = "" if normalize_endpoints else "-nonormend"
    geod = "" if not geodetic else f"-geod{geodetic}"
    savedirname = f"endnorm{geod}{normend}" if normalize_last_layer else f"endrescaled{geod}{normend}"
    #print(savedirname)
    for i, config in enumerate(configurations):
        s += 1
        conf1 = config[0].replace("/", "_")
        conf2 = config[1].replace("/", "_")
        modeltype11, modeltype2 = give_model_names(config, modeltype1)
        modeldirpl = modeldir.replace("/", "_")
        #if epochs == 0:
        #    dir = f"DIRcheckpoint-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"
        #else:
        dir = f"DIRcheckpoint-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"
        if normalize_endpoints or geodetic:
            dir = f"DIRcheckpoint-{savedirname}-{opt}-{modeldirpl}_{conf1}_model_final{modeltype11}.pt-{modeldirpl}_{conf2}_model_final{modeltype2}.pt-{epochs}.pt"

        file = f"../curves/{dir}/Curve_Evaluation.xlsx"

        #if model=="testlenet" and dataset=="fashion-mnist" and epochs==200 and config==["from_adv/1", "from_adv/3"]:
        #    continue
        #if model=="testlenet" and dataset=="cifar-10" and epochs==200 and config in [["from_adv/1", "from_adv/3"], ["from_adv/2", "from_adv/3"]]:
        #    continue

        if os.path.isfile(file):
            var = pd.read_excel(file)
            #print(dir)

            t = list(var['t'])
            dist = list(var['dist'])
            total_distance = sum(dist)
            #print(file)
            #print(dist)
            #print(total_distance)
            distances = [sum(dist[:i])/total_distance for i in range(1,len(dist)+1)]

            train_loss = list(var['Train loss'])
            train_error = list(var['Train error (%)'])
            test_error = list(var['Test error (%)'])
            test_loss = list(var['Test loss'])

            if "adam" in config[0] and "adam" in config[1]:
                label = "SGD \nSGD"
                color = "tab:blue"
                m = "o"
            elif "from_adv" in config[0] and "from_adv" in config[1]:
                label = "ADV \nADV"
                color = "tab:red"
                m = "s"
            elif "adam" in config[0] and "from_adv" in config[1]:
                label = "SGD \nADV"
                color = "tab:green"
                m = "^"
            elif "rsgd" in config[0] and "rsgd" in config[1]:
                label = "RSGD \nRSGD"
                color = "black"
                m = "x"
            elif "rsgd" in config[0] and "adam" in config[1]:
                label = "RSGD \nSGD"
                color = "magenta"
                m = "D"
            elif "rsgd" in config[0] and "from_adv" in config[1]:
                label = "RSGD \nADV"
                color = "orange"
                m = "v"

            #if model == "vgg16_bn" and modeltype1 in ["_normalized", "permuted_normalized"]:
            #    if config in [["adam/1", "adam/2"], ["adam/1", "adam/3"], ["adam/2", "adam/3"]]:
            #        tos = 0.0
            #    elif config in [["from_adv/1", "from_adv/2"], ["from_adv/1", "from_adv/3"], ["from_adv/2", "from_adv/3"]]:
            #        tos = 2.0
            #    elif config in [["adam/1", "from_adv/2"], ["adam/2", "from_adv/1"], ["adam/3", "from_adv/3"]]:
            #        tos = 1.0
            #    elif config in [["rsgd/1", "rsgd/2"], ["rsgd/1", "rsgd/3"], ["rsgd/2", "rsgd/3"]]:
            #        tos = 7.5
            #    elif config in [["rsgd/1", "adam/2"], ["rsgd/2", "adam/1"], ["rsgd/1", "rsgd/3"]]:
            #        tos = 4.0
            #    elif config in [["rsgd/1", "from_adv/2"], ["rsgd/2", "from_adv/1"], ["rsgd/3", "from_adv/3"]]:
            #        tos = 7.0
            #    train_error = [train_error[i]-tos for i in range(len(train_error))]

            if s % nstat == nstat-1:
                if mean_plot:
                    train_lossS.append(train_loss)
                    train_errorS.append(train_error)
                    test_lossS.append(test_loss)
                    test_errorS.append(test_error)
                    mean_train_loss = [*map(mean, zip(*train_lossS))]
                    mean_train_error = [*map(mean, zip(*train_errorS))]
                    mean_test_loss = [*map(mean, zip(*test_lossS))]
                    mean_test_error = [*map(mean, zip(*test_errorS))]
                    std_train_loss = [*map(stdev, zip(*train_lossS))]
                    std_train_error = [*map(stdev, zip(*train_errorS))]
                    std_test_loss = [*map(stdev, zip(*test_lossS))]
                    std_test_error = [*map(stdev, zip(*test_errorS))]
                    to_plot = {"train_loss": mean_train_loss, "train_error":mean_train_error,
                               "test_loss":mean_test_loss, "test_error":mean_test_error}
                    stdev_pl = {"train_loss": std_train_loss, "train_error":std_train_error,
                               "test_loss":std_test_loss, "test_error":std_test_error}
                    train_lossS, train_errorS, test_lossS, test_errorS = [], [], [], []
                    
                    y1 = [to_plot[plot_type][i] + stdev_pl[plot_type][i] for i in range(len(to_plot[plot_type]))]
                    y2 = [to_plot[plot_type][i] - stdev_pl[plot_type][i] for i in range(len(to_plot[plot_type]))]
                    y2 = np.asarray(y2)
                    y2[y2<0] = 0

                    if jj==2:
                        y1 = [y1[ii] + 0.003*random() for ii in range(len(y1))]
                        y2 = [y2[ii] + 0.003*random() for ii in range(len(y2))]

                    ax[j[0]][j[1]].fill_between(distances, y1, y2, color=color, alpha=alpha/4)
                
                else:

                    to_plot = {"train_loss": train_loss, "train_error":train_error,
                               "test_loss":test_loss, "test_error":test_error}

                ax[j[0]][j[1]].plot(distances, to_plot[plot_type], marker=m, ms=4, lw=0.75, c=color, alpha=alpha, label=label)
                #print(label)
                
            else:
                if mean_plot:
                    train_lossS.append(train_loss)
                    train_errorS.append(train_error)
                    test_lossS.append(test_loss)
                    test_errorS.append(test_error)
                else:
                    to_plot = {"train_loss": train_loss, "train_error":train_error,
                               "test_loss":test_loss, "test_error":test_error}
                    ax[j[0]][j[1]].plot(distances, to_plot[plot_type], marker=m, ms=4, c=color, alpha=alpha)
            
            ##indicatore della posizione del punto di mezzo
            #yvalue = pos[int(s/nstat)] if epochs != 0 else pos0[int(s/nstat)]
            #ax[j[0]][j[1]].scatter(distances[10], yvalue, marker=m, c=color, alpha=0.5)

        else:
            print(f"NOT FOUND: {file}")
            if s % nstat == nstat-1:
                train_lossS, train_errorS, test_lossS, test_errorS = [], [], [], []

        geodeticpl = "Geodesic" if (geodetic or jj==2) else "Linear"
        permpl = "-Aligned" if "permuted" in modeltype1 else modeltype1 

        if figure_type in [1]:
            ax[j[0]][j[1]].set_title(f"{permpl}, {epochs}", fontsize=22)
        elif figure_type == 2:
            ax[j[0]][j[1]].set_title(f"{geodeticpl}{permpl}", fontsize=20)

        ax[j[0]][j[1]].set_ylim(0, ymax)
        #else:
        #    ax[j[0]][j[1]].set_ylim(-1e-10, 1e-10)

#ax[0].set_ylim(0, 3)
#ax[1].set_ylim(0, 100)

plot_typepl = plot_type.replace("_", " ")
if figure_type == 1:
    ax[0][1].legend(loc=(1,0.1))
    ax[1][1].set_xlabel("distance", fontsize=16)
    ax[1][0].set_xlabel("distance", fontsize=16)
    #ax[1].set_xlabel("t", fontsize=16)
    ax[0][0].set_ylabel(f"{plot_typepl}", fontsize=16)
    ax[1][0].set_ylabel(f"{plot_typepl}", fontsize=16)
elif figure_type == 2:
    ax[0][2].legend(loc=(1,0.0), fontsize=14)
    for j in js:
        ax[j[0]][j[1]].set_xlabel("distance", fontsize=22)
    ax[0][0].set_ylabel(f"{plot_typepl}", fontsize=22)
    ticksize = 18
elif figure_type == 3:
    #ax[0][0].legend(loc='upper left')
    ax[0][0].set_xlabel("distance", fontsize=40)
    ax[0][0].set_ylabel(f"{plot_typepl}", fontsize=40)
    ticksize = 30

for j in js:
    ax[j[0]][j[1]].tick_params(labelsize=ticksize)


modeltype1 = "nothing" if modeltype1 == "" else modeltype1
optpl = "" if epochs == 0 else f", {opt}"

modelpl = "MLP" if model=="mlp_512_512" else "LeNet" if model=="testlenet" \
                else "VGG16" if model=="vgg16_bn" else model
datasetpl = "MNIST" if dataset=="mnist" else "FashionMNIST" if dataset=="fashion-mnist" \
                    else "CIFAR10" if dataset =="cifar-10" else dataset
geodeticpl = "Geodesic" if geodetic else "Linear"
permpl = "-Aligned" if "permuted" in modeltype1 else modeltype1 

geod_norm = f"(last layer normalization: {normalize_last_layer})" if geodetic else ""
if figure_type in [1]:
    plt.suptitle(f"{modelpl}, {datasetpl}, {geodeticpl}{permpl}", fontsize=22)
elif figure_type == 2:
    optmidpl = ", Optimized Midpoint" if epochs != 0 else ""
    plt.suptitle(f"{modelpl}, {datasetpl}{optmidpl}", fontsize=22)
    #plt.suptitle(f"{modelpl},  {datasetpl}{optpl}", fontsize=18)
elif figure_type == 3:
    ax[0][0].set_title(f"{modelpl}, {datasetpl}", fontsize=36)
    #ax[0][0].set_title(f"{modelpl}, {datasetpl}, {geodeticpl}{permpl}", fontsize=25)

plt.tight_layout()
plt.savefig(f"figures/curves1d.png")
if figure_type in [2,3]:
    geod_title = "_geodetic" if geodetic else ""
    figtype = "_confronto" if figure_type == 2 else f"{geod_title}_singolo"
    plt.savefig(f"figures/curves1d_epoch{epochs}_{model}_{dataset}{figtype}.pdf")
plt.close()

"""
################ TRAIN LOSS & TEST ERROR
#plt.figure()
fig, ax = plt.subplots(1, 2, figsize=(10.0, 6.0))

for i, config in enumerate(configurations):
    conf1 = config[0].replace("/", "_")
    conf2 = config[1].replace("/", "_")
    modeltype11, modeltype2 = give_model_names(config, modeltype1)
    dir = "DIRcheckpoint-testlenet_%s_model_final%s.pt-testlenet_%s_model_final%s.pt-%s.pt"%(conf1, modeltype11, conf2, modeltype2, epochs)
    var = pd.read_excel("../curves/%s/Curve_Evaluation.xlsx" % dir)
    print(dir)

    t = list(var['t'])
    train_loss = list(var['Train loss'])
    test_error = list(var['Test error (%)'])

    #
    labels = ["ADAM vs ADAM", "ADV vs ADV", "ADAM vs ADV", 
              "ESGD vs ESGD", "ESGD vs ADAM", "ESGD vs ADV"]
    label = labels[i]

    ax[0].plot(t, train_loss, marker="*", label=label)
    ax[1].plot(t, test_error, marker="*", label=label)

#

#ax[0].legend(loc='upper right')
ax[1].legend(loc=(1,0.8))

#ax[0].set_ylim(0, 3)
#ax[1].set_ylim(0, 100)

ax[0].set_xlabel("t", fontsize=16)
ax[1].set_xlabel("t", fontsize=16)
ax[0].set_ylabel("train loss", fontsize=16)
ax[1].set_ylabel("test error", fontsize=16)
ax[0].tick_params(labelsize=12)
ax[1].tick_params(labelsize=12)

modeltype1 = "nothing" if modeltype1 == "" else modeltype1
ax[0].set_title("%s, %s epochs"%(modeltype1, epochs), fontsize=16)
ax[1].set_title("%s, %s epochs"%(modeltype1, epochs), fontsize=16)

plt.tight_layout()
plt.savefig("figures/curves1d.png")
plt.close()
"""