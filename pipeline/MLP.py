import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Linear, Sequential, ReLU, Tanh, Conv2d, Flatten, MaxPool2d, Module, BatchNorm2d

import curves

class MLPBase(nn.Module):
    def __init__(self, nin, nhs, nout):
        super(MLPBase, self).__init__()
        assert isinstance(nhs, list)

        self.layers = nn.Sequential(
            nn.Linear(nin, nhs[0]),
            nn.ReLU(),
            *[nn.Linear(nhs[i//2], nhs[i//2 + 1]) if i%2==0 else nn.ReLU() for i in range(2*(len(nhs)-1))],
            nn.Linear(nhs[-1], nout))
        
    def forward(self, x):
        x = torch.flatten(x, start_dim=1)
        return self.layers(x)
    
class MLPCurve(nn.Module):
    def __init__(self, nin, nhs, nout, fix_points):
        super(MLPCurve, self).__init__()
        assert isinstance(nhs, list)

        self.layers = nn.Sequential(
            curves.Linear(nin, nhs[0], fix_points),
            nn.ReLU(),
            *[curves.Linear(nhs[i//2], nhs[i//2 + 1], fix_points) if i%2==0 else nn.ReLU() for i in range(2*(len(nhs)-1))],
            curves.Linear(nhs[-1], nout,fix_points))
        
    def forward(self, x, coeffs_t):
        x = torch.flatten(x, start_dim=1)
        x = self.layers[0](x, coeffs_t)
        x = self.layers[1](x)
        for i in range(2,4):
            if type(self.layers[i]) not in [Linear, Conv2d, BatchNorm2d, curves.Linear, curves.Conv2d, curves.BatchNorm2d]:
                x = self.layers[i](x)
            else:
                x = self.layers[i](x, coeffs_t)
        x = self.layers[-1](x, coeffs_t)
        return x


class MLP:
    base = MLPBase
    curve = MLPCurve
    kwargs = {}
