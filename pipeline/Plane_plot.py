def plane(grid,Logn_obj,values, vmax=None, log_alpha=-5, N=7, cmap='jet_r'):
    cmap = plt.get_cmap(cmap)
    if vmax is None:
        clipped = values.copy()
    else:
        clipped = np.minimum(values, vmax)
    log_gamma = (np.log(clipped.max() - clipped.min()) - log_alpha) / N
    levels = clipped.min() + np.exp(log_alpha + log_gamma * np.arange(N + 1))
    levels[0] = clipped.min()
    levels[-1] = clipped.max()
    levels = np.concatenate((levels, [1e10]))
    norm = Logn_obj(clipped.min() - 1e-8, clipped.max() + 1e-8, log_alpha=log_alpha)
    contour = plt.contour(grid[:, :, 0], grid[:, :, 1], values, cmap=cmap, norm=norm,
                          linewidths=2.5,
                          zorder=1,
                          levels=levels)
    contourf = plt.contourf(grid[:, :, 0], grid[:, :, 1], values, cmap=cmap, norm=norm,
                            levels=levels,
                            zorder=0,
                            alpha=0.55)
    colorbar = plt.colorbar(format='%.2g')
    labels = list(colorbar.ax.get_yticklabels())
    labels[-1].set_text(r'$>\,$' + labels[-2].get_text())
    colorbar.ax.set_yticklabels(labels)
    return contour, contourf, colorbar

def main():
    
    parser = argparse.ArgumentParser(description='Plane Plot')
    parser.add_argument('--file',required = True, type=str, default=None, metavar='DIR',
                            help='training directory (default: None')
    parser.add_argument('--save_dir',required = True, type=str, default=None, metavar='DIR',
                            help='training directory (default: None')
    parser.add_argument('--curve', type=str, default='PolyChain', metavar='CURVE',
                        help='curve type to use (default: PolyChain')
    
    args = parser.parse_args()
    
    file = np.load(args.file)
    

    matplotlib.rc('xtick.major', pad=12)
    matplotlib.rc('ytick.major', pad=12)
    matplotlib.rc('grid', linewidth=0.8)
    
    sns.set_style('whitegrid')
    
    class LogNormalize(colors.Normalize):

        def __init__(self, vmin=None, vmax=None, clip=None, log_alpha=None):
            self.log_alpha = log_alpha
            colors.Normalize.__init__(self, vmin, vmax, clip)
    
        def __call__(self, value, clip=None):
            log_v = np.ma.log(value - self.vmin)
            log_v = np.ma.maximum(log_v, self.log_alpha)
            return 0.9 * (log_v - self.log_alpha) / (np.log(self.vmax - self.vmin) - self.log_alpha)

    
    plt.figure(figsize=(12.4, 7))

    contour, contourf, colorbar = plane(
        file['grid'],
        LogNormalize,
        file['tr_err'],
        vmax=60,
        log_alpha=-5.0,
        N=7
    )
    
    bend_coordinates = file['bend_coordinates']
    curve_coordinates = file['curve_coordinates']
    
    
    plt.scatter(bend_coordinates[0][0], bend_coordinates[0][0], marker='o', c='k', s=120, zorder=2,linestyle='--')
    plt.scatter(bend_coordinates[2][0], bend_coordinates[0][0], marker='o', c='k', s=120, zorder=2,linestyle='--')
    if args.curve == 'PolyChain':
        plt.scatter(bend_coordinates[1][0] + 7, bend_coordinates[[0, 2], 0][1], marker='D', c='k', s=120, zorder=2)
    else:
        print('No D for you')
    x = np.linspace(0,bend_coordinates[1][0] + 7,len(curve_coordinates[:, 0]))

    def interpolate(p1,p2,xCoord):
        lip = []

        x0 = p1[0]
        y0 = p1[1]
        x1 = p2[0]
        y1 = p2[1]

        for x in xCoord:
            y = y0 + (x -x0)*((y1-y0)/(x1-x0))
            lip.append(y)
        
        return lip

    y = interpolate((0,0),(bend_coordinates[1][0] + 7,bend_coordinates[[0, 2], 0][1]),x)
    plt.plot(x,y, linewidth=4, c='k', label='$w(t)$', zorder=4,linestyle='--')
    plt.plot(bend_coordinates[[0, 2], 0], bend_coordinates[[0, 2], 1], c='k', linestyle='--', dashes=(3, 4), linewidth=3, zorder=2)
    
    plt.margins(0.0)
    plt.yticks(fontsize=18)
    plt.xticks(fontsize=18)
    colorbar.ax.tick_params(labelsize=18)
    plt.savefig('{}/train_loss.pdf'.format(args.save_dir), format='pdf', bbox_inches='tight')
    plt.show()
    
    plt.figure(figsize=(12.4, 7))
    
    contour, contourf, colorbar = plane(
        file['grid'],
        LogNormalize,
        file['te_err'],
        vmax=60,
        log_alpha=-0.0,
        N=7
    )
    
    bend_coordinates = file['bend_coordinates']
    curve_coordinates = file['curve_coordinates']
    
    
    plt.scatter(bend_coordinates[0][0], bend_coordinates[0][0], marker='o', c='k', s=120, zorder=2,linestyle='--')
    plt.scatter(bend_coordinates[2][0], bend_coordinates[0][0], marker='o', c='k', s=120, zorder=2,linestyle='--')
    if args.curve == 'PolyChain':
        plt.scatter(bend_coordinates[1][0] + 7, bend_coordinates[[0, 2], 0][1], marker='D', c='k', s=120, zorder=2)
    else:
        print('No D for you')
    x = np.linspace(0,bend_coordinates[1][0] + 7,len(curve_coordinates[:, 0]))
    y = interpolate((0,0),(bend_coordinates[1][0] + 7,bend_coordinates[[0, 2], 0][1]),x)
    print(x)
    print(y)
    print(curve_coordinates[:, 0])
    plt.plot(x,y, linewidth=4, c='k', label='$w(t)$', zorder=4,linestyle='--')
    plt.plot(bend_coordinates[[0, 2], 0], bend_coordinates[[0, 2], 1], c='k', linestyle='--', dashes=(3, 4), linewidth=3, zorder=2)
    
    plt.margins(0.0)
    plt.yticks(fontsize=18)
    plt.xticks(fontsize=18)
    colorbar.ax.tick_params(labelsize=18)
    plt.savefig("{}/test_loss.pdf".format(args.save_dir), format='pdf', bbox_inches='tight')
    plt.show()
    
if __name__ == '__main__':
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt
    import matplotlib.colors as colors
    import seaborn as sns
    import argparse
    
    main()
    
    
    
    
