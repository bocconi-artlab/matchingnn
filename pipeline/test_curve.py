def clean_dict(check,to_retain='center'):
    out = {}
    for i in check.keys():
        if i.split('.')[0] == to_retain: 
            out[i] = check[i]  
    return out

def initialize(model,curve,loaders,num_classes,num_bends,init_start,
               init_end,init_linear=True,is_rsgd=True,mixed=True):
    
    dim = loaders['train'].dataset[0][0].shape[2]
    in_channels = loaders['train'].dataset[0][0].shape[0]

    architecture = getattr(LeNet, model)

    curve = getattr(curves, curve)
    curve_model = curves.CurveNet(
        num_classes,
        in_channels,
        dim,
        curve,
        architecture.curve,
        num_bends,
        True,
        True,
        architecture_kwargs=architecture.kwargs,
    )

    base = [architecture.base(nclasses= num_classes,in_channels=in_channels,dim=dim, **architecture.kwargs) for _ in range(2)]
    for base_model, path, k in zip(base, [init_start, init_end], [0, num_bends - 1]):
        if path is not None:
            checkpoint = torch.load(path)[0]
            
            if is_rsgd:
                state_dict = {}
                checkpoint = clean_dict(checkpoint,'center')
                for i in checkpoint.keys():
                    print(i)
                    temp2 = re.sub("center.","",i)
                    state_dict[temp2] = checkpoint[i]
                    
                if mixed:
                    is_rsgd = False
                    
            else: 
                state_dict={}
                for i in checkpoint.keys():
                    print(i)
                    temp2 = re.sub("replicas.0.","",i)
                    state_dict[temp2] = checkpoint[i]
                if mixed:
                    is_rsgd = True
            print('Loading %s as point #%d' % (path, k))
            base_model.load_state_dict(state_dict)
        curve_model.import_base_parameters(base_model, k)
    
    if init_linear:
        print('Linear initialization.')
        curve_model.init_linear()
    curve_model.cuda()
    for base_model in base:
        base_model.cuda()
    return base,curve_model



def main():
    parser = argparse.ArgumentParser(description='Test DNN curve')

    parser.add_argument('--dir',required=True, type=str, default=None, metavar='PATH',
                        help='path to datasets location (default: None)')
    parser.add_argument('--dataset',required = True, type=str, default='FashionMNIST', metavar='DATASET',
                        help='dataset name (default: FashionMNIST)')
    parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                        help='input batch size (default: 128)')
    parser.add_argument('--model', type=str, default='LeNet', metavar='MODEL',
                        help='model name (default: LeNet)')
    
    parser.add_argument('--curve', type=str, default='PolyChain', metavar='CURVE',
                        help='curve type to use (default: PolyChain)')
    parser.add_argument('--init_start',required=True, type=str, default=None, metavar='CKPT',
                        help='checkpoint to init start point (default: None)')
    parser.add_argument('--init_end',required = True, type=str, default=None, metavar='CKPT',
                        help='checkpoint to init end point (default: None)')
    parser.add_argument('--seed', type=int, default=42, metavar='S', help='random seed (default: 42)')
    parser.add_argument('--mixed', dest='mixed', action='store_false',
                    help='turns off mixed (default: off)')
    parser.add_argument('--is_rsgd', dest='is_rsgd', action='store_false',
                   help='turns off if_rsgd (default: off)')
    
    parser.set_defaults(mixed=True)
    parser.set_defaults(is_rsgd=True)
    
    args = parser.parse_args()
    
    data_path = args.dir
    batch_size = args.batch_size
    random_seed = args.seed
    
    torch.backends.cudnn.benchmark = True ## Choose the fastest convolution algorithm
    torch.manual_seed(random_seed) ## Set random_seed to reproduce results
    
    basic_transform = getattr(Transforms,args.dataset)
    
    loaders, num_classes = data_new.loaders(
        args.dataset,
        basic_transform,
        data_path,
        batch_size,
        dl=True,
        use_test = True
    )
    
    loader = loaders['test']
    
    base,curve_model = initialize('LeNet',args.curve,loaders,10,3,args.init_start,
                                  args.init_end,is_rsgd=args.is_rsgd,mixed=args.mixed)
    
    t = torch.FloatTensor([0.0]).cuda()
    for base_model, t_value in zip(base, [0.0, 1.0]):
        print('T: %f' % t_value)
        t.data.fill_(t_value)
        curve_model.import_base_buffers(base_model)
        curve_model.eval()
        base_model.eval()
    
        max_error = 0.0
        for i, (input, _)in enumerate(loader):
            input = input.cuda(non_blocking=True)
    
            base_ouput = base_model(input)
            curve_output = curve_model(input, t)
    
            error = torch.max(torch.abs(base_ouput - curve_output)).item()
            print('Batch #%d. Error: %g' % (i, error))
            max_error = max(max_error, error)
        print('Max error: %g' % max_error)
        assert max_error < 1e-4, 'Error is too big (%g)' % max_error
        
if __name__ == '__main__':
    
    import argparse
    import torch
    import curves
    import data_new
    import LeNet
    import Transforms
    import re
    
    main()
    
        
    