import torch
from torch.functional import norm
import torch.nn.functional as F
from torch.nn import Linear, Conv2d, BatchNorm2d
from torch.utils.data import SubsetRandomSampler, DataLoader
from copy import deepcopy
import numpy as np
import math
import re
import os

# add sacreddnn and matchingnn dir to path
import os, sys
home_folder = os.getenv("HOME")
matchingnn_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sacreddnn_dir = home_folder + "/workspace/SacredDNN"
if not matchingnn_dir in sys.path:
    sys.path.insert(0, matchingnn_dir) 
if not sacreddnn_dir in sys.path:
    sys.path.insert(0, sacreddnn_dir)

from src.matching import match_sequential, get_parametric_inds, permute_sequential
from src.normalize import normalize_model
from sacreddnn.parse_args import get_model_type, get_loss_function, get_dataset
from sacreddnn.utils import num_params, l2_norm
from scripts.dnn import eval_loss_and_error

def calculate_euclidean_distance(net1, net2, npars):
    dist = 0.0
    for w1, w2 in zip(net1.parameters(), net2.parameters()):
        dist += F.mse_loss(w1, w2, reduction='sum')
    dist /= npars
    return torch.sqrt(dist).item()

def calculate_euclidean_distance_layers(net1, net2):
    layer_dists, layer_names, layer_numels = [], [], []
    for (name1, w1), (name2, w2) in zip(net1.named_parameters(), net2.named_parameters()):
        numel = w1.numel()
        dist_layer = F.mse_loss(w1, w2, reduction='sum') / numel
        layer_dists.append(torch.sqrt(dist_layer).item())
        layer_names.append(name1)
        layer_numels.append(numel)
        #print(name1, dist_layer, len(w1))
    return layer_dists, layer_names, layer_numels

class Args:
    def __init__(self, model, dataset):
        self.model = model
        self.dataset = dataset
        self.activation = None

        self.loss = "nll"
        self.preprocess = 1
        self.datapath = '~/data/'
        self.batch_size = 128

def substitute_esgd_keys(loaded_model):
    checkpoint_temp = loaded_model
    checkpoint={}
    for i in checkpoint_temp.keys():
        #print(i)
        temp2 = re.sub("replicas.0.", "", i)
        #temp2 = re.sub("model.","model.",i)
        checkpoint[temp2] = checkpoint_temp[i]
    return checkpoint

def to_gpuid_string(x):
    if isinstance(x, list):
        return str(x)[1:-1]
    return str(x)

def run_experiment(model_type, dataset, load_model1="adam/1", 
                                        load_model2="adam/2", 
                                        do_matching=True, normalize=False, 
                                        normalize_last_layer=True, normed_matching_nonormed_net=False,
                                        use_cuda=False, gpu_id=0, verbose=1):
    
    os.environ["CUDA_VISIBLE_DEVICES"] = to_gpuid_string(gpu_id)    
    device = torch.device(f"cuda" if use_cuda else "cpu")
    args = Args(model_type, dataset)

    # read models from file
    model_folder = "../configurations/"
    modeltype1 = load_model1.replace("/", "_")
    modeltype2 = load_model2.replace("/", "_")
    datasetpl = "cifar" if dataset=="cifar10" else dataset
    load_model1 = f"configs/{model_type}/{datasetpl}/{load_model1}/model_final.pt"
    load_model2 = f"configs/{model_type}/{datasetpl}/{load_model2}/model_final.pt"
    f = open(f"distances/dists_{model_type}_{dataset}_{modeltype1}_{modeltype2}_norm{normalize}_match{do_matching}.txt", "w")
    Net = get_model_type(args)

    model1 = Net().to(device)
    #print(torch.load(model_folder + load_model1, map_location=torch.device('cpu'))['net'])
    if model_type == "cnndescent":
        model1.load_state_dict(torch.load(model_folder + load_model1, map_location=torch.device('cpu'))['net'])
    else:
        loaded_model = torch.load(model_folder + load_model1, map_location=torch.device('cpu'))
        checkpoint = substitute_esgd_keys(loaded_model)
        model1.load_state_dict(checkpoint)
        
    model1.eval()

    model2 = Net().to(device)
    if model_type == "cnndescent":
        model2.load_state_dict(torch.load(model_folder + load_model2, map_location=torch.device('cpu'))['net'])
    else:
        loaded_model = torch.load(model_folder + load_model2, map_location=torch.device('cpu'))
        checkpoint = substitute_esgd_keys(loaded_model)
        model2.load_state_dict(checkpoint)
    
    model2.eval()
    model2_before_matching = deepcopy(model2)

    npars = num_params(model1)

    # get loss function and dataset for forward pass
    loss = get_loss_function(args)
    dtrain, dtest = get_dataset(args)
    train_idxs = list(range(len(dtrain)))
    test_idxs = list(range(len(dtest)))
    loader_args = {'pin_memory': True} if use_cuda else {}
    train_loader = DataLoader(dtrain,
        sampler=SubsetRandomSampler(train_idxs),
        batch_size=args.batch_size, **loader_args)
    test_loader = DataLoader(dtest,
        sampler=SubsetRandomSampler(test_idxs),
        batch_size=args.batch_size, **loader_args)

    # if I change model -> layers in testlenet I can avoid this if/elif
    if model_type in ["testlenet", "vgg16_bn", "resnet18"]:
        net1 = model1.model
        net2 = model2.model
    elif model_type == "mlp_512_512":
        net1 = model1.layers
        net2 = model2.layers
    elif model_type == "cnndescent":
        net1 = model1.module
        net2 = model2.module
    else:
        raise NameError("modeltype: %s" % model_type)

    print("\nAND THE MODEL IS...")
    print(net1)
    parametric_inds = get_parametric_inds(net1)

    # NORMALIZATION
    ################################################################################
    if normed_matching_nonormed_net:
        net2_nonormed = deepcopy(net2)
        do_matching = True
        normalize = True
        normalize_last_layer = True
        print("\nPERMUTATION FROM NORMED NETWORK\n")
    
    if normalize:

        def print_norm(layer, check_normalization=False):

            with torch.no_grad():
                num_units = len(layer.bias)
                if type(layer) == Linear:
                    unit_norms = [torch.sqrt(layer.weight[i, :].norm()**2).cpu() for i in range(num_units)]
                elif type(layer) == Conv2d:
                    unit_norms = [torch.sqrt(layer.weight[i, :, :, :].norm()**2).cpu() for i in range(num_units)]
                elif type(layer) == BatchNorm2d:
                    # no need to normalize BN when using ReLU (like bias, just rescale them)
                    unit_norms = [torch.tensor(1.0).cpu()]
                else:
                    print(layer)
                    print(layer.__dict__)
                    raise NameError("normalization not implemented for layer %s"%type(layer))
                
                layer_norm_squared = (layer.weight[:].norm()**2).cpu().item()

                print(layer, "mean layer units norm", np.mean(unit_norms), "layer_norm_squared", layer_norm_squared)
                #print(layer, "layer units norm", unit_norms)
                if check_normalization:
                    assert all(math.isclose(x.item(), 1.0, rel_tol=1e-3) for x in unit_norms)

                #layer_norm = torch.sqrt(layer.weight.norm()**2 + layer.bias.norm()**2)
                #layer_norm = torch.sqrt(layer.weight.norm()**2)
                #print(layer, "layer norm", layer_norm.item())

        # calculate Euclidean distance before normalization
        dist_before_normalization = calculate_euclidean_distance(net1, net2, npars)
        layer_dists, _, _ = calculate_euclidean_distance_layers(net1, net2)
        f.write("before_normalization " + " ".join(str(item) for item in layer_dists) + "\n")

        print(f"\nBEFORE NORMALIZATION (normalize_last_layer={normalize_last_layer}):")
        print("\n** mean distance: %.4e\n" % dist_before_normalization)
        for idx in parametric_inds:
            print_norm(net2[idx])

        # forward pass before normalization
        if verbose > 0:
            if verbose > 1:
                train_loss1_init, train_error1_init = eval_loss_and_error(loss, model1, device, train_loader)
                test_loss1_init, test_error1_init = eval_loss_and_error(loss, model1, device, test_loader)
                print("\n* trn_loss1: %.4e trn_err1: %.4e tst_loss1: %.4e tst_err1: %.4e" % (train_loss1_init, train_error1_init, test_loss1_init, test_error1_init))
            train_loss2_init, train_error2_init = eval_loss_and_error(loss, model2, device, train_loader)
            test_loss2_init, test_error2_init = eval_loss_and_error(loss, model2, device, test_loader)
            print("\n* trn_loss2: %.4e trn_err2: %.4e tst_loss2: %.4e tst_err2: %.4e\n" % (train_loss2_init, train_error2_init, test_loss2_init, test_error2_init))
        
        net1 = normalize_model(net1, normalize_last_layer=normalize_last_layer)
        net2 = normalize_model(net2, normalize_last_layer=normalize_last_layer)

        if model_type in ["testlenet", "vgg16_bn", "resnet18"]:
            model1.model = net1
            model2.model = net2
        elif model_type == "mlp_512_512":
            model1.layers = net1
            model2.layers = net2
        elif model_type == "cnndescent":
            model1.module = net1
            model2.module = net2
        else:
            raise NameError("model type: %s" % model_type)

        print("\nAFTER NORMALIZATION AND BEFORE MATCHING:\n")
        for i in range(len(parametric_inds)):
            idx = parametric_inds[i]
            check_normalization = True if i+1 < len(parametric_inds) else False
            print_norm(net2[idx], check_normalization=check_normalization)

    ################################################################################

    # MATCHING

    # calculate Euclidean distance before matching
    dist_before_matching = calculate_euclidean_distance(net1, net2, npars)
    layer_dists, _, _ = calculate_euclidean_distance_layers(net1, net2)
    f.write("before_matching " + " ".join(str(item) for item in layer_dists) + "\n")

    if not normalize:
        print("\nBEFORE MATCHING:")
    print("\n** mean distance: %.4e\n" % dist_before_matching)

    # forward pass before matching
    if verbose > 0:
        if verbose > 1:
            train_loss1_beforematch, train_error1_beforematch = eval_loss_and_error(loss, model1, device, train_loader)
            test_loss1_beforematch, test_error1_beforematch = eval_loss_and_error(loss, model1, device, test_loader)
            print("\n* trn_loss1: %.4e trn_err1: %.4e tst_loss1: %.4e tst_err1: %.4e" % (train_loss1_beforematch, train_error1_beforematch, test_loss1_beforematch, test_error1_beforematch))
        train_loss2_beforematch, train_error2_beforematch = eval_loss_and_error(loss, model2, device, train_loader)
        test_loss2_beforematch, test_error2_beforematch = eval_loss_and_error(loss, model2, device, test_loader)
        print("\n* trn_loss2: %.4e trn_err2: %.4e tst_loss2: %.4e tst_err2: %.4e\n" % (train_loss2_beforematch, train_error2_beforematch, test_loss2_beforematch, test_error2_beforematch))
    
        if normalize:
            assert math.isclose(train_error2_init, train_error2_beforematch, rel_tol=1e-3)
            assert math.isclose(test_error2_init, test_error2_beforematch, rel_tol=1e-3)
            #assert math.isclose(train_loss2_init, train_loss2_beforematch, rel_tol=1e-3)
            #assert math.isclose(test_loss2_init, test_loss2_beforematch, rel_tol=1e-3)

    if do_matching:

        p_list, net2 = match_sequential(net1, net2)

        if normed_matching_nonormed_net:
            net2 = permute_sequential(net2_nonormed, p_list)

        if model_type in ["testlenet", "vgg16_bn", "resnet18"]:
            model2.model = net2
        elif model_type == "mlp_512_512":
            model2.layers = net2
        elif model_type == "cnndescent":
            model2.module = net2
        else:
            raise NameError("model type: %s" % model_type)

    # calculate Euclidean distance after matching
    dist_after_matching = calculate_euclidean_distance(net1, net2, npars)
    layer_dists, layer_names, layer_numels = calculate_euclidean_distance_layers(net1, net2)
    f.write("after_matching " + " ".join(str(item) for item in layer_dists) + "\n")
    f.write("layer_names " + " ".join(str(item) for item in layer_names) + "\n")
    f.write("layer_numels " + " ".join(str(item) for item in layer_numels))

    print("\nFINAL: NORMALIZATION=%s, MATCHING=%s:"%(normalize, do_matching))
    print("\n** mean distance: %.4e" % dist_after_matching)

    # forward pass after matching
    if verbose > 0:
        train_loss2_aftermatch, train_error2_aftermatch = eval_loss_and_error(loss, model2, device, train_loader)
        test_loss2_aftermatch, test_error2_aftermatch = eval_loss_and_error(loss, model2, device, test_loader)
        print("\n* trn_loss2: %.4e trn_err2: %.4e tst_loss2: %.4e tst_err2: %.4e\n" % (train_loss2_aftermatch, train_error2_aftermatch, test_loss2_aftermatch, test_error2_aftermatch))

        assert math.isclose(train_error2_aftermatch, train_error2_beforematch, rel_tol=1e-3)
        assert math.isclose(test_error2_aftermatch, test_error2_beforematch, rel_tol=1e-3)
        #assert math.isclose(train_loss2_aftermatch, train_loss2_beforematch, rel_tol=1e-3)
        #assert math.isclose(test_loss2_aftermatch, test_loss2_beforematch, rel_tol=1e-3)

    #if model_type in ["testlenet", "vgg16_bn", "resnet18"]:
    #    equal_layers = [torch.eq(model2.model[i].weight, model2_before_matching.model[i].weight).all() for i in parametric_inds]
    #    assert not all(equal for equal in equal_layers)
    #elif model_type == "mlp_512_512":
    #    equal_layers = [torch.eq(model2.layers[i].weight, model2_before_matching.layers[i].weight).all() for i in parametric_inds]
    #    assert not all(equal for equal in equal_layers)
    #elif model_type == "cnndescent":
    #    equal_layers = [torch.eq(model2.module[i].weight, model2_before_matching.module[i].weight).all() for i in parametric_inds]
    #    assert not all(equal for equal in equal_layers)
    #else:
    #    assert not torch.eq(model2.layers[0].weight, model2_before_matching.layers[0].weight).all()

    # SAVE FINAL MODEL
    if not normed_matching_nonormed_net:
        if do_matching and normalize and normalize_last_layer:
            torch.save(model2.state_dict(), model_folder + load_model2[:-3] + f"_permuted_{modeltype1}_normalized.pt")
        elif do_matching and normalize and not normalize_last_layer:
            torch.save(model2.state_dict(), model_folder + load_model2[:-3] + f"_permuted_{modeltype1}_rescaled.pt")
        elif do_matching and not normalize:
            torch.save(model2.state_dict(), model_folder + load_model2[:-3] + f"_permuted_{modeltype1}.pt")
        elif not do_matching and normalize and normalize_last_layer:
            torch.save(model2.state_dict(), model_folder + load_model2[:-3] + "_normalized.pt")
        elif not do_matching and normalize and not normalize_last_layer:
            torch.save(model2.state_dict(), model_folder + load_model2[:-3] + "_rescaled.pt")

        if normalize and normalize_last_layer:
            torch.save(model1.state_dict(), model_folder + load_model1[:-3] + "_normalized.pt")
        elif normalize and not normalize_last_layer:
            torch.save(model1.state_dict(), model_folder + load_model1[:-3] + "_rescaled.pt")

    else:
        torch.save(model2.state_dict(), model_folder + load_model2[:-3] + f"_permuted_{modeltype1}_fromnormed.pt")

    f.close()
    return dist_before_matching, dist_after_matching
