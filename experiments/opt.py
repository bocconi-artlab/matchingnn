optimizer = torch.optim.Adam(
    filter(lambda param: param.requires_grad, model.parameters()),
    lr=args.lr,
    weight_decay=args.wd if args.curve is None else 0.0
)
