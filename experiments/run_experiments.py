import os
from experiments import run_experiment

configurations=[#["adam/1", "adam/2"],
                #["adam/1", "adam/3"],
                #["adam/2", "adam/3"],
                
                #["from_adv/1", "from_adv/2"],
                #["from_adv/1", "from_adv/3"],
                #["from_adv/2", "from_adv/3"],
                #["adam/1", "from_adv/2"],
                #["adam/2", "from_adv/1"],
                #["adam/3", "from_adv/3"],
                
                ["rsgd/1", "rsgd/2"],
                #["rsgd/1", "rsgd/3"],
                #["rsgd/2", "rsgd/3"],
                #["rsgd/1", "adam/2"],
                #["rsgd/1", "adam/1"],
                #["rsgd/3", "adam/3"],
                #["rsgd/1", "from_adv/2"],
                #["rsgd/2", "from_adv/1"],
                #["rsgd/3", "from_adv/3"],

                # SUPER-ADV (confusion_R=10)
                #["from_adv/4", "from_adv/6"], # from same init sgd vs adam
                #["from_adv/6", "from_adv/9"], # from different init adam vs sgd
                #["from_adv/4", "from_adv/9"], # from different init sgd vs sgd
                #["from_adv/10", "from_adv/11"], # sgd vs sgd earlystop different init
                #["from_adv/6", "from_adv/8"], # different init adam vs adam
                #["from_adv/4", "from_adv/12"], # different DATASET sgd vs sgd
                #["adam/1", "from_adv/6"],
                #["rsgd/1", "from_adv/6"],

                ]

#model = "mlp_512_512"; dataset = "mnist"
#model = "mlp_512_512"; dataset = "fashion"
#model = "mlp_512_512"; dataset = "cifar10"
#model = "testlenet"; dataset = "mnist"
#model = "testlenet"; dataset = "fashion"
#model = "testlenet"; dataset = "cifar10"
model = "vgg16_bn"; dataset = "cifar10"
#model = "resnet18"; dataset = "cifar10"

gpu_id = 1

normed_matching_nonormed_netS = [False] # questo uguale a True è per fare il matching con la permutazione trovata sulla rete normalizzata
#normed_matching_nonormed_netS = [False]

for normed_matching_nonormed_net in normed_matching_nonormed_netS:
    normalize_all = [True]
    matching_all = [False]
    normalize_last_layer = True

    if normed_matching_nonormed_net:
        normalize_all = [True]
        matching_all = [True]
        normalize_last_layer = True
    
    for config in configurations:
        for normalize in normalize_all:
            for do_matching in matching_all:
                print(config, f"model={model}, dataset={dataset}, NORMALIZE={normalize}, MATCHING={do_matching}")
                if not normalize and not do_matching:
                    continue
                run_experiment(model, dataset, load_model1=config[0], load_model2=config[1], 
                        normed_matching_nonormed_net = normed_matching_nonormed_net,
                        normalize=normalize, normalize_last_layer=normalize_last_layer,
                        do_matching=do_matching, use_cuda=True, gpu_id=gpu_id)
