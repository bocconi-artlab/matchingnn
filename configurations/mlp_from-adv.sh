seeds=(2 7 11 19 23 27)
folder=(1 2 3 4 5 6)
lrs=(0.02)
i=-1
dset=cifar
for seed in ${seeds[@]}; do
    echo $seed
    ((i++))
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/mlp_512_512/$dset/from_adv with load_model=configs/mlp_512_512/$dset/adv_init/${folder[i]}/model_final.pt seed=$seed dataset=cifar10 model=mlp_512_512 no_cuda=False logtime=10 save_model=True save_epoch=-1 batch_size=128 opt=nesterov droplr=cosine weight_decay=0.0 preprocess=1 gpu=2 activation=relu deterministic=True epochs=600 lr=$lr
    done
done
