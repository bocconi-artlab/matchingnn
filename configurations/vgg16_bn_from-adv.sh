#seeds=(31)
#folder=(4)
seeds=(37)
folder=(10)
lrs=(0.02)
#lrs=(0.001)
opt=sgd
i=-1
for seed in ${seeds[@]}; do
    echo $seed
    ((i++))
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/vgg16_bn/cifar/from_adv with load_model=../../BadGlobalMinima/cifar10/model_weight/vgg16/model_weight_adversarial_init_00/${folder[i]}/pre_confusion_10_zero_out_0.1.pt seed=$seed dataset=cifar10 model=vgg16_bn no_cuda=False logtime=5 save_model=True save_epoch=-1 batch_size=1024 opt=$opt droplr=0 drop_mstones=drop_150_250 weight_decay=0.0 preprocess=1 gpu=0 activation=relu deterministic=True epochs=300 lr=$lr earlystop=True
    done
done
