seeds=(19 23 27)
lrs=(0.1)
dset=fashion
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/testlenet/$dset/adv_init with seed=$seed dataset=$dset adv=True model=testlenet no_cuda=False logtime=10 save_model=True save_epoch=-1 batch_size=128 opt=sgd droplr=10.0 drop_mstones=drop_150_250 weight_decay=0.0 preprocess=1 gpu=2 activation=relu deterministic=True epochs=350 lr=$lr 
    done
done
