seeds=(2 7 11 19 23 29)
folder=(1 2 3 4 5 6)
lrs=(0.02)
i=-1
for seed in ${seeds[@]}; do
    echo $seed
    ((i++))
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/resnet18/cifar/from_adv with load_model=../../BadGlobalMinima/cifar10/model_weight/resnet18/model_weight_adversarial_init_00/${folder[i]}/pre_confusion_1_zero_out_0.1.pt seed=$seed dataset=cifar10 model=resnet18 no_cuda=False logtime=5 save_model=True save_epoch=-1 batch_size=128 opt=nesterov droplr=cosine weight_decay=0.0 preprocess=1 gpu=1 activation=relu deterministic=True epochs=300 lr=$lr
    done
done
