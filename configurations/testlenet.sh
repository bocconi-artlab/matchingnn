seeds=(2 7 11 19 23 27)
lrs=(0.02)
dset=cifar10
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/testlenet/cifar/adam with seed=$seed dataset=$dset model=testlenet no_cuda=False logtime=10 save_model=True save_epoch=-1 batch_size=128 opt=nesterov droplr=cosine weight_decay=0.0 dropout=0.0 preprocess=1 gpu=2 activation=relu deterministic=True epochs=600 lr=$lr
    done
done
