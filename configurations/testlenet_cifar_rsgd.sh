seeds=(2 7 11 19 23 27)
for seed in ${seeds[@]}; do
    echo $seed
    python3 ../../SacredDNN/scripts/robust_dnn.py -F configs/testlenet/cifar/rsgd with seed=$seed dataset=cifar10 model=testlenet use_center=False no_cuda=False logtime=10 save_model=True save_epoch=-1 batch_size=128 droplr=cosine opt=nesterov preprocess=1 gpu=2 deterministic=True activation=relu lr=0.02 epochs=600 y=5 g=None grate=None gmax=1
done
