seeds=(22)
dsets=(cifar10)
opts=(nesterov)
lrs=(0.01)
for seed in ${seeds[@]}; do
    echo $seed
    for dset in ${dsets[@]}; do
        echo $dset
        for opt in ${opts[@]}; do
            echo $opt
            for lr in ${lrs[@]}; do
                echo $lr
                python3 ../../sacreddnn/scripts/robust_dnn.py -F resnet18_cifar10_april/singleverylast with seed=$seed dataset=$dset model=resnet18 use_center=False no_cuda=False logtime=2 save_model=True save_epoch=-1 bs=128 drop_mstones=drop_110 opt=$opt droplr=10. preprocess=2 gpu=0 det=True activation=relu epochs=160 y=1 lr=$lr 
            done
        done
    done
done
