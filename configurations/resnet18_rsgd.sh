seeds=(1 2 3 4 5 6)
for seed in ${seeds[@]}; do
    echo $seed
    python3 ../../SacredDNN/scripts/robust_dnn.py -F configs/resnet18/cifar/rsgd with seed=$seed dataset=cifar10 model=resnet18 use_center=False no_cuda=False logtime=10 save_model=True save_epoch=-1 batch_size=128 droplr=cosine opt=nesterov preprocess=1 gpu=1 deterministic=True activation=relu lr=0.05 epochs=300 y=5 gtime=10 g=None grate=None
done
