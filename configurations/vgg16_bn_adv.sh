seeds=(2)
lrs=(0.1)
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/vgg16_bn/adv_init with seed=$seed dataset=cifar10 adv=True model=badvgg no_cuda=False logtime=5 save_model=True save_epoch=-1 batch_size=128 opt=sgd droplr=10.0 drop_mstones=drop_150_250  weight_decay=0.0 preprocess=1 gpu=2 activation=relu deterministic=True epochs=350 lr=$lr
    done
done
