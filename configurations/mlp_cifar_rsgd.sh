seeds=(2 7 11 19 23 27)
lrs=(0.02)
for seed in ${seeds[@]}; do
   echo $seed
   python3 ../../SacredDNN/scripts/robust_dnn.py -F configs/mlp_512_512/cifar/rsgd with seed=$seed dataset=cifar10 model=mlp_512_512 use_center=False no_cuda=False logtime=10 save_model=True save_epoch=-1 batch_size=128 droplr=cosine opt=nesterov preprocess=1 gpu=2 deterministic=True activation=relu lr=0.02 epochs=600 y=7 g=None grate=1e-3 gtime=10
done
