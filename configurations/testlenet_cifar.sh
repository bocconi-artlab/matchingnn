seeds=(2 7 11)
lrs=(0.002)
dset=cifar10
opt=nesterov
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/testlenet/cifar/adam with seed=$seed dataset=$dset model=testlenet no_cuda=False logtime=10 save_model=True save_epoch=-1 batch_size=128 opt=$opt droplr=cosine drop_mstones=drop_300_450 weight_decay=0.0 dropout=0.0 preprocess=1 gpu=1 activation=relu deterministic=True epochs=600 lr=$lr
    done
done
