seeds=(2 7 11 19 23 27)
dset=fashion
for seed in ${seeds[@]}; do
    echo $seed
    python3 ../../SacredDNN/scripts/robust_dnn.py -F configs/testlenet/$dset/rsgd with seed=$seed dataset=$dset model=testlenet use_center=False no_cuda=False logtime=10 save_model=True save_epoch=-1 batch_size=128 droplr=cosine opt=nesterov preprocess=1 gpu=2 deterministic=True activation=relu lr=0.05 epochs=600 y=7 g=None grate=None gtime=10 gmax=1
done
