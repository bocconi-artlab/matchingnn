seeds=(2 7 11 19 23 29)
lrs=(0.02)
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/resnet18/cifar/adam with seed=$seed dataset=cifar10 model=resnet18 no_cuda=False logtime=5 save_model=True save_epoch=-1 batch_size=128 opt=nesterov droplr=cosine weight_decay=0.0 dropout=0. preprocess=1 gpu=1 activation=relu deterministic=True epochs=300 lr=$lr
    done
done
