seeds=(2 7 11)
lrs=(0.1)
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../sacreddnn/scripts/dnn.py -F configs/testlenet/adv_init with seed=$seed dataset=fashion adv=True model=testlenet no_cuda=False logtime=5 save_model=True save_epoch=-1 batch_size=128 opt=sgd droplr=10.0 drop_mstones=drop_150_250 weight_decay=0.0 preprocess=1 gpu=0 activation=relu deterministic=True epochs=350 lr=$lr &
    done
done
