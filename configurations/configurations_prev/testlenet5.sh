seeds=(11)
lrs=(0.001)
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../sacreddnn/scripts/dnn.py -F testlenet/relu with seed=$seed dataset=fashion model=testlenet no_cuda=False logtime=3 save_model=True save_epoch=-1 batch_size=128 opt=adam droplr=0. drop_mstones=drop_150_225 weight_decay=1e-4 dropout=0. preprocess=1 gpu=2 activation=relu deterministic=True epochs=100 lr=$lr
    done
done
