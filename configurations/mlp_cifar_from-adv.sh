#seeds=(2 7 11 19 23 27)
#folder=(1 2 3 4 5 6)
seeds=(2 7 11)
folder=(1 2 3)
lrs=(0.01)
i=-1
dset=cifar
opt=nesterov
for seed in ${seeds[@]}; do
    echo $seed
    ((i++))
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/mlp_512_512/$dset/from_adv with load_model=configs/mlp_512_512/$dset/adv_init/${folder[i]}/model_final.pt seed=$seed dataset=cifar10 model=mlp_512_512 no_cuda=False logtime=10 save_model=True save_epoch=-1 batch_size=128 opt=$opt droplr=5.0 drop_mstones=drop_1000_1500 weight_decay=0.0 preprocess=1 gpu=1 activation=relu deterministic=True epochs=2000 lr=$lr
    done
done
