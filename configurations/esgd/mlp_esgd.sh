seeds=(2)
lrs=(0.5)
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/esgd_dnn.py -F configs/mlp_512_512/esgd with seed=$seed dataset=mnist model=mlp_512_512 no_cuda=False logtime=4 save_model=True save_epoch=-1 batch_size=128 opt=entropy-sgd droplr=cosine drop_mstones=drop_60_90 weight_decay=0.0 dropout=0.0 preprocess=1 gpu=1 activation=relu deterministic=True epochs=120 lr=$lr L=5 sgld_lr=0.02 g=0.5 grate=2e-5
    done
done
