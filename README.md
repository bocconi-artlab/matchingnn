# matchingNN

Dealing with symmetries in neural networks

CIAO


## Usage

Navigate to the `experiments/` folder and import the function for performing matching experiments in an (I)Python REPL with:

```python
from experiments import run_experiment
```

and then run the experiments with

```python
run_experiment(model_type, dataset, load_model1=path_to_first_model, load_model2=path_to_second_model, normalize=False)
```

`model_type` and `dataset` are the same strings that are passed as arguments in `sacreddnn`. 

If you want also to normalize the network (it must have ReLU activation functions) set the keyword argument to `normalize=True`

You should have also a folder with `sacreddnn` somewhere.

*NB*: Check the variables `sacreddnn_dir` and `model_folder` as your folder hierarchy may be different.

To calculate the model stabilities navigate to the `plots/` folder and in IPython run:
```python
import plot_stability
plot_stability.plot_stability(model_type, dataset, load_model=path_to_model)
```
The figures are saved in the `plots/figures` folder.
