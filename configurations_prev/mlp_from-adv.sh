seeds=(11)
lrs=(0.1)
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/mlp_512_512/from_adv with load_model=configs/mlp_512_512/adv_init/3/model_final.pt seed=$seed dataset=mnist model=mlp_512_512 no_cuda=False logtime=5 save_model=True save_epoch=-1 batch_size=128 opt=sgd droplr=0.0 weight_decay=0.0 preprocess=1 gpu=2 activation=relu deterministic=True epochs=1000 lr=$lr
    done
done
