seeds=(2 7 11)
lrs=(0.5)
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/esgd_dnn.py -F configs/vgg16_bn/esgd with seed=$seed dataset=cifar10 model=vgg16_bn no_cuda=False logtime=2 save_model=True save_epoch=-1 batch_size=128 opt=entropy-sgd droplr=0. drop_mstones=drop_150_225 weight_decay=5e-4 dropout=0. preprocess=1 gpu=1 activation=relu deterministic=True epochs=30 lr=$lr L=5 sgld_lr=0.02 g=0. grate=0.0
    done
done
