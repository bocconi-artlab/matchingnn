seeds=(11)
lrs=(0.1)
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/testlenet/from_adv with load_model=configs/testlenet/adv_init/3/model_final.pt seed=$seed dataset=fashion model=testlenet no_cuda=False logtime=5 save_model=True save_epoch=-1 batch_size=128 opt=sgd droplr=0.0 weight_decay=0.0 preprocess=1 gpu=1 activation=relu deterministic=True epochs=1000 lr=$lr
    done
done
