seeds=(2 7)
lrs=(0.001)
for seed in ${seeds[@]}; do
    echo $seed
    for lr in ${lrs[@]}; do
        echo $lr
        python3 ../../SacredDNN/scripts/dnn.py -F configs/vgg16_bn/adam with seed=$seed dataset=cifar10 model=vgg16_bn no_cuda=False logtime=5 save_model=True save_epoch=-1 batch_size=128 opt=adam droplr=0. drop_mstones=drop_150_225 weight_decay=0.0 dropout=0. preprocess=1 gpu=1 activation=relu deterministic=True epochs=100 lr=$lr &
    done
done
