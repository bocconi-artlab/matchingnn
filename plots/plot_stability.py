from matplotlib import pyplot as plt
import os
import numpy as np
import statistics

home_folder = os.getenv("HOME")
from calculate_stability import calculate_stability

def plot_stability(model_type, dataset, load_model="MLP/mlp_mnist/relu/1/model_final.pt", use_cuda=False):

    model_folder = home_folder + "/workspace/expDeepNets/"

    file = model_folder+load_model[:-3]+"_stabilities.dat"

    #if not os.path.isfile(file):
    stabilities, _, _ = calculate_stability(model_type, dataset, load_model=load_model, use_cuda=use_cuda)
    np.savetxt(file, stabilities)
    #else:
    #    stabilities = np.loadtxt(file)

    print("median stability:", statistics.median(stabilities))
    plt.hist(stabilities, density=True, bins=100, alpha=0.75,
             label="median stability:%s"%round(statistics.median(stabilities),2))
    plt.xlabel("stabilities", fontsize=14)
    plt.ylabel("P(stabilities)", fontsize=14)

    plt.legend()

    plt.savefig("figures/fig_stabilities-%s.png"%load_model[:-3].replace('/', '-'))
    plt.close()