import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
import os
os.chdir("/home/fabrizio/workspace/matchingnn/plots")

model = "mlp_512_512"; dataset = "mnist"
#model = "testlenet"; dataset = "fashion"
#model = "testlenet"; dataset = "cifar10"
#model = "vgg16_bn"; dataset = "cifar10"

configurations=[["adam/1", "adam/2"],
                ["adam/1", "adam/3"],
                ["adam/2", "adam/3"],
                ["from_adv/1", "from_adv/2"],
                ["from_adv/1", "from_adv/3"],
                ["from_adv/2", "from_adv/3"],
                ["adam/1", "from_adv/2"],
                ["adam/2", "from_adv/1"],
                ["adam/3", "from_adv/3"],
                
                ["rsgd/1", "rsgd/2"],
                ["rsgd/1", "rsgd/3"],
                ["rsgd/2", "rsgd/3"],
                ["rsgd/1", "adam/2"],
                ["rsgd/2", "adam/1"],
                ["rsgd/3", "adam/3"],
                ["rsgd/1", "from_adv/2"],
                ["rsgd/2", "from_adv/1"],
                ["rsgd/3", "from_adv/3"],
                ]

tickpositions = [0,1]
marks = ["o", "s", "^", "x", "D", "v"]
colors = ["tab:blue", "tab:red", "tab:green", "black", "magenta", "orange"]
labels = ["SGD vs SGD", "ADV vs ADV", "SGD vs ADV", "RSGD vs ESGD", "RSGD vs SGD", "RSGD vs ADV"]

nstat = 3

if model == "testlenet":
    nlayers = 4
    lay_range = [*range(1, nlayers*2, 2)]
elif model == "mlp_512_512":
    nlayers = 3
    lay_range = [*range(1, nlayers*2, 2)]
elif model == "vgg16_bn":
    nlayers = 16
    lay_range = [*range(1, 52, 4), *range(53, 58, 2)]
else:
    raise NameError

if model != "vgg16_bn":
    lay_axes = [[0,i] for i in range(nlayers)]
    lay_axes_norm = [[1,i] for i in range(nlayers)]
    fig, ax = plt.subplots(2, nlayers, figsize=(12.0, 8.0))
else:
    lay_axes = [[0,i] for i in range(8)]
    lay_axes_norm = [[1,i] for i in range(8)]
    lay_axes.extend([[2,i] for i in range(8)])
    lay_axes_norm.extend([[3,i] for i in range(8)])
    fig, ax = plt.subplots(4, 8, figsize=(18.0, 8.0))

print(lay_axes)

def calculate_layer_distance(dists, npars):
    dist_layer = 0.0
    for d,n in zip(dists, npars):
        dist_layer += n*float(d)**2
    return np.sqrt(dist_layer / sum(npars))

j = -1
for i, config in enumerate(configurations):

    conf1 = config[0].replace("/", "_")
    conf2 = config[1].replace("/", "_")

    filename1 = f"../experiments/distances/dists_{model}_{dataset}_{conf1}_{conf2}_normTrue_matchTrue.txt"
    filename2 = f"../experiments/distances/dists_{model}_{dataset}_{conf1}_{conf2}_normFalse_matchTrue.txt"

    if os.path.isfile(filename1) and os.path.isfile(filename2):
        dist1 = pd.read_csv(filename1, header=None, sep=" ")
        dist2 = pd.read_csv(filename2, header=None, sep=" ")
        if i % nstat == 0:
            j += 1

        #print(dist1, "\n", dist2, "\n")
        print(dist1, "\n", "\n")
        #print(dist2[0][0], dist2[0][1])
        #print(dist1[0][1], dist1[0][2])
        #raise NameError
            
        lay_idx = 0
        for (layer, l, lnorm) in zip(lay_range, lay_axes, lay_axes_norm):
            lay_idx += 1

            if model != "vgg16_bn" or (model=="vgg16_bn" and layer >= 53):
                layer_range = 2
            else:
                # batchnorm layers VGG
                layer_range = 4

            npars = [float(dist2[layer+i][3]) for i in range(layer_range)]
            dist_before_match = calculate_layer_distance([dist2[layer+i][0] for i in range(layer_range)], npars)
            dist_after_match = calculate_layer_distance([dist2[layer+i][1] for i in range(layer_range)], npars)
            dist_after_norm = calculate_layer_distance([dist1[layer+i][1] for i in range(layer_range)], npars)
            dist_after_norm_and_match = calculate_layer_distance([dist1[layer+i][2] for i in range(layer_range)], npars)
            #print(npars)

            distances = [dist_before_match, dist_after_match]
            distances_normalized = [dist_after_norm, dist_after_norm_and_match]

            if i % nstat == 0:
                ax[l[0]][l[1]].plot(tickpositions, distances, marker=marks[j], ms=6, c=colors[j], alpha=0.6,
                        label=f"{config[0][:-2]} vs {config[1][:-2]}")
                ax[lnorm[0]][lnorm[1]].plot(tickpositions, distances_normalized, marker=marks[j], ms=6, c=colors[j], alpha=0.6,
                        label=f"{config[0][:-2]} vs {config[1][:-2]}")
            else:
                ax[l[0]][l[1]].plot(tickpositions, distances, marker=marks[j], ms=6, c=colors[j], alpha=0.6)
                ax[lnorm[0]][lnorm[1]].plot(tickpositions, distances_normalized, marker=marks[j], ms=6, c=colors[j], alpha=0.6)
            
            ax[l[0]][l[1]].set_title(f"layer {lay_idx}")

    else:
        
        print(f"NOT FOUND: {filename1} or {filename2}")



#ax.set_ylim(0.04, 0.06)

#ax[0].legend(loc="best", fontsize=11)
ax[0][-1].legend(loc=(1,0.6), fontsize=11)

ax[0][0].set_ylabel("mean distance (per-parameter)", fontsize=12)

for l in lay_axes:
    ax[l[0]][l[1]].set_xticks(tickpositions)
    xticklabels1 = ["nothing", "matching"]
    ax[l[0]][l[1]].set_xticklabels(xticklabels1)
    ax[l[0]][l[1]].tick_params(labelsize=10)

for lnorm in lay_axes_norm:
    ax[lnorm[0]][lnorm[1]].set_xticks(tickpositions)
    xticklabels2 = ["norm", "norm+matching"]
    ax[lnorm[0]][lnorm[1]].set_xticklabels(xticklabels2)
    ax[lnorm[0]][lnorm[1]].tick_params(labelsize=10)

plt.suptitle(f"{model},  {dataset}", fontsize=20)
plt.tight_layout()
plt.savefig(f"figures/dists.png")
plt.savefig(f"figures/dists_{model}.pdf")
plt.close()

