import torch
from torch.utils.data import SubsetRandomSampler, DataLoader

# add sacreddnn and matchingnn dir to path
import os, sys
home_folder = os.getenv("HOME")
matchingnn_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sacreddnn_dir = home_folder + "/workspace/sacreddnn"
if not matchingnn_dir in sys.path:
    sys.path.insert(0, matchingnn_dir) 
if not sacreddnn_dir in sys.path:
    sys.path.insert(0, sacreddnn_dir)

from sacreddnn.parse_args import get_model_type, get_loss_function, get_dataset

class Args:
    def __init__(self, model, dataset):
        self.model = model
        self.dataset = dataset
        self.activation = None

        self.loss = "nll"
        self.preprocess = 1
        self.datapath = '~/data/'
        self.batch_size = 60000


def calculate_stability(model_type, dataset, load_model="MLP/mlp_mnist/relu/1/model_final.pt", use_cuda=False):

    device = torch.device(f"cuda" if use_cuda else "cpu")
    args = Args(model_type, dataset)

    dtrain, dtest = get_dataset(args)
    train_idxs = list(range(len(dtrain)))
    loader_args = {'pin_memory': True} if use_cuda else {}
    train_loader = DataLoader(dtrain, batch_size=args.batch_size, **loader_args)

    # read models from file
    model_folder = home_folder + "/workspace/expDeepNets/"
    Net = get_model_type(args)

    model = Net().to(device)
    model.load_state_dict(torch.load(model_folder + load_model))
    model.eval()

    print("FORWARD PASS...")
    outputs, targets = [], []
    with torch.no_grad():
        for data, target in train_loader:
            output = model(data)
            outputs.append(output)
            targets.append(target)

    print("CALCULATING STABILITIES...")
    num_examples = 0
    stabilities = []
    
    for o, targ in zip(outputs, targets):
        for i in range(args.batch_size):
            num_examples += 1
            oo = o[i,:]
            label = targ[i].item()

            first_two_max = torch.topk(oo, 2)
            first_max = first_two_max.values[0]
            second_max = first_two_max.values[1]

            if label == first_two_max.indices[0]:
                stab = 1
                #stability = (first_max - second_max).item() # ok
            else:
                stab = -1
                #stability = (oo[label] - first_max).item() # qui potrebbe essere che oo[label] non è il second_max quindi è un pochino diverso

            #stability = abs(first_max - second_max).item() # non prende in considerazione se è giusto o sbagliato
            stability = stab * abs(first_max - second_max).item() # giusto
            stabilities.append(stability)

    return stabilities, num_examples, output.size()