import torch
from torch.nn import Linear, Sequential, ReLU, Tanh, Conv2d, Flatten, MaxPool2d, BatchNorm2d, AdaptiveAvgPool2d
from typing import List
from scipy.optimize import linear_sum_assignment
from copy import deepcopy
from functools import singledispatch
import numpy as np

# document compatible modules
COMPAT_PARAMETRIC = (Linear, Conv2d, BatchNorm2d)
COMPAT_ACT = (Tanh, ReLU, MaxPool2d, Flatten, AdaptiveAvgPool2d)

# this checks for compatibility 
def check_compatibility(seq: Sequential):

    for layer in seq:
        if not isinstance(layer, COMPAT_PARAMETRIC) and not isinstance(layer, COMPAT_ACT):
            raise ValueError("layer type not compatible: {}".format(type(layer))) 

    return True

def check_equal_types(seq1: Sequential, seq2: Sequential):

    for i in range(len(seq1)):
        assert type(seq1[i]) == type(seq2[i])

    return True

def get_parametric_inds(seq: Sequential):

    parametric_inds = []
    for ind, layer in enumerate(seq):
        if isinstance(layer, COMPAT_PARAMETRIC):
            parametric_inds.append(ind)

    return parametric_inds


# PERMUTING METHODS

@singledispatch
def permute_output(layer, p, inplace=True):
    raise NotImplementedError("Not implemented for {}".format(type(layer)))

@singledispatch
def permute_input(layer, p, inplace=True):
    raise NotImplementedError("Not implemented for {}".format(type(layer)))

@permute_output.register(Linear)
def _(layer: Linear, p, inplace=True):

    assert isinstance(layer, Linear)
    assert layer.weight.shape[0] == len(p)

    if not inplace:
        layer = deepcopy(layer)

    if len(p) == 1:
        return layer

    with torch.no_grad():
        layer.weight[:] = layer.weight[p, :]
        layer.bias[:] = layer.bias[p]

    return layer

@permute_input.register(Linear)
def _(layer: Linear, p, inplace=True):

    assert isinstance(layer, Linear)
    assert layer.weight.shape[1] == len(p)

    if not inplace:
        layer = deepcopy(layer)

    if len(p) == 1:
        return layer

    with torch.no_grad():
        layer.weight[:] = layer.weight[:, p]

    return layer

@permute_output.register(Conv2d)
def _(layer: Conv2d, p, inplace=True):

    assert isinstance(layer, Conv2d)
    assert layer.weight.shape[0] == len(p)

    if not inplace:
        layer = deepcopy(layer)

    if len(p) == 1:
        return layer

    with torch.no_grad():
        layer.weight[:] = layer.weight[p, :, :, :]
        layer.bias[:] = layer.bias[p]

    return layer

@permute_input.register(Conv2d)
def _(layer: Conv2d, p, inplace=True):

    assert isinstance(layer, Conv2d)
    assert layer.weight.shape[1] == len(p)

    if not inplace:
        layer = deepcopy(layer)

    if len(p) == 1:
        return layer

    with torch.no_grad():
        layer.weight[:] = layer.weight[:, p, :, :]

    return layer

#def permute_input_BasicBlock(layer: BasicBlock, p, inplace=True):
#
#    assert isinstance(layer, Conv2d)
#    assert layer.weight.shape[1] == len(p)
#
#    if not inplace:
#        layer = deepcopy(layer)
#
#    if len(p) == 1:
#        return layer
#
#    with torch.no_grad():
#        permute_sequential()
#
#    return layer

def permute_input_conv_to_lin(layer: Linear, p, inplace=True):

    assert isinstance(layer, Linear)
    Nout = layer.weight.shape[0]
    Nin = layer.weight.shape[1]
    assert Nin % len(p) == 0

    if not inplace:
        layer = deepcopy(layer)

    if len(p) == 1:
        return layer

    with torch.no_grad():
        layer_weights = layer.weight.reshape(Nout, len(p), -1)
        print("(matching) Conv2d->Linear, reshaping from:", layer.weight.shape, "to:", layer_weights.shape)
        layer_weights = layer_weights[:,p,:]
        layer_weights = layer_weights.reshape(-1, Nin)
        layer.weight[:] = layer_weights

    return layer


@permute_input.register(BatchNorm2d)
def _(layer: BatchNorm2d, p, inplace=True):
    
    assert isinstance(layer, BatchNorm2d)
    assert layer.weight.shape[0] == len(p)

    if not inplace:
        layer = deepcopy(layer)

    if len(p) == 1:
        return layer

    with torch.no_grad():
        layer.weight[:] = layer.weight[p]
        layer.bias[:] = layer.bias[p]
        layer.running_mean[:] = layer.running_mean[p]
        layer.running_var[:] = layer.running_var[p]

    return layer


# MATCHING METHODS

@singledispatch
def match(layer1, layer2):
    raise NotImplementedError("No match function for {}".format(type(layer1)))

@match.register(Conv2d)
def _(layer1, layer2) -> List:

    assert type(layer1) == type(layer2)
    assert layer1.weight.shape == layer2.weight.shape

    nchannels = layer1.weight.shape[0]
    nparams = np.prod(layer1.weight.shape[1:])

    layer1_weights = layer1.weight.reshape(nchannels, -1)
    layer1_bias = layer1.bias

    layer2_weights = layer2.weight.reshape(nchannels, -1)
    layer2_bias = layer2.bias

    linear1 = Linear(nparams, nchannels)
    linear2 = Linear(nparams, nchannels)

    linear1.weight[:] = layer1_weights[:]
    linear2.weight[:] = layer2_weights[:]

    linear1.bias[:] = layer1_bias[:]
    linear2.bias[:] = layer2_bias[:]

    p = match(linear1, linear2)

    return p


@match.register(Linear)
def _(layer1, layer2) -> List:

    assert type(layer1) == type(layer2)
    assert layer1.weight.shape == layer2.weight.shape

    # if there is only one neuron, there is not need to match
    if layer1.weight.shape[0] == 1:
        return [0]

    ninputs = layer1.weight.shape[1]

    with torch.no_grad():

        w1 = layer1.weight
        w2 = layer2.weight
        b1 = layer1.bias
        b2 = layer2.bias

        if ninputs > 1:

            norm1 = torch.sqrt(torch.norm(w1, dim=1)**2 + b1**2)
            norm2 = torch.sqrt(torch.norm(w2, dim=1)**2 + b2**2)

            cost = - (w1 @ w2.T + torch.outer(b1, b2)) / torch.outer(norm1, norm2) / (w1.shape[1]+1)

        else:
            
            norm1 = torch.sqrt(w1.squeeze()**2 + b1**2)
            norm2 = torch.sqrt(w2.squeeze()**2 + b2**2)

            cost = - (torch.outer(w1.squeeze(), w2.squeeze()) + torch.outer(b1, b2)) / torch.outer(norm1, norm2) / 2

        _, col = linear_sum_assignment(cost.cpu())

    return list(col)

# returns permutation and also matched seq2
def match_sequential(seq1: Sequential, seq2: Sequential):

    # Deciderei prima di tutto come farlo. forse la cosa più semplice è:
    # - decidere se tenere la struttura con BasicBlock (oppure toglierla fin dall'inizio)
    # la cosa migliore per generalità del codice è mantenere la struttura a blocchi, 
    # senza toglierla fin dall'inizio (altrimenti diventa più complicato interagire 
    # con altri codici). Mi richiede di scrivere un po' di codice in più, ma probabilmente 
    # neanche troppo (basta che passo tra con/senza blocchi, non dovrebbe essere difficile)
    # OK, quindi questo è deciso. estraggo i blocchi a posteriori e poi li rimetto dentro.
    # Rimane il problema delle skip connection parametriche. Vanno sicuramente permutate 
    # con la stessa permutazione dell'ultimo layer
    # e poi vanno anche inserite nel costo del matching, ma quello dopo.
    # rimane poi il problema della normalizzazione: prima normalizzerei come abbiamo detto
    # (blocco per blocco e poi ultimo layer - ma forse anche solo blocco per blocco per iniziare). 
    # Poi vedrei se ci sono altre opzioni.


    # treat the ResNet case: extract all layers from BasicBlock
    # TODO: still there is the problem of the conv1x1 skip connections
    #assert check_equal_types(seq1, seq2)
    #assert len(seq1) == len(seq2)
    #if False: # TODO: mettere qui la condizione per decidere se è ResNet o no
    #    seq1_temp, seq2_temp = [], []
    #    for m1, m2 in zip(seq1.modules(), seq2.modules()):
    #        if isinstance(m1, BasicBlock) and isinstance(m2, BasicBlock):
    #            for layer_in_block1, layer_in_block2 in zip(m1.modules(), m2.modules()):
    #                seq1_temp.append(layer_in_block1)
    #                seq2_temp.append(layer_in_block2)
    #        else:
    #            seq1_temp.append(m1)
    #            seq2_temp.append(m2)
#
    #    seq1, seq2 = seq1_temp, seq2_temp

    assert check_equal_types(seq1, seq2)
    assert len(seq1) == len(seq2)
    assert check_compatibility(seq1)

    parametric_inds = get_parametric_inds(seq1)

    p_list = [None]*len(seq1)

    with torch.no_grad():
        l = 0
        for _ in range(len(parametric_inds)):

            ind_this = parametric_inds[l]
            ind_next = parametric_inds[l+1] if l+1<len(parametric_inds) else None
            
            p = match(seq1[ind_this], seq2[ind_this])
            p_list[ind_this] = p

            if ind_next is not None:

                ind_next_next = None
                if type(seq2[ind_next]) == BatchNorm2d:
                    ind_next_next = parametric_inds[l+2]

                # ind_this non deve mai essere batchnorm
                #print("ind_this", seq2[ind_this])
                #print("ind_next", seq2[ind_next])
                #if not ind_next_next is None:
                #    print("ind_next_next", seq2[ind_next_next])
                #print("permuting", seq2[ind_this])

                permute_output(seq2[ind_this], p)

                if type(seq2[ind_this]) == Conv2d and type(seq2[ind_next]) == Linear:
                    permute_input_conv_to_lin(seq2[ind_next], p)
                else:
                    permute_input(seq2[ind_next], p)

                l += 1
                # vado avanti di un layer in più se ho incontrato un layer batchnorm
                # in più devo permutare anche il layer successivo
                if type(seq2[ind_next]) == BatchNorm2d:
                    l += 1
                    if type(seq2[ind_this]) == Conv2d and type(seq2[ind_next_next]) == Linear:
                        permute_input_conv_to_lin(seq2[ind_next_next], p)
                    else:
                        permute_input(seq2[ind_next_next], p)

    return p_list, seq2

def permute_sequential(seq: Sequential, p_list, inplace=False):

    assert check_compatibility(seq)
    assert len(p_list) == len(seq)

    parametric_inds = get_parametric_inds(seq)
    parametric_inds_from_list = [i for i in range(len(p_list)) if p_list[i] is not None] 
    print("len(parametric_inds):", len(parametric_inds), "len(parametric_inds_from_list)", len(parametric_inds_from_list))
    # TODO: questo assert dà errore con BatchNorm anche se non dovrebbe
    #assert len(parametric_inds) == len(parametric_inds_from_list)

    if not inplace:
        seq = deepcopy(seq)
 
    with torch.no_grad():
        l = 0
        for _ in range(len(parametric_inds)):

            ind_this = parametric_inds[l]
            ind_next = parametric_inds[l+1] if l+1<len(parametric_inds) else None
            
            p = p_list[ind_this]

            if ind_next is not None:

                ind_next_next = None
                if type(seq[ind_next]) == BatchNorm2d:
                    ind_next_next = parametric_inds[l+2]

                permute_output(seq[ind_this], p)

                if type(seq[ind_this]) == Conv2d and type(seq[ind_next]) == Linear:
                    permute_input_conv_to_lin(seq[ind_next], p)
                else:
                    permute_input(seq[ind_next], p)

                l += 1
                # vado avanti di un layer in più se ho incontrato un layer batchnorm
                # in più devo permutare anche il layer successivo
                if type(seq[ind_next]) == BatchNorm2d:
                    l += 1
                    if type(seq[ind_this]) == Conv2d and type(seq[ind_next_next]) == Linear:
                        permute_input_conv_to_lin(seq[ind_next_next], p)
                    else:
                        permute_input(seq[ind_next_next], p)

    return seq

#def permute_sequential_layer(seq: Sequential, i, p, inplace=True):
#
#    assert seq[i].weight.shape[0] == len(p)
#
#    if not inplace:
#        seq = deepcopy(seq)
#
#    if len(p) == 1:
#        return seq
#
#    nlayers = len(seq)
#
#    with torch.no_grad():
#        seq[i].weight[:] = seq[i].weight[p, :]
#        seq[i].bias[:] = seq[i].bias[p]
#        if i < nlayers-1:
#            seq[i+1].weight[:] = seq[i+1].weight[:, p]
#    return seq
