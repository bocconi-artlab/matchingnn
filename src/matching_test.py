import torch
from torch.nn import Linear, Sequential, ReLU, Tanh, Conv2d, Flatten, MaxPool2d, Module
from matching import match, match_sequential, permute_sequential, COMPAT_ACT
import numpy as np


# test lenet used in test3 
# modified from https://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html
class TestLeNet5(Module):

    def __init__(self):
        super(TestLeNet5, self).__init__()

        conv1 = Conv2d(1, 6, 5, 1)
        conv2 = Conv2d(6, 16, 5, 1)
        maxp = MaxPool2d(2, stride=2, padding=0, dilation=1)
        # an affine operation: y = Wx + b
        fc1 = Linear(16 * 4 * 4, 120)  # 4*4 from image dimension
        fc2 = Linear(120, 84)
        fc3 = Linear(84, 10)

        module_list = [conv1, ReLU(), maxp, conv2, ReLU(), maxp, Flatten(), fc1, ReLU(), fc2, ReLU(), fc3]

        self.model = Sequential(*module_list)

    def forward(self, x):

        return self.model(x)


# Simple reverse permutation
def test1(N1, N2):

    layer1 = Linear(N1, N2)
    layer2 = Linear(N1, N2)
    with torch.no_grad():
        p = list(reversed(range(N2)))
        layer2.weight[:] = layer1.weight[p, :]
        matching = match(layer1, layer2)
        assert p == matching
    return True


# Sequential Linear with single output and actication functions
def test2(nlayers, nhidden_max, input_size=10, nsamples_test = 100):

    sample_act = lambda: np.random.choice(COMPAT_ACT)()

    assert nhidden_max > 0
    assert nlayers > 1

    layer_sizes = np.random.choice(range(1, nhidden_max), nlayers)
    # single output
    layer_sizes[-1] = 1

    # create sequential & permutations
    seq_list = [Linear(input_size, layer_sizes[0]), sample_act()]
    p_list = [np.random.permutation(layer_sizes[0]), None]
    for i in range(0, nlayers-1):
        seq_list.append(Linear(layer_sizes[i], layer_sizes[i+1]))
        p_list.append(np.random.permutation(layer_sizes[i+1]))
        seq_list.append(sample_act())
        p_list.append(None)
    seq = Sequential(*seq_list)

    # create permuted version
    seq_p = permute_sequential(seq, p_list)

    # check if outputs are the same
    data = torch.rand(nsamples_test, input_size)
    assert torch.allclose(seq_p(data), seq(data))

    # get p_list_matched & check output
    p_list_matched, seq_p_matched = match_sequential(seq, seq_p)
    assert torch.allclose(seq(data), seq_p_matched(data))

    # p_list is the permutation seq -> seq_p
    # p_list_matched is the permutation seq_p -> seq
    # therefore argsort(p_list) should be p_list_matched
    for i in range(len(p_list)):
        if p_list[i] is None:
            assert p_list_matched[i] is None
        else:
            assert (np.argsort(p_list[i]) == p_list_matched[i]).all()

    return True


# LeNet
def test3():

    lenet = TestLeNet5()
    import pdb; pdb.set_trace()

if __name__ == '__main__':

    print("Testing 1..", end='')
    if test1(100, 100):
        print("success")
    else:
        print("failure")

    print("Testing 2..", end='')
    if test2(10, 10):
        print("success")
    else:
        print("failure")

    print("Testing 3..", end='')
    if test3():
        print("success")
    else:
        print("failure")




