module Match

using LinearAlgebra
using SparseArrays
using JuMP
using MathOptInterface; const MOI = MathOptInterface

using GLPK
const default_optimizer = ()->GLPK.Optimizer()
const default_optimizer_opts = ()

function getmatch(D::DenseMatrix{<:AbstractFloat}, optimizer, optimizer_opts::Tuple{Vararg{Pair}} = ())
    N1, N2 = size(D)

    m = Model()
    set_optimizer(m, optimizer)
    for (k,v) in optimizer_opts
        set_optimizer_attribute(m, k, v)
    end
    @variable(m, 0.0 ≤ x[1:N1, 1:N2] ≤ 1.0)
    ex = @expression(m, sum(D .* x))
    @objective(m, Min, ex)

    if N1 ≤ N2
        @constraint(m, lcon[i = 1:N1], sum(x[i,:]) == 1)
    else
        @constraint(m, lcon[i = 1:N1], sum(x[i,:]) ≤ 1)
    end

    if N2 ≤ N1
        @constraint(m, rcon[j = 1:N2], sum(x[:,j]) == 1)
    else
        @constraint(m, rcon[j = 1:N2], sum(x[:,j]) ≤ 1)
    end
    optimize!(m)
    termination_status(m) == MOI.OPTIMAL || error("failed: status = $(termination_status(m))")

    return value.(x)::Matrix{Float64}
end

function overlap1(W1::Matrix{Float64}, W2::Matrix{Float64})
    @assert size(W1) == size(W2)
    N, K = size(W1)

    ovs = zeros(K)

    for k = 1:K
        r1 = @view W1[:, k]
        r2 = @view W2[:, k]
        n1 = r1 ⋅ r1
        n2 = r2 ⋅ r2
        ovs[k] = (r1 ⋅ r2) / √(n1 * n2)
    end

    return ovs
end

function norms!(ns::Vector{Float64}, W::Matrix{Float64})
    N, K = size(W)
    @assert length(ns) == K

    for k = 1:K
        r = @view W[:, k]
        ns[k] = √(r ⋅ r)
    end
    return ns
end

function getperm(P::Matrix{Float64})
    K = size(P, 1)
    return Int[findmax(@view P[i,:])[2] for i in 1:K]
end

let Cdict = Dict{Int,Matrix{Float64}}(),
    P1dict = Dict{Int,Vector{Float64}}(),
    P2dict = Dict{Int,Vector{Float64}}()

    global function match(W1::Matrix{Float64}, W2::Matrix{Float64})
        @assert size(W1) == size(W2)
        N, K = size(W1)

        C = get!(Cdict, K) do; zeros(K, K) end
        P1 = get!(P1dict, K) do; zeros(K) end
        P2 = get!(P2dict, K) do; zeros(K) end

        C = transpose(W1)*W2
        # scale!(C, -1.0)
        norms!(P1, W1)
        norms!(P2, W2)

        for j = 1:K
            p2 = P2[j]
            for i = 1:K
                p1 = P1[i]
                C[i,j] = -abs(C[i,j]) / (p1 * p2)
            end
        end

        # P = getmatch(C, default_lpsolver)
        P = getmatch(C, default_optimizer, default_optimizer_opts)

        p = getperm(P)

        return p
    end

    global function clearmem_match!()
        empty!(Cdict)
        empty!(P1dict)
        empty!(P2dict)
    end
end

end # module

