import torch
from torch.nn import Linear, Sequential, Conv2d, BatchNorm2d
import numpy as np
from functools import singledispatch
from src.matching import get_parametric_inds
from copy import deepcopy

@singledispatch
def normalize_output(layer, inplace=True):
    raise NotImplementedError("Not implemented for {}".format(type(layer)))

@singledispatch
def normalize_input(layer, output_norms, inplace=True):
    raise NotImplementedError("Not implemented for {}".format(type(layer)))

@normalize_output.register(Linear)
def _(layer: Linear, inplace=True):

    assert isinstance(layer, Linear)

    if not inplace:
        layer = deepcopy(layer)

    output_norms = []
    with torch.no_grad():
        num_percs = len(layer.bias)
        for i in range(num_percs):
            perc_norm = layer.weight[i, :].norm()
            layer.weight[i, :] /= perc_norm
            layer.bias[i] /= perc_norm
            output_norms.append(perc_norm)

    return layer, output_norms

def normalize_output_final(layer: Linear, inplace=True):

    assert isinstance(layer, Linear)

    if not inplace:
        layer = deepcopy(layer)

    with torch.no_grad():
        layer_norm = layer.weight[:].norm()
        layer.weight[:] /= layer_norm
        layer.bias[:] /= layer_norm
        # to impose that last layer has squared norm equal to the number of its units
        layer.weight[:] *= np.sqrt(10)
        layer.bias[:] *= np.sqrt(10)

    return layer

@normalize_input.register(Linear)
def _(layer: Linear, output_norms, inplace=True):

    assert isinstance(layer, Linear)
    num_prev_percs = len(output_norms)
    assert layer.weight.shape[1] == num_prev_percs

    if not inplace:
        layer = deepcopy(layer)

    with torch.no_grad():
        for i in range(num_prev_percs):
            layer.weight[:, i] *= output_norms[i]

    return layer

@normalize_output.register(Conv2d)
def _(layer: Conv2d, inplace=True):

    assert isinstance(layer, Conv2d)

    if not inplace:
        layer = deepcopy(layer)

    output_norms = []
    with torch.no_grad():
        num_filters = len(layer.bias)
        for i in range(num_filters):
            filter_norm = layer.weight[i, :, :, :].norm()

            layer.weight[i, :, :, :] /= filter_norm
            layer.bias[i] /= filter_norm
            output_norms.append(filter_norm)

    return layer, output_norms

@normalize_input.register(Conv2d)
def _(layer: Conv2d, output_norms, inplace=True):

    assert isinstance(layer, Conv2d)
    num_prev_filters = len(output_norms)
    assert layer.weight.shape[1] == num_prev_filters

    if not inplace:
        layer = deepcopy(layer)

    with torch.no_grad():
        for i in range(num_prev_filters):
            layer.weight[:, i, :, :] *= output_norms[i]

    return layer

def normalize_input_conv_to_lin(layer: Linear, output_norms, inplace=True):

    assert isinstance(layer, Linear)
    num_prev_filters = len(output_norms)
    Nout = layer.weight.shape[0]
    Nin = layer.weight.shape[1]
    assert Nin % num_prev_filters == 0

    if not inplace:
        layer = deepcopy(layer)

    with torch.no_grad():
        layer_weights = layer.weight.reshape(Nout, num_prev_filters, -1)
        #print("(normalization) Conv2d->Linear, reshaping from:", layer.weight.shape, "to:", layer_weights.shape)
        for i in range(num_prev_filters):
            layer_weights[:, i, :] *= output_norms[i]
        layer_weights = layer_weights.reshape(-1, Nin)
        layer.weight[:] = layer_weights

    return layer

@normalize_input.register(BatchNorm2d)
def _(layer: BatchNorm2d, output_norms, inplace=True):

    assert isinstance(layer, BatchNorm2d)

    num_prev_units = len(output_norms)
    assert layer.weight.shape[0] == num_prev_units

    if not inplace:
        layer = deepcopy(layer)

    with torch.no_grad():
        for i in range(num_prev_units):
            layer.bias[i] /= output_norms[i]
            layer.running_mean[i] /= output_norms[i]

    return layer


####
def normalize_model(seq: Sequential, normalize_last_layer=True):

    parametric_inds = get_parametric_inds(seq)
    layer_norms = []

    with torch.no_grad():
        l = 0
        for _ in range(len(parametric_inds)):

            ind_this = parametric_inds[l]
            ind_next = parametric_inds[l+1] if l+1<len(parametric_inds) else None

            if ind_next is not None:

                ind_next_next = None
                if type(seq[ind_next]) == BatchNorm2d:
                    ind_next_next = parametric_inds[l+2]

                # ind_this non deve mai essere batchnorm
                #print("ind_this", seq[ind_this])
                #print("ind_next", seq[ind_next])
                #if not ind_next_next is None:
                #    print("ind_next_next", seq[ind_next_next])
                #print("normalizing", layer_this)

                _, output_norms = normalize_output(seq[ind_this])

                if type(seq[ind_this]) == Conv2d and type(seq[ind_next]) == Linear:
                    normalize_input_conv_to_lin(seq[ind_next], output_norms)
                else:
                    normalize_input(seq[ind_next], output_norms)

                l += 1
                # vado avanti di un layer in più se ho incontrato un layer batchnorm
                # in più devo permutare anche il layer successivo
                if type(seq[ind_next]) == BatchNorm2d:
                    l += 1
                    if type(seq[ind_this]) == Conv2d and type(seq[ind_next_next]) == Linear:
                        normalize_input_conv_to_lin(seq[ind_next_next], output_norms)
                    else:
                        normalize_input(seq[ind_next_next], output_norms)

            else:
                # last layer normalization
                if normalize_last_layer:
                    _ = normalize_output_final(seq[ind_this])

    return seq

"""
def normalize_model(seq: Sequential):

    parametric_inds = get_parametric_inds(seq)
    layer_norms = []

    for idx in parametric_inds:
        layer = seq[idx]
        if idx+1 < len(seq):
            if type(layer) == Linear:
                #print(layer.weight.shape)
                #print(layer.bias.shape)

                with torch.no_grad():
                    layer_norm = torch.sqrt(layer.weight.norm()**2 + layer.bias.norm()**2)

                    layer.weight /= layer_norm
                    layer.bias /= layer_norm

                    layer_norms.append(layer_norm)

            elif type(layer) == Conv2d:
                
                with torch.no_grad():
                    layer_norm = torch.sqrt(layer.weight.norm()**2 + layer.bias.norm()**2)

                    layer.weight /= layer_norm
                    layer.bias /= layer_norm

                    layer_norms.append(layer_norm)

        else:
            with torch.no_grad():
                layer.weight *= np.prod(layer_norms)
                layer.bias *= np.prod(layer_norms)

    return seq
"""